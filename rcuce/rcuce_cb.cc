#include <rcuce/rcuce_cb.hh>
#include <iostream>
#include <iomanip>
#include <utility>
#include <list>
#define BIT10 0x3FF
#define BIT16 0xFFFF
#define BIT20 0xFFFFF
#define BIT24 0xFFFFFF
#define BIT32 0xFFFFFFFF

//====================================================================
void
RcuCE::CodeBook::Memory::print(const char* name) const
{
  std::cout << "  Memory   " << std::left << std::setfill('.') 
	    << std::setw(24) << name << " ";
  if (_address != RcuCE::CodeBook::_invalid)
    std::cout << std::hex << std::setfill('0') 
	      << "@ 0x" << std::setw(4) << _address
	      << "-0x" << std::setw(4) << (_address+_size-1)
	      << " [0x" << std::setw(8) << _mask << "]";
  else 
    std::cout << "invalid";
  std::cout  << std::dec << std::setfill(' ') << std::endl;
}
//====================================================================
void
RcuCE::CodeBook::Register::print(const char* name) const
{
  std::cout << "  Register " << std::left << std::setfill('.') 
	    << std::setw(24) << name << " ";
  if (_address != RcuCE::CodeBook::_invalid)
    std::cout << std::hex << std::setfill('0') 
	      << "@ 0x" << std::setw(4) << _address
	      << "        [0x" << std::setw(8) << _mask << "]"
	      << std::dec << std::setfill(' ');
  else 
    std::cout << "invalid";
  std::cout << std::dec << std::setfill(' ') << std::endl;
}
//====================================================================
void
RcuCE::CodeBook::Command::print(const char* name) const
{
  std::cout << "  Command  " << std::left << std::setfill('.') 
	    << std::setw(24) << name << " ";
  if (_address != RcuCE::CodeBook::_invalid) 
    std::cout << std::hex << std::setfill('0') 
	      << "@ 0x" << std::setw(4) << _address
	      << std::dec << std::setfill(' ');
  else 
    std::cout << "invalid";
  std::cout << std::dec << std::setfill(' ') << std::endl;
}

//=====================================================================
RcuCE::CodeBook::CodeBook()
  : _version(0), 
    // Bus interface
    _rmem	   (0x6000,	BIT20,	128), 
    _imem	   (0x7000,	BIT24,	256), 
    _exec	   (0x0000),
    _abort	   (0x0800),
    _iradd	   (0x7804,	BIT20),
    _irdat	   (0x7805,	BIT20),
    // Pattern interface 
    _pmem	   (0x6800,	BIT10,	1024), 
    _pmcfg	   (0x7805,	BIT20),
    // Slow control 
    _sc_result	   (0x8002,	0x1FFFFF),
    _sc_error	   (0x8003,	0x3),
    _sc_debug	   (_invalid,	0xFFFFFFFF),
    _sc_address    (0x8005,	BIT16), 
    _sc_data	   (0x8006,	BIT16),
    _sc_actfec     (0x8000,     BIT32),
    _sc_switch     (0x8009),
    _sc_exec	   (0x8010),
    _sc_reset	   (0x8011),
    _sc_clear	   (0x8012),
    _sc_old_exec   (0xC000),
    // Monitor 
    _int_mode	   (0x8004,	0x3), 
    _int_type      (_invalid,   0xFF), // 0x8007
    _smem_status   (_invalid,   0x3FF), // 0x8009
    _rst_afl	   (_invalid),		// 0x8013
    _rst_rdo	   (_invalid),		// 0x8014
    _rs_smem	   (_invalid),		// 0x8015
    _status	   (0x8100,	BIT16,	33),
    // Cards 
    _afl	   (_afl_addr,	BIT32),
    _rdo	   (0x8001,	BIT32),
    _acl	   (0x6400,	BIT16,	256),
    // TTC rx
    _ttcrx         (_invalid, 0, 0),
    // hit list 
    _hit_list      (_invalid, 0, 0),
    // Misc
    _header	   (0x4001,	BIT32,	8),
    _errst	   (0x7800,	BIT32),
    _trcfg	   (0x7801,	BIT32),
    _trcnt	   (0x7802,	BIT32),
    _lwadd	   (0x7803,	0x3FFFF),
    _chadd	   (0x7807,	BIT24),
    _reset_errst   (0x6c01),
    _reset_trcfg   (0x6c02),
    _reset_trcnt   (0x6c03),
    // _reset_dmem1 (0x6c04),
    // _reset_dmem2 (0x6c05),
    _fw_version	   (_fw_vers,	BIT24), 
    _dcs_master	   (0xE000),
    _ddl_master	   (0xF000),
    _enable_ttc	   (0xE800),
    _enable_aux	   (0xF800),
    _enable_cmd	   (0xD800),
    _trigger_l1	   (0xD000), 
    _reset_global  (0x2000),
    _reset_fec	   (0x2001),
    _reset_rcu	   (0x2002),
    // RCU II specific 
    _trg_conf      (_invalid, 0),
    _altro_if      (_invalid, 0),
    _rdo_mod       (_invalid, 0),
    _altro_cfg_1   (_invalid, 0),
    _altro_cfg_2   (_invalid, 0),
    _fec_err_a     (_invalid, 0),
    _fec_err_b     (_invalid, 0),
    _rdo_err       (_invalid, 0),
    _rcu_bus       (_invalid, 0),
    _altro_bus	   (_invalid, 0),
    _altro_bus_trsf(_invalid, 0),
    _altro_bus_busy(_invalid, 0),
    _meb_cnt	   (_invalid, 0),
    _sw_trg_cnt    (_invalid, 0),
    _aux_trg_cnt   (_invalid, 0),
    _ttc_l2a_cnt   (_invalid, 0),
    _ttc_l2r_cnt   (_invalid, 0),
    _dstb_cnt_a	   (_invalid, 0),
    _dstb_cnt_b	   (_invalid, 0),
    _trsf_cnt_a	   (_invalid, 0),
    _trsf_cnt_b	   (_invalid, 0),
    _ack_cnt_a	   (_invalid, 0),
    _acl_cnt_b	   (_invalid, 0),
    _cstb_cnt_a	   (_invalid, 0),
    _cstb_cnt_b	   (_invalid, 0),
    _last_dstb_a   (_invalid, 0),
    _last_dstb_b   (_invalid, 0),
    _add_mm_cnt	   (_invalid, 0),
    _bl_mm_cnt	   (_invalid, 0),
    _abd_state	   (_invalid, 0),
    _rdo_state	   (_invalid, 0),
    _imem_state	   (_invalid, 0),
    _evm_state	   (_invalid, 0),
    _da_state	   (_invalid, 0),
    _bp_version    (_invalid, 0),
    _rcu_id        (_invalid, 0),
    _clear_rdrx_reg(_invalid),
    _arb_iter_irq  (_invalid),
    _conf_fec      (_invalid), 
    // TTC rcu II
    _ttc_control   (_invalid, 0),
    _ttc_reset     (_invalid),
    _ttc_event_info(_invalid, 0), 
    _ttc_event_error(_invalid, 0),
    _rcu_temp_cmd(_invalid, 0),
    _rcu_temp_val(_invalid, 0)
    
{}

//____________________________________________________________________
const char*
RcuCE::CodeBook::ElementName(const Element& e) const
{
  if      (e._address == _invalid)                   return 0;
  else if (e._address == _rmem._address)	     return "rmem";
  else if (e._address == _imem._address)	     return "imem";
  else if (e._address == _exec._address)	     return "exec";
  else if (e._address == _abort._address)	     return "abort";
  else if (e._address == _iradd._address)	     return "iradd";
  else if (e._address == _irdat._address)	     return "irdat";
    // Pattern interface 
  else if (e._address == _pmem._address)	     return "pmem";
  else if (e._address == _pmcfg._address)	     return "pmcfg";
    // Slow control 
  else if (e._address == _sc_result._address)	     return "sc_result";
  else if (e._address == _sc_error._address)	     return "sc_error";
  else if (e._address == _sc_debug._address)	     return "sc_debug";
  else if (e._address == _sc_address._address)	     return "sc_address";
  else if (e._address == _sc_data._address)	     return "sc_data";
  else if (e._address == _sc_actfec._address)	     return "sc_actfec";
  else if (e._address == _sc_switch._address)	     return "sc_switch";
  else if (e._address == _sc_exec._address)	     return "sc_exec";
  else if (e._address == _sc_reset._address)	     return "sc_reset";
  else if (e._address == _sc_clear._address)	     return "sc_clear";
  else if (e._address == _sc_old_exec._address)	     return "sc_old_exec";
    // Monitor 
  else if (e._address == _int_mode._address)	     return "int_mode";
  else if (e._address == _int_type._address)	     return "int_type";
  else if (e._address == _smem_status._address)	     return "smem_status";
  else if (e._address == _rst_afl._address)	     return "rst_afl";
  else if (e._address == _rst_rdo._address)	     return "rst_rdo";
  else if (e._address == _rs_smem._address)	     return "rs_smem";
  else if (e._address == _status._address)	     return "status";
    // Cards 
  else if (e._address == _afl._address)		     return "afl";
  else if (e._address == _rdo._address)		     return "rdo";
  else if (e._address == _acl._address)		     return "acl";
    // TTC rx
  else if (e._address == _ttcrx._address)	     return "ttcrx";
    // hit list 
  else if (e._address == _hit_list._address)	     return "hit_list";
    // Misc
  else if (e._address == _header._address)	     return "header";
  else if (e._address == _errst._address)	     return "errst";
  else if (e._address == _trcfg._address)	     return "trcfg";
  else if (e._address == _trcnt._address)	     return "trcnt";
  else if (e._address == _lwadd._address)	     return "lwadd";
  else if (e._address == _chadd._address)	     return "chadd";
  else if (e._address == _reset_errst._address)	     return "reset_errst";
  else if (e._address == _reset_trcfg._address)	     return "reset_trcfg";
  else if (e._address == _reset_trcnt._address)	     return "reset_trcnt";
    // else if (e._address == _reset_dmem1._address) return "reset_dmem1";
    // else if (e._address == _reset_dmem2._address) return "reset_dmem2";
  else if (e._address == _fw_version._address)	     return "fw_version";
  else if (e._address == _dcs_master._address)	     return "dcs_master";
  else if (e._address == _ddl_master._address)	     return "ddl_master";
  else if (e._address == _enable_ttc._address)	     return "enable_ttc";
  else if (e._address == _enable_aux._address)	     return "enable_aux";
  else if (e._address == _enable_cmd._address)	     return "enable_cmd";
  else if (e._address == _trigger_l1._address)	     return "trigger_l1";
  else if (e._address == _reset_global._address)     return "reset_global";
  else if (e._address == _reset_fec._address)	     return "reset_fec";
  else if (e._address == _reset_rcu._address)	     return "reset_rcu";
    // RCU II specific 
  else if (e._address == _trg_conf._address)	     return "trg_conf";
  else if (e._address == _altro_if._address)	     return "altro_if";
  else if (e._address == _rdo_mod._address)	     return "rdo_mod";
  else if (e._address == _altro_cfg_1._address)	     return "altro_cfg_1";
  else if (e._address == _altro_cfg_2._address)	     return "altro_cfg_2";
  else if (e._address == _fec_err_a._address)	     return "fec_err_a";
  else if (e._address == _fec_err_b._address)	     return "fec_err_b";
  else if (e._address == _rdo_err._address)	     return "rdo_err";
  else if (e._address == _rcu_bus._address)	     return "rcu_bus";
  else if (e._address == _altro_bus._address)	     return "altro_bus";
  else if (e._address == _altro_bus_trsf._address)   return "altro_bus_trsf";
  else if (e._address == _altro_bus_busy._address)   return "altro_bus_busy";
  else if (e._address == _meb_cnt._address)	     return "meb_cnt";
  else if (e._address == _sw_trg_cnt._address)	     return "sw_trg_cnt";
  else if (e._address == _aux_trg_cnt._address)	     return "aux_trg_cnt";
  else if (e._address == _ttc_l2a_cnt._address)	     return "ttc_l2a_cnt";
  else if (e._address == _ttc_l2r_cnt._address)	     return "ttc_l2r_cnt";
  else if (e._address == _dstb_cnt_a._address)	     return "dstb_cnt_a";
  else if (e._address == _dstb_cnt_b._address)	     return "dstb_cnt_b";
  else if (e._address == _trsf_cnt_a._address)	     return "trsf_cnt_a";
  else if (e._address == _trsf_cnt_b._address)	     return "trsf_cnt_b";
  else if (e._address == _ack_cnt_a._address)	     return "ack_cnt_a";
  else if (e._address == _acl_cnt_b._address)	     return "acl_cnt_b";
  else if (e._address == _cstb_cnt_a._address)	     return "cstb_cnt_a";
  else if (e._address == _cstb_cnt_b._address)	     return "cstb_cnt_b";
  else if (e._address == _last_dstb_a._address)	     return "last_dstb_a";
  else if (e._address == _last_dstb_b._address)	     return "last_dstb_b";
  else if (e._address == _add_mm_cnt._address)	     return "add_mm_cnt";
  else if (e._address == _bl_mm_cnt._address)	     return "bl_mm_cnt";
  else if (e._address == _abd_state._address)	     return "abd_state";
  else if (e._address == _rdo_state._address)	     return "rdo_state";
  else if (e._address == _imem_state._address)	     return "imem_state";
  else if (e._address == _evm_state._address)	     return "evm_state";
  else if (e._address == _da_state._address)	     return "da_state";
  else if (e._address == _bp_version._address)	     return "bp_version";
  else if (e._address == _rcu_id._address)	     return "rcu_id";
  else if (e._address == _clear_rdrx_reg._address)   return "clear_rdrx_reg";
  else if (e._address == _arb_iter_irq._address)     return "arb_iter_irq";
  else if (e._address == _conf_fec._address)	     return "conf_fec";
    // TTC rcu II
  else if (e._address == _ttc_control._address)	     return "ttc_control";
  else if (e._address == _ttc_reset._address)	     return "ttc_reset";
  else if (e._address == _ttc_event_info._address)   return "ttc_event_info";
  else if (e._address == _ttc_event_error._address)  return "ttc_event_error";
  else if (e._address == _rcu_temp_cmd._address)     return "rcu_temp_cmd";
  else if (e._address == _rcu_temp_val._address)     return "rcu_temp_val";
  return 0;
}

namespace 
{
  struct less
  {
    bool operator()(const RcuCE::CodeBook::Element* lhs, 
		    const RcuCE::CodeBook::Element* rhs) 
    {
      return (lhs->_address < rhs->_address);
    }
  };
}

#define REF(X) &X
//____________________________________________________________________
void
RcuCE::CodeBook::ListElements() const
{
  std::list<const Element*> l;
  l.push_back(REF(_rmem));
  l.push_back(REF(_imem));
  l.push_back(REF(_exec));
  l.push_back(REF(_abort));
  l.push_back(REF(_iradd));
  l.push_back(REF(_irdat));
  l.push_back(REF(_pmem));
  l.push_back(REF(_pmcfg));
  l.push_back(REF(_sc_result));
  l.push_back(REF(_sc_error));
  l.push_back(REF(_sc_debug));
  l.push_back(REF(_sc_address));
  l.push_back(REF(_sc_data));
  l.push_back(REF(_sc_actfec));
  l.push_back(REF(_sc_switch));
  l.push_back(REF(_sc_exec));
  l.push_back(REF(_sc_reset));
  l.push_back(REF(_sc_clear));
  l.push_back(REF(_sc_old_exec));
  l.push_back(REF(_int_mode));
  l.push_back(REF(_int_type));
  l.push_back(REF(_smem_status));
  l.push_back(REF(_rst_afl));
  l.push_back(REF(_rst_rdo));
  l.push_back(REF(_rs_smem));
  l.push_back(REF(_status));
  l.push_back(REF(_afl));
  l.push_back(REF(_rdo));
  l.push_back(REF(_acl));
  l.push_back(REF(_ttcrx));
  l.push_back(REF(_hit_list));
  l.push_back(REF(_header));
  l.push_back(REF(_errst));
  l.push_back(REF(_trcfg));
  l.push_back(REF(_trcnt));
  l.push_back(REF(_lwadd));
  l.push_back(REF(_chadd));
  l.push_back(REF(_reset_errst));
  l.push_back(REF(_reset_trcfg));
  l.push_back(REF(_reset_trcnt));
  l.push_back(REF(_fw_version));
  l.push_back(REF(_dcs_master));
  l.push_back(REF(_ddl_master));
  l.push_back(REF(_enable_ttc));
  l.push_back(REF(_enable_aux));
  l.push_back(REF(_enable_cmd));
  l.push_back(REF(_trigger_l1));
  l.push_back(REF(_reset_global));
  l.push_back(REF(_reset_fec));
  l.push_back(REF(_reset_rcu));
  l.push_back(REF(_trg_conf));
  l.push_back(REF(_altro_if));
  l.push_back(REF(_rdo_mod));
  l.push_back(REF(_altro_cfg_1));
  l.push_back(REF(_altro_cfg_2));
  l.push_back(REF(_fec_err_a));
  l.push_back(REF(_fec_err_b));
  l.push_back(REF(_rdo_err));
  l.push_back(REF(_rcu_bus));
  l.push_back(REF(_altro_bus));
  l.push_back(REF(_altro_bus_trsf));
  l.push_back(REF(_altro_bus_busy));
  l.push_back(REF(_meb_cnt));
  l.push_back(REF(_sw_trg_cnt));
  l.push_back(REF(_aux_trg_cnt));
  l.push_back(REF(_ttc_l2a_cnt));
  l.push_back(REF(_ttc_l2r_cnt));
  l.push_back(REF(_dstb_cnt_a));
  l.push_back(REF(_dstb_cnt_b));
  l.push_back(REF(_trsf_cnt_a));
  l.push_back(REF(_trsf_cnt_b));
  l.push_back(REF(_ack_cnt_a));
  l.push_back(REF(_acl_cnt_b));
  l.push_back(REF(_cstb_cnt_a));
  l.push_back(REF(_cstb_cnt_b));
  l.push_back(REF(_last_dstb_a));
  l.push_back(REF(_last_dstb_b));
  l.push_back(REF(_add_mm_cnt));
  l.push_back(REF(_bl_mm_cnt));
  l.push_back(REF(_abd_state));
  l.push_back(REF(_rdo_state));
  l.push_back(REF(_imem_state));
  l.push_back(REF(_evm_state));
  l.push_back(REF(_da_state));
  l.push_back(REF(_bp_version));
  l.push_back(REF(_rcu_id));
  l.push_back(REF(_clear_rdrx_reg));
  l.push_back(REF(_arb_iter_irq));
  l.push_back(REF(_conf_fec));
  l.push_back(REF(_ttc_control));
  l.push_back(REF(_ttc_reset));
  l.push_back(REF(_ttc_event_info));
  l.push_back(REF(_ttc_event_error));
  l.push_back(REF(_rcu_temp_cmd));
  l.push_back(REF(_rcu_temp_val));
  l.sort(less());

  std::cout << std::hex << std::setfill('0');
  for (std::list<const Element*>::iterator i = l.begin(); i != l.end(); i++) { 
    if (!ElementName(*(*i))) continue;
    std::cout << "0x" << std::setw(4) << (*i)->_address  << ": " 
	      << ElementName(*(*i)) << std::endl;
  }
  std::cout << std::dec << std::setfill(' ') << std::flush;
}

//____________________________________________________________________
void
RcuCE::CodeBook::Configure(const unsigned int fw_version)
{
  _version = fw_version;

  unsigned int day   = 0;
  unsigned int month = 0;
  unsigned int year  = 0;
  if (fw_version == 0xffffffff) 
    year = 0;
  else if (fw_version >  0xffffff) { 
    std::cout << "RCU II from 2009" << std::endl;
    day   = ((fw_version >> 24) & 0xFF);
    month = ((fw_version >> 16) & 0xFF);
    year  = ((fw_version >>  8) & 0xFF);
  }
  else {
    std::cout << "RCU II from 2008" << std::endl;
    day   = ((fw_version >> 16) & 0xFF);
    month = ((fw_version >>  8) & 0xFF);
    year  = ((fw_version >>  0) & 0xFF);
  }
  unsigned int val = (year << 16) | (month << 8) | day;

  // unsigned int day   = ((_version / 10000) % 100);
  // unsigned int month = ((_version / 100) % 100);
  // unsigned int year  = ((_version) % 100);
  // unsigned int val   = (year * 10000 + month * 100 + day);
  
  std::cout << "Initialised code-book with version 0x" 
	    << std::setfill('0') << std::hex << std::setw(8) << _version
	    << " (yy/mm/dd=" << std::setfill('0')
	    << std::setw(2) << year << "/" 
	    << std::setw(2) << month << "/" 
	    << std::setw(2) << day << ")" 
	    << std::setfill(' ') << std::dec << std::endl;

  if (val >=  0x080400) { 
    _fw_version._address = _fw_vers_new;

    _imem.set(0x0000, 0x0fff, 0x3fffff);
    _acl.set(0x1000, 0x1fff, 0xfff);
    _rmem.set(0x2000, 0x37ff, 0x1ffffff);
    _ttcrx.set(0x4000, 0x4fff, 0xffffffff);
    _hit_list.set(0x5000, 0x507f, 0xffffffff);
    // _dmem.set(0x5800, 0x6000, 0xfffff);
    
    _reset_global._address   = 0x5300;
    _reset_fec._address      = 0x5301;
    _reset_rcu._address      = 0x5302;
    _conf_fec._address       = 0x5303;
    _exec._address           = 0x5304;
    _abort._address          = 0x5305;
    _trigger_l1._address     = 0x5306;
    _reset_errst._address    = 0x5307;
    _clear_rdrx_reg._address = 0x5308;
    _reset_trcnt._address    = 0x5309;
    _arb_iter_irq._address   = 0x530a;
    

    _reset_trcfg.invalidate();
    // _reset_dmem1.invalidate();
    // _reset_dmem2.invalidate();
    // _dcs_master.invalidate();
    // _ddl_master.invalidate();
    _enable_ttc.invalidate();
    _enable_aux.invalidate();
    _enable_cmd.invalidate();
    // _trigger_clear.invalidate();
    // _write_flash.invalidate();
    // _read_flash.invalidate();
    // _abort_readout.invalidate();
    // _clear_evet.invalidate();
    
    _afl.set(0x5100, 0xffffffff);
    _altro_if.set(0x5101, 0xffff);
    _trg_conf.set(0x5102, 0xffff);
    _rdo_mod.set(0x5103, 0x3);
    _altro_cfg_1.set(0x5104, 0xffff);
    _altro_cfg_2.set(0x5105, 0xffff);
    _fw_version.set(0x5106, 0xffffffff);
    _bp_version.set(0x5107, 0x1);
    _rcu_id.set(0x5108, 0x3ff);
    _fec_err_a.set(0x5110, 0xffffffff);
    _fec_err_b.set(0x5111, 0xffffffff);
    _rdo_err.set(0x5112, 0x3f);
    _rcu_bus.set(0x5113, 0x3);

    _altro_bus.set(0x5114, 0xff);
    _altro_bus_trsf.set(0x5115, 0xff);
    _altro_bus_busy.set(0x5116, 0x3);
    _meb_cnt.set(0x511b, 0xf);

    _sw_trg_cnt.set(0x511c, 0xfffff);
    _aux_trg_cnt.set(0x511d, 0xfffff);
    _ttc_l2a_cnt.set(0x511e, 0xfffff);
    _ttc_l2r_cnt.set(0x511f, 0xfffff);

    _dstb_cnt_a.set(0x5120, 0xfffff);
    _dstb_cnt_b.set(0x5121, 0xfffff);
    _trsf_cnt_a.set(0x5122, 0xfff);
    _trsf_cnt_b.set(0x5123, 0xfff);
    _ack_cnt_a.set(0x5124, 0xfff);
    _acl_cnt_b.set(0x5125, 0xfff);
    _cstb_cnt_a.set(0x5126, 0xfff);
    _cstb_cnt_b.set(0x5127, 0xfff);
    _last_dstb_a.set(0x5128, 0x1ff);
    _last_dstb_b.set(0x5129, 0x1ff);
    _add_mm_cnt.set(0x512a, 0xfff);
    _bl_mm_cnt.set(0x512b, 0xfff);
    _abd_state.set(0x512c, 0x3fffff);
    _rdo_state.set(0x512d, 0x7fff);
    _imem_state.set(0x512e, 0x7fff);
    _evm_state.set(0x512f, 0xfff);
    _da_state.set(0x5130, 0x3fffffff);
    

    _ttc_control.set(0x4000, 0xffffff);
    _ttc_reset._address = 0x4001; 
    _ttc_event_info.set(0x4028, 0xffffffff);
    _ttc_event_error.set(0x4029, 0xffffffff);
      
    
    _errst.invalidate();
    _trcnt.invalidate();
    _trcfg.invalidate();
    _lwadd.invalidate();
    _chadd.invalidate();
    _iradd.invalidate();
    _irdat.invalidate();
    _pmcfg.invalidate();
    _rdo.invalidate();
    _sc_switch.invalidate();
    
    if (val >= 0x091123) { 
      _sc_error.invalidate();
      _sc_debug.set(0x8003, 0xFFFFFFFF);
      _int_type.set(0x8007,0xFF);
      _smem_status.set(0x8009,0x3FF);
      _rst_afl.set(0x8013);
      _rst_rdo.set(0x8014);
      _rs_smem.set(0x8015);
      _status.set(0x8100,BIT16,32);
    }
    if (val >= 0x101101) { 
      _rcu_temp_cmd.set(0x810a,  0xFFFFFFFF);
      _rcu_temp_val.set(0x800d,  0xFFFF);
    }
  }
  if (val <= 0x060504) {
    _fw_version._address = _fw_vers_old;
    _sc_address.invalidate();
    _sc_data.invalidate();
    _sc_switch.invalidate();
  }
  if (val < 0x060503)
    _fw_version.invalidate();
}

//____________________________________________________________________
void
RcuCE::CodeBook::Print() const
{
  unsigned int day   = ((_version >> 16) & 0xff);
  unsigned int month = ((_version >>  8) & 0xff);
  unsigned int year  = ((_version >>  0) & 0xff);
  // unsigned int day   = ((_version / 10000) % 100);
  // unsigned int month = ((_version / 100) % 100);
  // unsigned int year  = ((_version) % 100);
  
  std::cout << "RCU code book for firmware version 0x" 
	    << std::setfill('0') << std::hex << std::setw(8) 
	    << _version << std::dec 
	    << " (yy/mm/dd=" 
	    << std::setw(2) << year << "/" 
	    << std::setw(2) << month << "/" 
	    << std::setw(2) << day << ")" 
	    << std::setfill(' ') << std::endl;

  std::cout << " ALTRO Bus access:" << std::endl;
  _rmem.print("rmem");				// Result memory
  _imem.print("imem");				// Instruction memory
  _exec.print("exec");				// Execute command
  _abort.print("abort");			// Abort command
  _iradd.print("iradd");			// Last address
  _irdat.print("irdat");			// Last data

  std::cout << " Pattern memory:" << std::endl;
  _pmem.print("pmem");				// Pattern memory
  _pmcfg.print("pmcfg");			// Pattern memory configuration

  std::cout << " Slow control:" << std::endl;
  _sc_result.print("sc_result");		// Result of slow control 
  _sc_error.print("sc_error");			// Error of slow control
  _sc_address.print("sc_address");		// Address for slow control
  _sc_data.print("sc_data");			// Data for slow control
  _sc_actfec.print("sc_actfec");                // Act front-ends for sc
  _sc_switch.print("sc_switch");		// Swithc lines
  _sc_exec.print("sc_exec");			// Execute
  _sc_reset.print("sc_reset");			// Reset slow control errors
  _sc_clear.print("sc_clear");			// Clear result register
  _sc_old_exec.print("sc_old_exec");		// Old 5bit command interface

  std::cout << " Monitor and safety:" << std::endl;
  _int_mode.print("int_mode");			// Interrupt enable
  _status.print("status");			// Status memory
  _rcu_temp_cmd.print("temp_cmd");              // Temperature command
  _rcu_temp_val.print("temp_val");              // Temperature value

  std::cout << " Cards:" << std::endl;
  _afl.print("afl");				// Active front-end cards
  _rdo.print("rdo");				// Read-out front-end cards
  _acl.print("acl");				// Active channel list

  std::cout << " Misc: " << std::endl;
  _ttcrx.print("ttcrx");			// TTCrx registers
  _hit_list.print("hit_list"); 			// Hit list memory
  _header.print("header");			// Common data header
  _errst.print("errst");			// Error and status
  _trcfg.print("trcfg");			// Trigger configuration
  _trcnt.print("trcnt");			// Trigger counter
  _lwadd.print("lwadd");			// Last event length
  _chadd.print("chadd");			// Last channel read out
  _reset_errst.print("reset_errst");		// Reset the status
  _reset_trcfg.print("reset_trcfg");		// Reset trigger configutation
  _reset_trcnt.print("reset_trcnt");		// Reset trigger counter 
  // _reset_dmem1.print("reset_dmem1");		// Reset DMEM1
  // _reset_dmem2.print("reset_dmem2");		// Reset DMEM2
  _fw_version.print("fw_version");		// Firmware version register
  _dcs_master.print("dcs_master");		// Make DCS master of bus
  _ddl_master.print("ddl_master");		// Make DDL master of bus
  _enable_ttc.print("enable_ttc");		// Enable TTC triggers
  _enable_aux.print("enable_aux");		// Enable AUX triggers
  _enable_cmd.print("enable_cmd");		// Enable command triggers
  _trigger_l1.print("trigger_l1");		// Send an L1
  _reset_global.print("reset_global");		// Reset both RCU and FEC
  _reset_fec.print("reset_fec");		// Reset only FEC
  _reset_rcu.print("reset_rcu");		// Reset only RCU

  std::cout << " Registers in RCU II:" << std::endl;
  _trg_conf.print("trg_conf");			// Trigger configuration
  _altro_if.print("altro_if");			// ALTRO bus interface
  _rdo_mod.print("rdo_mod");			// Read out mode
  _altro_cfg_1.print("altro_cfg_1");		// ALTRO coniguration 1
  _altro_cfg_2.print("altro_cfg_2");		// ALTRO coniguration 2
  _fec_err_a.print("fec_err_a");		// Bus errors - branch A
  _fec_err_b.print("fec_err_b");		// Bus errors - branch B
  _rdo_err.print("rdo_err");			// Read-out errors
  _rcu_bus.print("rcu_bus");			// Bus grant register
  _sw_trg_cnt.print("sw_trg_cnt");		// Software trigger counter
  _aux_trg_cnt.print("aux_trg_cnt");		// Auxillary trigger counter
  _ttc_l2a_cnt.print("ttc_l2a_cnt");		// TTC L2 accept counter
  _ttc_l2r_cnt.print("ttc_l2r_cnt");		// TTC L2 reject counter
  _bp_version.print("bp_version");		// Back-plane version register
  _clear_rdrx_reg.print("clear_rdrx_reg");	// Clear RDOERR register
  _arb_iter_irq.print("arb_iter_irq");		// Request DCS interrupt
  _conf_fec.print("conf_fec");			// Configure front-end card fw
}

  
//
// EOF
//

