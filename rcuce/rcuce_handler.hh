#ifndef RCUCE_HANDLER_HH
#define RCUCE_HANDLER_HH
#include <feeserverxx/fee_bytes.hh>
#include <cstdlib>

namespace RcuCE
{
  using FeeServer::byte_array;
  using FeeServer::byte_ptr;
  using FeeServer::byte;

  // Forward declarations 

  /** @class CommandHandler rcuce_handler.hh <rcuce/rcuce_handler.hh>
      @brief Base class for command handlers 

      A command handler can be any entity in the server. It does not
      have to correspond to a physical sub-system (though it usually
      does). 

      @ingroup rcuce_core 
  */
  class CommandHandler 
  {
  public:

    /** Destructor */ 
    virtual ~CommandHandler() {}

    /** Check if this handler can handle the command @a cmd with
	parameters @a param. 
	@param group The command group
	@param cmd   The command that we should handle
	@return true if this object can handle the command. */
    virtual bool CanHandle(unsigned char group, unsigned char cmd) const;

    /** Handle a command 
	@param cmd      Command. 
	@param param    Parameter to command @a cmd.
	@param payload  The payload of the command. 
	@param isize    Maximum number of bytes in payload. 
	@param odata    Output data array. 
	@param osize    Number of previously set bytes in @a odata.
	                On return, it contains the new number of valid
			bytes in @a odata.
	@return Number of bytes processed from @a payload or negative
	error code in case of trouble. */
    virtual int Handle(unsigned char cmd, unsigned short param, 
		       byte_ptr payload, size_t isize, 
		       byte_array& odata, 
		       size_t& osize) = 0;

    /**
     * Clean-up on exit 
     */
    virtual void CleanUp() {}
  protected:
    /** Constructor 
	@param group Group ID */ 
    CommandHandler(unsigned char group) 
      : _group(group & 0xF), 
	_in_handler(false) 
    {}
    /** Copy constructor 
	@param other object to copy from */
    CommandHandler(const CommandHandler& other) 
      : _group(other._group), 
	_in_handler(other._in_handler) 
    {}
    /** Assign operator 
	@param other object to assign from 
	@return reference to this object */
    CommandHandler& operator=(const CommandHandler& other);
    /** Must be called at end of handler */
    void EndHandle() { _in_handler = false; }
    /** the command group */
    unsigned char _group;
    /** True when in handler */ 
    mutable bool _in_handler;
  };
  //__________________________________________________________________
  inline bool 
  CommandHandler::CanHandle(unsigned char cmd, unsigned char /*param*/) const
  {
    _in_handler = cmd == _group;
    return cmd == _group;
  }
  //__________________________________________________________________
  inline CommandHandler& 
  CommandHandler::operator=(const CommandHandler& other) 
  { 
    _group = other._group;
    return *this;
  }
}
#endif
//
// EOF
//


		       
