#include <rcuce/rcuce_cb.hh>
#include <feeserverxx/fee_dbg.hh>
#include <iostream>
#include <cassert>
#include <sstream>


template <typename T>
T& str2val(const char* arg, T& v)
{
  std::stringstream s(arg);
  if (arg[0] == '0') { 
    s << std::oct;
    if (arg[1] == 'x' || arg[1] == 'X') 
      s << std::hex;
  }
  s >> v;
  return v;
}


int 
main(int argc, char** argv)
{
  int fw_vers = -1;
  for (int i  = 1; i < argc; i++) { 
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [OPTIONS] VERSION \n\n" 
		  << "Options:\n" 
		  << "\t-h\tThis help\n" 
		  << std::endl;
	return 0;
      default:
	std::cerr << argv[0] << ": unknown option '" << argv[i] << "'" 
		  << std::endl;
	return 1;
      }
      continue;
    }
    fw_vers = str2val(argv[i], fw_vers);
  }
  if (fw_vers < 0) { 
    std::cerr << argv[0] << "Invalid version number: " << fw_vers << std::endl;
    return 2;
  }

  RcuCE::CodeBook cb;
  cb.Configure(fw_vers);
  cb.Print();
  cb.ListElements();

  return 0;
}

  
