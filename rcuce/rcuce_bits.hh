#ifndef RCUCE_BITS_HH
#define RCUCE_BITS_HH

namespace RcuCE
{
  /** @defgroup rcuce_bits Command interpretation for the RCU control engine. 
      
      The RcuCE::ControlEngine::Issue member function expects the
      input encoded in a message buffer format or as ControlEngine
      commands. 

      The FEE API supports one DIM command channel which contains a
      byte array.  The RCU CE treats the first 4 bytes as a single
      32bit header word.  

      @note The architecture of ARM Linux is @e little @e endian,
      which means that the most significant bit is the first bit of
      the word. 

      All server commands start with 4 1's (@c 0xF) in the most
      significant bits, and anything else is treated as a binary large
      object (@e BLOB) encoded in the message buffer format. 
  */

  //__________________________________________________________________
  /** @name Layout of the CE commands. 
      @ingroup rcuce_bits 

      The format of a the command buffer is as follows 

      <pre>
      <i>command_buffer</i> : <i>command_list</i>
      <i>command_list</i>   : 
                     | <i>command</i> <i>command_list</i>
      <i>command</i>        : <i>header</i> <i>payload</i> <i>trailer</i> 
      <i>header</i>         : <tt>0xF</tt> <i>cmd_id</i> <i>sub_id</i> <i>parameter</i>
      <i>cmd_id</i>         :  4 bit group id
      <i>sub_id</i>         :  8 bit command sub-id
      <i>parameter</i>      : 16 bit user parameter 
      <i>payload</i>        : <i>byte_list</i>
      <i>byte_list</i>      : 
                     | <i>byte</i> <i>byte_list</i>
      <i>byte</i>           : a single unsigned char
      <i>trailer</i>        : <i>end_marker</i> <tt>0x0000</tt>
                     | <i>end_marker</i> <i>version</i>
      <i>end_marker</i>     : <tt>0xDD33</tt>
      <i>version</i>        : 16 bit number
      </pre>

      That is, the command send from the DIM client should be a list
      of commands, each with a header and a trailer word. 
                     
      The format of the header word is as follows 
      @verbatim 
      Bit 31 - 28 | 27 - 24 | 23 - 16 | 15 - 0 
          1 1 1 1   cmd id    sub id    parameter 
	     ^         ^         ^         ^
             |         |         |         |
	     |         |         |      16 bit user parameter passed
	     |         |         |      to the handler function 
	     |         |         |
             |         |      8 bit command sub id passed to the
	     |         |      handler function  
	     |         |        
             |      4 bit command id switches to the handler function 
	     |
	  4 bit code for RCU CE command
      @endverbatim

      The trailer word format is fixed for a given version of the
      command set.  It is 
      @verbatim 
      Bit              31 - 16            | 15 - 0
          1 1 0 1 1 1 0 1 0 0 1 1 0 0 1 1   version
	                  ^                    ^
                          |                    |
                          |                 Command set version 
			  |
          Fixed end of block marker (0xdd33). 
      @endverbatim 

      If the flag ControlEngine::_relax_version_check is set, then the
      version of the command trailer and the current command set
      version need not match. 

      The meaning of the payload (the bytes between the first header
      32bit word and the last trailer 32bit word) depends on the
      command.  Sometimes, there's no payload need - at other times it
      is required.  
  */
  /** Masks for command extraction */
  enum { 
    /** Mask of commands for this CE */
    CeMask    = 0xf0000000, 
    /** Mask of group id */
    GroupMask = 0x0f000000, 
    /** Sub-command mask */
    CmdMask   = 0x00ff0000, 
    /** Parameter mask */ 
    ParamMask = 0x0000ffff,
    /** End-marker mask */
    EndMask   = 0xffff0000,
    /** End-marker version mask */
    VersMask  = 0x0000ffff
  };
  /** Bit fields 
      @ingroup rcuce_bits 
  */
  enum {
    /** Version of this command set. 
	- 1: Initial version 
	- 2: Command set extension for configuration and RCU
  	    register access.
	- 3: More RCU issues. Access to AFL, ACL, data memory, 
	     reset, and trigger */
    VersValue  = 0x0003,  
    /** End-marker */
    EndMarker  = 0xdd330000, 
    /** A full trailer.  The union of version and marker */
    Trailer    = EndMarker | VersValue, 
    /** Our command identifier */
    OurCommand = 0xf0000000
  };

  //__________________________________________________________________
  /** @name Groups 
      @ingroup rcuce_bits 

      Each sub-system of the control engine has a separate command
      group identifier.  When a command is received by the FeeServer,
      the control engine will extract the group identifier from the
      command header (bits 24 to 27), and check with each registered
      CommandHandler, whether the command handler can process this
      command.   If a command handler is willing to process the
      command, then the control engine will give control to that
      objects CommandHandler::Handle member function, and no further
      processing will be done (either by the control engine or other
      command handlers) until a new block is investigated. 
  */
  /** Group ids. */
  enum {
    /** Control engine group 
	@see ControlEngine */
    CeId = 1,
    /** DCS message buffer interface 
	@see Dcsc */
    DcscId = 2, 
    /** RCU 
	@see Rcu */
    RcuId = 3
  };

  //__________________________________________________________________
  /** @name Utility functions 
      @ingroup rcuce_bits
      This is a set of inline utility functions */ 
  /** Get the group ID 
      @param header a 32bit header word 
      @return The group ID  */
  inline unsigned char GetGroup(unsigned int header) { 
    return ((header & GroupMask) >> 24) & 0xF;
  }
  /** Get the command id
      @param header a 32bit header word 
      @return The command ID  */
  inline unsigned char GetCommand(unsigned int header) { 
    return ((header & CmdMask) >> 16) & 0xFF;
  }
  /** Get the command parameter
      @param header a 32bit header word 
      @return The command parameter */
  inline unsigned short GetParameter(unsigned int header) { 
    return ((header & ParamMask) >> 0) & 0xFFFF;
  }
  /** Encode a header 
      @param group Group id 
      @param cmd   Command id 
      @param param Parameter
      @return A header   */
  inline unsigned int MakeHeader(unsigned char group, 
				 unsigned char cmd, 
				 unsigned short param) {
    return (OurCommand | 
	    ((group & 0xF) << 24) | 
	    ((cmd & 0xFF) << 16)  | 
	    (param & 0xFFFF));
  }
  /** Encode a trailer 
      @param version Version 
      @return a trailer word  */ 
  inline unsigned int MakeTrailer(unsigned short version=VersValue) {
    return EndMarker | (VersMask & version);
  }

  //__________________________________________________________________
  /** @defgroup rcuce_ce_bits Control engine commands 
      @ingroup rcuce_bits 

      This defines a number of commands available for the user to send
      to the ControlEngine it self. 

      @see RcuCE::ControlEngine */
  /** Commands for Ce
      @ingroup rcuce_ce_bits */
  enum CeGroup { 
    DUMMY_00,			// 0x0
    /** set the maximum size for the command buffer debug print, by
	default set to 0. 
	@param payload: 1 32bit word 
	@note This command will be ignored  */
    SET_BUF_PRINT_SIZE,		// 0x01
    DUMMY_02,			// 0x02
    /** turn on/off the update of the services.
        @param parameter 0x0 off, otherwise on
        @param payload	 0 */
    EN_SERVICE_UPDATE,		// 0x03
    /** Set the value of a service.
        @param parameter length of the service name string including
	                 the terminating 0
	@param payload	 - a 32bit word
	                 - char string for the service name, zero
			   terminated and padded to 32 bit 
	@return none  
	@note This command will be ignored */
    SET_SERVICE_VALUE,		// 0x04
    /** returns a list of all possible services.
        @param parameter ignored
        @param payload	 0 
	@note This command will be ignored */
    READ_DEFAULT_SERVICES,	// 0x05
    /** returns a list of the valid Front end cards.
        @param parameter ignored 
        @param payload	 0 */
    READ_VALID_FECS,		// 0x06
    /** relax the version checking for commands.
        @param parameter 1 relax, 0 strict
        @param payload	 0 */
    RELAX_CMD_VERS_CHECK,	// 0x07
    /** Set the logging level
        @param parameter log level
        @param payload	 0
	@note  This command will be ignored. Use the FeeServer logging
	facility instead. */
    SET_LOGGING_LEVEL,		// 0x08
    /** Get a list of available high-level commands.
        @param parameter ignored
        @param payload	 none
	@return char buffer
	@version 2 
	@note This command will be ignored*/
    GET_HIGHLEVEL_CMDS,		// 0x09
    /** Force update of a service channel.
        @param parameter number of bytes in the payload
        @param payload	 char buffer containing service name
	@return none 
	@version 2 
	@note This command will be ignored*/
    FORCE_CH_UPDATE,		// 0x0a
    /** Trigger transition for one of the state machines.
        @param parameter number of bytes in the payload
        @param payload	 char buffer containing service name belonging
	                 to the state machine followed by the action
			 separated by a blank. 
	@return  none
	@version 2 
	@note This command will be ignored*/
    TRIGGER_TRANSITION,		// 0x0b
    /** Get a description of states for one of the state machines.
        @param parameter number of bytes in the payload
        @param payload	 char buffer containing service name belonging
	                 to the state machine.
	@return  char buffer with state description
	@version 2 
	@note This command will be ignored*/
    GET_STATES,			// 0x0c
    /** Get a description of transitions for one of the state machines.
        @param parameter number of bytes in the payload
        @param payload	 char buffer containing service name belonging
	                 to the state machine.
	@return  char buffer with state description
	@version 2 
	@note This command will be ignored*/
    GET_TRANSITIONS,		// 0x0d
    DUMMY_0E,
    DUMMY_0F,
    DUMMY_10,
    DUMMY_11,
    DUMMY_12,
    DUMMY_13,
    DUMMY_14,
    DUMMY_15,
    DUMMY_16,
    DUMMY_17,
    DUMMY_18,
    DUMMY_19,
    DUMMY_1A,
    DUMMY_1B,
    DUMMY_1C,
    DUMMY_1D,
    DUMMY_1E,
    DUMMY_1F,
    /** A configure command for the FEE.
	The command encapsulates and arbitrary sequence of other
	commands. The @e Hardware @e address (HW address) is a unique
	ID specifying the target device/partition to receive the
	configuration command. 

	The layout of registers, the memory sizes aso might be
	different between RCU firmware versions. In order to adapt the
	CE to a firmware version, different code books are used to
	define the memory and register layout. Currently, there is no
	version number available in the RCU firmware and the CE is
	therefor @e hard-wired to a certain firmware version. 

        @param parameter number of words inside the block after the
	                 checksum, i.e. the the total number of
	                 (including @ref VERIFICATION  and @ref
	                 CONFIGURE_END) 
        @param payload
         - 32 bit HW address, related to the ALTRO addressing
           - bit 11-0   ALTRO address
           - bit 15-12  Device
	     - 0  CE
	     - 1  RCU
	     - 2  FEC
	     - 3  Altro
	     - 4  Channel
	   - bit 31-16  configuration id to distinguish blocks to the
	                same device/partition 
         - 32 bit checksum for this configuration command
         - first command of the block 
         - ...
         - last configuration command of the block
         - @ref VERIFICATION command
         - @ref CONFIGURE_END command
	 
	   The @ref VERIFICATION command can be empty but is
	   mandatory. If not empty it is added to the list for the
	   verification daemon. The @ref CONFIGURE_END command signals
	   the end of the configuration for a device/partition.
	@return  the output of all the sub-commands
	@version 2 */
    CONFIGURE,			// 0x20
    /** Signal the end of the configuration for a device specified by
        the HW address.  The end of a configuration for a
        device/partition will cause it to change its state. 
        @param parameter ignored
        @param payload	 32 bit HW address as for the @ref
	                 CONFIGURE command 
	@return  none
	@version 2 */
    CONFIGURE_END,		// 0x21
    /** Specify a verification sequence.  The command can only occur
        inside a @ref CONFIGURE command and will be connected to
        this. The verification daemon periodically checks the
        configuration with the help of verification sequences. The
        daemon doesn't know about the operations of the verification *
        and neither the result.

	It treats the sub-commands as normal CE commands and
	calculates the checksum from the result.
	
	@note This implies a modulation of the hardware when preparing
	the verification command block.
     
        @param parameter number of payload words excluding the 2
	                 checksum words. If 0, no checksum words 
                         are expected 
        @param payload
        - 32 bit checksum for this verification command block
        - 32 bit checksum for the result of the verification sequence
        - first command of the block 
        - ...
        - last command of the block
     
	@note An empty verification command (parameter zero) does not
	have any payload, not even the checksums.
	@return  the output of all the sub-commands
	@version 2 */
    VERIFICATION,		// 0x22
    /** Signal the need for external configuration data.  The command
        can only occur inside a @ref CONFIGURE command and will abort
        the interpretation of the configuration sequence. The device
        will go into state @e CONF_EXTERNAL and wait for a @e
        closeConfExternal action. After this action the device is
        considered to be completely configured and it wil go to state
        @e STBY_CONFIGURED.
     
        @param parameter lower 16 bit of the device HW address
                         described in @ref CONFIGURE 
        @param payload	none
	@return  none
	@version 2 */
    EXTERNAL_CONFIGURATION,	// 0x23
  };

  //__________________________________________________________________
  /** @defgroup rcuce_dcsc_bits DCSC commands 
      @ingroup rcuce_bits 

      Commands for the DCS message buffer interface 
      
      @see RcuCE::Dcsc */
  /** Commands for Dcsc
      @ingroup rcuce_dcsc_bits */
  enum DcscGroup {
    /** set a debug option flag of the dcscRCUaccess library
        @param parameter bit field of flags 
        @param payload	 0 */
    SET_OPTION_FLAG = 0x01,
    /** clear a debug option flag of the dcscRCUaccess library
        @param parameter bit field of flags
        @param payload	 0 */
    CLEAR_OPTION_FLAG,		// 0x02
    /** enable/disable single write/read mode for MsgBuffer
        interface. 
        @param parameter 0 disable, 1 enable
        @param payload	 0 */
    USE_SINGLE_WRITE,		// 0x03
  };

  //__________________________________________________________________
  /** @defgroup rcuce_rcu_bits RCU Commands 
      @ingroup rcuce_bits 
      
      Commands that the RCU sub-system understands 

      @see RcuCE::Rcu */
  /** Commands for the RCU 
      @ingroup rcuce_rcu_bits */
  enum RcuGroup {
    /** Send the execution command to run the sequence written to rcu
        instruction memory. 
        @param parameter 8 bit start address in the instruction memory
        @param payload   nothing
        @note The start address is available from command set version
        2, in version 1 the parameter is just ignored */
    EXEC		= 0x01,
    /** Stop the execution of the RCU sequencer.
        @param parameter ignored
        @param payload   nothing */
    STOP_EXEC,		// 0x02

    /** @{
	The following commands allow to write a block of data to RCU
	instruction or pattern memory. The number of 32 bit words is
	given by the command parameter */
    /** Write to RCU instruction memory.
	@param parameter number of 32 bit words in the payload 
	@param payload  32 bit data words */
    WRITE_INSTRUCTION,	// 0x03
    /** Write to rcu instruction memory and send the execution
	command. 
	@param parameter number of 32 bit words in the payload 
	@param payload 32 bit data words  */
    EXEC_INSTRUCTION,	// 0x04
    /** Write 8 bit data to RCU pattern memory.
	@param parameter number of 8 bit words in the payload 
	@param payload 8 bit data words  */
    WRITE_PATTERN8,	// 0x05
    /** Write 16 bit data to RCU pattern memory.
	@param parameter number of 16 bit words in the payload 
	@param payload 16 bit data words */
    WRITE_PATTERN16,	// 0x06
    /** Write 32 bit data to rcu pattern memory.
        @param parameter number of 32 bit words in the payload 
        @param payload   32 bit data words */
    WRITE_PATTERN32,	// 0x07
    /** Write 10 bit compressed data to rcu pattern memory.
        Three 10 bit data words stored in 4 bytes in a 'little endian'
        representation.
        @param parameter number of 10 bit words in the payload 
        @param payload   10 bit compressed data words */
    WRITE_PATTERN10,	// 0x08
    /** @} */

    /** @{
	The following commands allow to read the content of the rcu
	instruction or pattern memory the number of 32 bit words is
	given by the command parameter */
    /** Read from rcu instruction memory.
        @param parameter number of 32 bit words to read 
        @param payload   0 */
    READ_INSTRUCTION,	// 0x09
    /** Read from rcu pattern memory.
        @param parameter number of 32 bit words to read 
        @param payload   0 */
    READ_PATTERN,	// 0x0A
    /** @} */
    
    /** @{ 
	The following commands allow to read and write from/to an RCU
	memory location. @a parameter is a 16 bit address */
    /** Read from rcu memory location.
        @param parameter 16 bit address 
        @param payload   0 */
    READ_MEMORY,	// 0x0B
    /** Write to rcu memory location.
        @param parameter 16 bit address 
        @param payload   a 32 bit word */
    WRITE_MEMORY,	// 0x0C
    /** Write to rcu result memory.
        @param parameter number of 32 bit words in the payload 
        @param payload   32 bit data words */
    WRITE_RESULT,	// 0x0D
    /** Read from rcu result memory.
        @param parameter number of 32 bit words to read 
        @param payload   0 */
    READ_RESULT,	// 0x0E
    DUMMY_0Fa,
    /** Write block to rcu memory.
        @param parameter number of 32 bit words to write 
        @param payload   the first 32 bit word of the payload is the
	             address to write to, the 32 bit data follows */ 
    WRITE_MEMBLOCK,	// 0x10
    /** Read block from rcu memory.
        @param parameter number of 32 bit words to read 
        @param payload   address */
    READ_MEMBLOCK,	// 0x11
    /** @} */

    /** @{ 
	Register access */
    /** Read the ERRST register of the RCU.
        @param parameter ignored 
        @param payload   none
	@return    32 bit register content
	@version 2  */
    READ_ERRST,		// 0x12
    /** Write the TRGCFG register of the RCU.
        @param parameter ignored 
        @param payload   32bit data (only bit 0 to 28 valid) 
	@return    none 
	@version 2  */
    WRITE_TRGCFG,	// 0x13
    /** Read the TRGCFG register of the RCU.
        @param parameter ignored 
        @param payload   none
	@return    32 bit register content 
	@version 2  */
    READ_TRGCFG,	// 0x14
    /** Write the PMCFG register of the RCU.
	@note The PMCFG register is 20 bit wide.
        @param parameter ignored 
        @param payload   32bit data 
	@return    none 
       @version 2  */
    WRITE_PMCFG,	// 0x15
    /** Read the PMCFG register of the RCU.
	@note The PMCFG register is 20 bit wide.
        @param parameter ignored 
        @param payload   none 
	@return    32 bit register content 
	@version 2  */
    READ_PMCFG,		// 0x16
    /** Read the TRCNT register of the RCU.
        @param parameter ignored 
        @param payload   none 
	@return    32 bit register content  
	@version 3  */
    READ_TRCNT,		// 0x17
    /** Write the AFL register of the RCU.
        @param parameter ignored 
        @param payload   32bit data (only bit 0 to 28 valid) 
	@return    none 
	@version 3  */
    WRITE_AFL,		// 0x18
    /** Read the AFL register of the RCU.
        @param parameter ignored 
        @param payload   none 
	@return    32 bit register content 
	@version 3  */
    READ_AFL,		// 0x19
    /** @} */

    /** @{ 
	These commands allows further memory access */
    /** Write to rcu ACL memory.
        @param parameter number of 32 bit words in the payload 
        @param payload   32 bit data words
	@version 3  */
    WRITE_ACL,		// 0x1A
    /** Read from rcu ACL memory.
        @param parameter number of 32 bit words to read 
        @param payload   0 
	@version 3  */
    READ_ACL,		// 0x1B
    /** Write to rcu HEADER memory.
        @param parameter number of 32 bit words in the payload 
        @param payload   32 bit data words
	@version 3  */
    WRITE_HEADER,	// 0x1C
    /** Read from rcu HEADER memory.
        @param parameter number of 32 bit words to read 
        @param payload   0
	@version 3  */
    READ_HEADER,	// 0x1D
    /** @} */

    DUMMY_1Ea, 
    DUMMY_1Fa,

    /** @{ 
	Other commands and register I/O */
    /** Check the error register of the RCU. The command can be used
       to check the status of the error register during a command
       sequence and abort if there is an error.
        @param parameter ignored 
	@return    none, the command fails if the error register of
	the RCU is non-zero.
	@version 2  */
    CHECK_ERROR,	// 0x20
    /** Enable/disable automatic error checking after each sequencer
        execution. 
        @param parameter 0 disable, 1 enable 
        @param payload   0 
	@return    none 
	@version 2  */
    EN_AUTO_CHECK,	// 0x21
    /** Set the Altro Bus Master
        @param parameter 0 DDL SIU, 1 DCS board 
        @param payload   0 
	@return    none 
	@version 2 
	@note  this command serves the DCS_ON/DDL_ON operations 
	of the RCU firmware */
    SET_ABM,		// 0x22
    /** Check if the CE matches a given version number of the RCU
	firmware. The RCU firmware can differ from version to version
	in the layout of the registers, the memory sizes aso. In order
	to adapt the CE to a firmware version, different code books
	are used to define the memory and register layout. Currently,
	there is no version number available in the RCU firmware and
	the CE is therefor "hard-wired" to a certain firmware
	version. 
        @param parameter version number 
        @param payload   0 
	@return:    32 bit value: 1 if version matches, 0 if not
	@version 2  */
    CHECK_VERSION,	// 0x23
    /** Reset the FEE
        @param parameter 1 = only FECs, 2 = only RCU, global otherwise 
        @param payload   0 
	@return    none 
	@version 3 
	@note  this command serves the GLB_RESET, FEC_RESET and
	RCU_RESET operations of the RCU firmware  */
    RESET,		// 0x24
    /** Enable trigger source
        @param parameter 1 = L1_TTC, 2 = L1_I2C, L1_CMD otherwise 
        @param payload   0 
	@return    none 
	@version 3 
	@note  this command serves the L1_CMD, L1_TTC and  L1_I2C
	operations of the RCU firmware */ 
    L1TRG_SELECT,	// 0x25
    /** Send an L1 trigger (L1 operation of RCU firmware)
        @param parameter ignored 
        @param payload   0 
	@return          none 
	@version 3  */
    SEND_L1_TRIGGER,	// 0x26
    /** Get the firmware version 
	@param parameter ignored 
	@param payload   0
	@return          The firmware version */
    GET_FWVERS		// 0x27
    /** @} */
  };  


}
#endif
//
// EOF
//

      
