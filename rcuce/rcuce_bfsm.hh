#ifndef RCUCE_BFSM
#define RCUCE_BFSM
#include <rcuce/rcuce_states.hh>
#include <rcuce/rcuce_serv.hh>
#include <rcuce/rcuce_fsm.hh>
#include <feeserverxx/fee_servt.hh>

namespace RcuCE
{
  struct StateService;
  struct BasicFsmParent;
  
  //____________________________________________________________________
  /** @class BasicFsm rcuce_bfsm.hh <rcuce/rcuce_bfsm.hh> 
      @brief A basic FSM for the CE and RCU 
      @image html basic_fsm.png "State diagram" 
      @ingroup rcuce_fsm
  */
  class BasicFsm : public StateMachine 
  {
  public:
    /** Constructor 
	@param initial The initial state
	@param id Id of this FSM */
    BasicFsm(unsigned char id, const State& initial=CommonStates::Idle);
    /** Destructor */
    virtual ~BasicFsm();

    /** Check if this state machine can handle the command @a action.  
	The state machines have an particular id, and perhaps also an
	associated address.  This identifer and optional address is
	used by the control engine to send commands to the right state
	machine.  
	@param action  Action that we may be able to handle 
	@param id      Identifier 
	@param address Address 
	@param param   Parameters 
	@return @c true if this state machine can handle the @a
	action, false otherwise */
    virtual bool TryHandle(const Action& action, 
			   unsigned char id, 
			   unsigned short address=0, 
			   unsigned short param=0) const;
    /** Get pointer to state service, if any */ 
    virtual const StateService* GetStateService() const { return 0; }
    /** Get the action identifier corresponding to a string
	@param action Action as a string. 
	@return Action number */
    static const Action& String2Action(const std::string& action);
    /** Get the action identifier corresponding to a string
	@param action Action as a string. 
	@return Action number */
    static std::string Action2String(const Action& action);
    /** Get a state corresponding to a string */
    static const State& String2State(const std::string& state);
    /** Get a state corresponding to a string */
    static std::string State2String(const State& state);
    /** Go to error state on illegal actions */
    void IllegalAction(const Action&, const State&);
    /** Get identifier */
    unsigned int Id() const { return unsigned(_id); }
    /** Set parent FSM */
    void SetParent(BasicFsmParent* p);
  protected:
    /** Copy ctor - not implemented */
    BasicFsm(const BasicFsm&);
    /** Assignement operator - not implemented */
    BasicFsm& operator=(const BasicFsm&);
    /** Called when state is changed */
    virtual void DoSetState(const State& s);
    /** Id of this state machine */
    unsigned char _id;
    /** Possible parent */
    BasicFsmParent* _parent;
  };  

  //__________________________________________________________________
  /** State service 
      @ingroup rcuce_fsm */
  struct StateService : public Service 
  {
    /** Constructor
	@param name Name of the service
	@param fsm  Reference to the state machine */
    StateService(const std::string& name, 
		 StateMachine&      fsm);
    /** Get the new value from the RCU 
	@return true on success */
    int Update();
    /** Always update this service - no matter what */
    bool Always() const { return true; }
  protected: 
    /** The state machine */ 
    StateMachine& _fsm;
    /** The service */
    FeeServer::IntService _serv;
  };
  //__________________________________________________________________
  /** Parent of BasicFsm objects 
      @ingroup rcuce_fsm */
  struct BasicFsmParent : public BasicFsm
  {
    /** Typedef of fsm list */
    typedef std::list<BasicFsm*> FsmList;

    /** Constructor */
    BasicFsmParent(int id);
    /** Constructor */
    virtual ~BasicFsmParent();
    /** Add a fsm */
    virtual bool AddFsm(BasicFsm& fsm);
    /** Remove a fsm */
    virtual bool RemoveFsm(BasicFsm& fsm);
    /** Find an FSM that can handle action @a action, with identifier
	@a id, and address @a addr, and parameters @a param. 
	@param action  Action to perform 
	@param id      Identifier 
	@param addr    Address 
	@param param   Parameters 
	@return Pointer to FSM or null if not found */
    BasicFsm* FindFsm(unsigned int   action, 
		      unsigned char  id, 
		      unsigned short addr, 
		      unsigned short param) const;
    /** Find an FSM that has a state service with name @a name. 
	@param name Name to look for 
	@return a pointer to the FSM or null if no such FSM was found */
    BasicFsm* FindFsm(const std::string& name) const;
    /** Virtual member function to call when a transition is made. 
	@param src    The source state 
	@param action The action that prompted the transition 
	@param dest   The destination state  */
    virtual void Transition(const State&  src, 
			    const Action& action, 
			    const State&  dest);
    /** Get list of childern */
    FsmList& Fsms() { return _fsms; }
    /** Get list of childern */
    const FsmList& Fsms() const { return _fsms; }
  protected:
    friend class BasicFsm;
    /** Implements 
	@verbatim
	if      all children in state s, go to state s
	else if any childern in state ERROR, go to state ERROR
	else    go to state MIXED
	@endverbatim
	@return new state
    */
    const State& UpdateState();
    /** List of command handlers */
    FsmList _fsms;
  };
}

#endif  
//____________________________________________________________________
//
// EOF
//



  
