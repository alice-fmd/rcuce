#include <rcuce/rcuce_fsm.hh>
#include <iostream>
#include <sstream>
#include <algorithm>

//____________________________________________________________________
bool
RcuCE::StateMachine::AddState(const State& state)
{
  FeeServer::Guard g(_mutex);
  if (FindState(state) >= 0) return false;
  _states.push_back(state);
  return true;
}

//____________________________________________________________________
bool
RcuCE::StateMachine::AddTransition(const State&  src,
				   const Action& action,
				   const State&  dest)
{
  FeeServer::Guard g(_mutex);
  int i1 = 0, i2 = 0;
  if ((i1 = FindState(src))  < 0) return false;
  if ((i2 = FindState(dest)) < 0) return false;

  // Check if we have seen this action already 
  ActionMap::const_iterator i = _map.find(action);
  if (i == _map.end()) _map[action] = TransitionMap();

  // Get the transition map
  TransitionMap&                t = _map[action];
  TransitionMap::const_iterator j = t.find(i1);
  
  // Check if we already have a transition on this action from this source.
  if (j != t.end()) return false; 
      
  // Add the transition to the map 
  t[i1] = i2;
  return true;
}

//____________________________________________________________________
const RcuCE::StateMachine::State&
RcuCE::StateMachine::Trigger(const Action& action)
{
  size_t src = _current;
  {
    FeeServer::Guard g(_mutex);
    ActionMap::const_iterator i = _map.find(action);
    if (i == _map.end()) return _states[_current];
    
    // Find the source state in the list of transitions 
    TransitionMap::const_iterator j = i->second.find(_current);
    if (j == i->second.end()) {
      _mutex.Unlock();
      IllegalAction(action, _states[_current]);
      _mutex.Lock();
      return _states[_current];
    }
  
    // Now do the transition
    _current   = j->second;
  }
  Transition(_states[src], action, _states[_current]);
  DoSetState(_states[_current]);
  return _states[_current];
}

//____________________________________________________________________
bool
RcuCE::StateMachine::SetState(const State& state)
{
  {
    FeeServer::Guard g(_mutex);
    int i = FindState(state);
    if (i < 0) return false; // Unknown state 
    _current = i;
  }
  DoSetState(_states[_current]);
  return true;
}

//____________________________________________________________________
const RcuCE::StateMachine::State&
RcuCE::StateMachine::GetState() const
{
  return _states[_current];
}
//____________________________________________________________________
void
RcuCE::StateMachine::Actions(const State& s, std::vector<Action>& a) const
{
  for (ActionMap::const_iterator i = _map.begin(); i != _map.end(); ++i) {
    if (i->second.find(FindState(s)) != i->second.end()) 
      a.push_back(i->first);
  }  
}
//____________________________________________________________________
void
RcuCE::StateMachine::Actions(std::vector<Action>& a) const
{
  Actions(_states[_current], a);
}
//____________________________________________________________________
void
RcuCE::StateMachine::Transitions(const State& s, 
				 std::map<Action,State>& t) const
{
  for (ActionMap::const_iterator i = _map.begin(); i != _map.end(); ++i) {
    TransitionMap::const_iterator j = i->second.find(FindState(s));
    if (j != i->second.end()) t[i->first] = _states[j->second];
  }  
}

//____________________________________________________________________
void
RcuCE::StateMachine::Transitions(std::map<Action,State>& t) const
{
  Transitions(_states[_current], t);
}

//____________________________________________________________________
int
RcuCE::StateMachine::FindState(const State& state) const
{
  StateVector::const_iterator i =std::find(_states.begin(),_states.end(),state);
  if (i == _states.end()) return -1;
  return std::distance(_states.begin(), i);
}
//====================================================================
const char*
RcuCE::StateMachinePlotter::StateName(const State& state) const
{
  std::stringstream s;
  s << "s" << state;
  return s.str().c_str();
}
//____________________________________________________________________
const char*
RcuCE::StateMachinePlotter::ActionName(const Action& action) const
{
  std::stringstream s;
  s << "a" << action;
  return s.str().c_str();
}
//____________________________________________________________________
const char*
RcuCE::StateMachinePlotter::StateComment(const State& /*state*/) const
{
  return 0;
}

//____________________________________________________________________
std::ostream&
RcuCE::StateMachinePlotter::Print(std::ostream& o) const
{
  o << "digraph FSM {\n";
  AtStart(o);
  o << " rankdir=TB;\n"
    << " node [shape=ellipse,style=filled,fillcolor=khaki1];\n"
    << " /* States */\n";
  typedef std::map<Action,State> TMap;
  const StateVector& v = _state_machine.States();
  for (StateVector::const_iterator i = v.begin(); i != v.end(); ++i)
    o << "  s" << *i << " [label=\"" << StateName(*i) << "\"];"
      << std::endl;
  o << "  /* Transitions */\n";
  for (StateVector::const_iterator i = v.begin(); i != v.end(); ++i) {
    TMap t;
    o << "/* for state s" << *i << " [" << StateName(*i) << "] */" 
      << std::endl;
    _state_machine.Transitions(*i,t);
    for (TMap::const_iterator j = t.begin(); j != t.end(); ++j) 
      o << "  s" << *i << " -> s" << j->second << " [label=\"" 
	<< ActionName(j->first) << "\"];" << std::endl;
  }
  o << "  /* Comments */\n"
    << "  node [shape=box,style=\"dashed,filled\",fillcolor=ivory1];\n";
  for (StateVector::const_iterator i = v.begin(); i != v.end(); ++i) {
    if (!StateComment(*i)) continue;
    o << "  c" << *i << " [label=\"" << StateComment(*i) << "\"];\n";
    o << "  c" << *i << " -> s" << *i << " [style=dashed];\n";
  }
  AtEnd(o);
  return o << "}";
}

//====================================================================
std::ostream& RcuCE::operator<<(std::ostream& o, 
				const RcuCE::StateMachinePlotter& s)
{
  return s.Print(o);
}

//____________________________________________________________________
//
// EOF
//
