#ifndef RCUCE_FEC_HH
#define RCUCE_FEC_HH
#include <rcuce/rcuce_serv.hh>
#include <rcuce/rcuce_rcu.hh>
#include <feeserverxx/fee_dbg.hh>
#include <feeserverxx/fee_servt.hh>
#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <cerrno>
#include <cstring>

namespace FeeServer 
{
  class Main;
}

namespace RcuCE
{
  // Forward declaration 
  class Rcu;
  class Fec;

  /** @defgroup rcuce_fecs Classes for use with FEC services 

      @brief This group of classes provides some utilities for
      defining FEC services, etc.  The idea is, that the user defines
      and specicies a FecFactory that will create specialised Fec
      objects.  The user can encode behviour into these objects.  The
      base class Fec provides a member function for defining FEC
      services, such as temperatures, voltages, and currents.  It can
      also define some sort of state if that is required. 

      An example follows (@ref test_server.cc) 

      First, we define our Fec class that corresponds to a Front-end
      card. 
      @dontinclude test_server.cc 
      @skip struct Fec 
      @until //___
      
      We define the inner class @c State for a state service.  This is
      a little special, in that it doesn't use the FecService
      interface, but rather the Service interface.  This illustrates
      that you can define your Fec class as freely as you want. 
      @until //___

      Next, we define the constructor.  Here, we add a service to read
      the temperature (register 0x6) from the BC, and our state
      service.  
      @until //___

      Next follows the destructor.  Note, that we remove the services
      explicitly 
      @until //___

      An finally we override ServiceProvider::UpdateAll to update our
      state service according to the read values. 
      @until //___
      
      In this example, the data members are all public 
      @until };

      The factory is pretty straight forward 
      @until };

      Finally, we can define our program.  We define some command line
      options 
      @until //EndOptions

      and our FeeServer 
      @until //EndFee

      Then we set up our control engine, set it in the server, define
      our DCS interface, our RCU interface, and the FecFactory 
      @until //EndRCU 

      We can now run our program 
      @until }
  */
  //__________________________________________________________________
  /** @class Fec rcuce_fec.hh <rcuce/rcuce_fec.hh>
      @brief that represents a front end card.  
      Users should derive classes from this class to publish services
      from a front-end card. 
      @ingroup rcuce_fecs */
  class Fec : public ServiceProvider 
  {
  public:
    virtual ~Fec();
    /** Add a service to this provider. 
	@param name NAme of service. 
	@param addr Address to read value from. */
    template <typename T, typename Trait>
    Service* AddService(const std::string& name, unsigned char addr);
    /** Add a service to this provider. 
	@param name NAme of service. 
	@param addr Address to read value from. */
    template <typename T, typename Trait>
    Service* AddBusService(const std::string& name, unsigned char addr);

    /** Read a register
	@param addr BC address to read from. 
	@param ret  Return value */
    int Read(unsigned char addr, unsigned short& ret);
    /** Read a register
	@param addr BC address to read from. 
	@param ret  Return value */
    int ReadBus(unsigned char addr, unsigned short& ret);
    /** Remove a service from the list */ 
    // virtual bool RemoveService(Service& service);
    /** Add a service.  Note, that services added using this member
	function is not managed. 
	@param s Service to add 
	@return true on success, false otherwise */
    bool AddService(Service& s) { return ServiceProvider::AddService(s); }
    /** Add a service.  Note, that services added using this member
	function is not managed. 
	@return true on success, false otherwise */
    bool AddStatusService();
    /** Update all services 
	@return 0 on success, error code otherwise */ 
    virtual int UpdateAll();
    /** Update some services - in particular those that return true
	when calling the member function Service::Always from it.
	@return 0 on success, error code otherwise */ 
    virtual int UpdateSome();
    /** 
     * enable monitoring 
     * 
     */
    static void Enable() { _enabled = true; }
    /** 
     * Disable monitoring 
     * 
     */
    static void Disable() { _enabled = false; }
    /**
     * Set the log output file 
     */
    static void SetLog(std::ostream* log) { _log_file = log; }
    
    static void LogService(const char* format, ...);
  protected:
    //________________________________________________________________
    /** Utility class to define services from FEC 
	@ingroup rcuce_fecs */
    template <typename T, typename Trait>
    class FecService : public Service 
    {
    public:
      /** Constructor 
	  @param fec Reference to FEC 
	  @param name Base name of the service 
	  @param addr BC address to read from */
      FecService(Fec& fec, const std::string& name, unsigned char addr);
      /** 
       * Update the service 
       *
       * Read the monited register via the I2C bus.  If the read
       * fails, then the quality stamp is set to a non-zero value.  
       * 
       * - If the card is not on, or the wrong card is seen in the
       *   response (EBADF) then bit 0 is asserted.
       * - If the card does not respond (EAGAIN), then bit 1 is
       *   asserted. 
       *
       * (Note, the PVSS client only looks at the 4 lower bits). 
       */
      int Update();
      /** Destructor */
      virtual ~FecService();
    protected: 
      /** The service */
      FeeServer::ServiceT<T, Trait> _serv;
      /** The address */
      unsigned char _addr;
      /** Reference to FEC */ 
      Fec& _fec;
    };
    //________________________________________________________________
    /** Utility class to define services from FEC 
	@ingroup rcuce_fecs */
    template <typename T, typename Trait>
    class BusService : public Service 
    {
    public:
      /** Constructor 
	  @param fec Reference to FEC 
	  @param name Base name of the service 
	  @param addr BC address to read from */
      BusService(Fec& fec, const std::string& name, unsigned char addr);
      /** 
       * Update the service 
       *
       * The monitored value is read using the ALTRO bus.  
       */
      int Update();
      /** Destructor */
      virtual ~BusService();
    protected: 
      /** The service */
      FeeServer::ServiceT<T, Trait> _serv;
      /** The address */
      unsigned char _addr;
      /** Reference to FEC */ 
      Fec& _fec;
    };
    //________________________________________________________________
    /** Utility class to define services from FEC 
	@ingroup rcuce_fecs */
    class StatusService : public Service 
    {
    public:
      StatusService(Fec& fec, const std::string& name);
      int Update();
    protected:
      FeeServer::ServiceT<int,FeeServer::IntTrait> _serv;
      Fec& _fec;
    };
    /** Utility function to make a service name.  
	The service name has the form @e XX_NAME where @e XX is the
	FEC address, and @e NAME is the passed name. 
	@param name Base name of service 
	@return @e XX_NAME */
    std::string MakeServiceName(const std::string& name) const;
    /** Constructor
	@param addr Address of FEC.  
	@param main Reference to fee server main 
	@param rcu Reference to RCU */
    Fec(unsigned char addr, FeeServer::Main& main, Rcu& rcu);
    /** Reference to RCU. */
    Rcu& _rcu;
    /** Address of this fec. */ 
    unsigned char _address;
    /** List of memory to clean up at deletion */
    typedef std::vector<Service*> CleanupList;
    /** List of memory to clean up at deletion */
    CleanupList _cleanup;
    /** Flag to turn on/off monitoring of FECs */
    static bool _enabled;
    /** Flag to turn on/off loging */
    static std::ostream* _log_file;
  };
  //__________________________________________________________________
  template <typename T, typename Trait> 
  inline Service*
  Fec::AddService(const std::string& name, unsigned char addr) 
  {
    DGUARD(2,"Adding service %s with addres 0x%x", name.c_str(), addr);
    std::string n = MakeServiceName(name);
    // std::cout << "New service name is " << n.c_str() << std::endl;
    Service* s = 0;
    _cleanup.push_back(s = new FecService<T,Trait>(*this, n, addr));
    if (!s) { 
      std::cerr << "Failed to make FecService " << n << std::endl;
      _cleanup.pop_back();
      return 0;
    }
    if (!AddService(*s)) { 
      delete s;
      _cleanup.pop_back();
      return 0;
    }
    return s;
  }
  //__________________________________________________________________
  template <typename T, typename Trait> 
  inline Service*
  Fec::AddBusService(const std::string& name, unsigned char addr) 
  {
    DGUARD(2,"Adding service %s with addres 0x%x", name.c_str(), addr);
    std::string n = MakeServiceName(name);
    // std::cout << "New service name is " << n.c_str() << std::endl;
    Service* s = 0;
    _cleanup.push_back(s = new BusService<T,Trait>(*this, n, addr));
    if (!s) { 
      std::cerr << "Failed to make BusService " << n << std::endl;
      _cleanup.pop_back();
      return 0;
    }
    if (!AddService(*s)) { 
      delete s;
      _cleanup.pop_back();
      return 0;
    }
    return s;
  }
  //==================================================================
  template <typename T, typename Trait>
  inline 
  Fec::FecService<T,Trait>::FecService(Fec& fec, const std::string& name, 
				       unsigned char addr)
    : _serv(name), _addr(addr), _fec(fec)
  { 
    DGUARD(1,"Constructing service %s with address 0x%x", 
	   name.c_str(), addr);
    _service = &_serv;
  }
  //__________________________________________________________________
  template <typename T, typename Trait>
  inline 
  Fec::FecService<T,Trait>::~FecService() { }
  //__________________________________________________________________
  template <typename T, typename Trait>
  inline int
  Fec::FecService<T,Trait>::Update() 
  {
    DGUARD(3,"Updating FEC service %s", _serv.Name().c_str());
    unsigned short value   = 0x0; 
    int            ret     = _fec.Read(_addr, value); 
    int            qual    = 0;
    int            oldQual = _serv.Quality();
    switch (ret) { 
    case -Rcu::NOT_ACTIVE:     qual |= 0x1; break;
    case -Rcu::NO_ANSWER:      qual |= 0x2; break;
    default:                   qual |= 0x4; break;
    }
    T            val;
    unsigned int temp = value;
    memcpy(&val, &temp, sizeof(temp));
    _serv = val;

    if (qual != 0 || qual != oldQual) _serv.ForceUpdate();
    _serv.SetQuality(qual);
    
    if (_serv.Updated() && Fec::_enabled) 
      Fec::LogService("0x%03x,0x%04x,'%s',0x%05x,0x%02x,%d", _fec._address,
		      _addr, _serv.Name().c_str(), value, qual, ret);
    return ret;
  }
  //==================================================================
  template <typename T, typename Trait>
  inline 
  Fec::BusService<T,Trait>::BusService(Fec& fec, const std::string& name, 
				       unsigned char addr)
    : _serv(name), _addr(addr), _fec(fec)
  { 
    DGUARD(4,"Constructing service %s with address 0x%x", name.c_str(), addr);
    _service = &_serv;
  }
  //__________________________________________________________________
  template <typename T, typename Trait>
  inline 
  Fec::BusService<T,Trait>::~BusService() { }
  //__________________________________________________________________
  template <typename T, typename Trait>
  inline int
  Fec::BusService<T,Trait>::Update() 
  {
    DGUARD(4,"Updating BUS service %s", _serv.Name().c_str());
    unsigned short value; 
    unsigned int temp;
    int ret = _fec.ReadBus(_addr, value); 
    T val;
    temp = value;
    memcpy(&val, &temp, sizeof(temp));
    _serv = val;
    int qual = 0;
    if (ret == -EBADF)      qual |= 0x1; // Not open
    if (ret == -EINVAL)     qual |= 0x2; // Invalid
    if (ret == -ETIMEDOUT)  qual |= 0x4; // Not enough data
    _serv.SetQuality(qual);
    return ret;
  }

  //__________________________________________________________________
  /** @class FecFactory rcuce_fec.hh <rcuce/rcuce_fec.hh>
      @brief Class to make Fec objects.  Users should derive from this
      class to make new Fec objects of their kind 
      @ingroup rcuce_fecs */
  class FecFactory 
  {
  public:
    /** Make a FEC.  If the fec already exists, a pointer to the
	existing FEC is returned. */
    virtual Fec* Make(unsigned char addr);
    /** Remove a FEC. */ 
    void Remove(unsigned char addr);
    virtual ~FecFactory() {}
  protected:
    /** Abstract member function to make a Fec. 
	@param addr Address of the FEC. Users should overload this
	member function. 
	@return A newly allocated object of class (derived from) Fec */
    virtual Fec* MakeFec(unsigned char addr) = 0;
    /** Constructor 
	@param m Reference to fee server main 
	@param rcu Reference to RCU */
    FecFactory(FeeServer::Main& m, Rcu& rcu);
    /** Reference to FeeServer::Main. */
    FeeServer::Main& _main;
    /** REference to RCU */
    Rcu& _rcu;
    /** Type of map. */
    typedef std::map<unsigned char, Fec*> FecMap;
    /** Map of FECs */
    FecMap _fecs;
  };
}

#endif
//
// EOF
//
    
