#include <rcuce/rcuce_bfsm.hh>
#include <feeserverxx/fee_dbg.hh>
#include <iostream>

namespace RcuCE
{
  //____________________________________________________________________
  struct BasicFsmPlotter : public StateMachinePlotter
  {
    BasicFsmPlotter(const BasicFsm& fsm) : StateMachinePlotter(fsm) {}
    const char* StateName(const State& s) const { 
      static std::string n;
      n = BasicFsm::State2String(s);
      return n.c_str();
    }
    const char* ActionName(const Action& s) const { 
      static std::string n;
      n = BasicFsm::Action2String(s);
      return n.c_str();
    }
    const char* StateComment(const State& s) const { 
      using namespace CommonStates;
      using namespace CommonActions;
      switch (s) { 
      case Idle:        return "Off?";
      case Standby:     return "Starting\\nstate";
      case Error:       return "Can be reached\\nfrom all states";
      case Downloading: return "Transitional\\nstate";
      case Ready:       return "Meaning\\nvaries";
      }
      return StateMachinePlotter::StateComment(s);
    }
  };
}
//____________________________________________________________________
int
main()
{
  std::cout << RcuCE::BasicFsmPlotter(RcuCE::BasicFsm(0)) << std::endl;
  return 0;
}

  
//____________________________________________________________________
//
// EOF
//



  
