#ifdef HAVE_EXECINFO_H
# include <execinfo.h>
#endif
#include <iostream>
#include <signal.h>
#include <cstring>

inline void 
print_trace()
{
# ifdef HAVE_BACKTRACE
  const int isize = 256;
  void* buf[isize];
  // Get list of function calls 
  int osize = backtrace(buf, isize);
  
  // Print to stderr (fd=2)
  backtrace_symbols_fd(buf, osize, 2);
#endif
}

inline void
signal_handler(int signum)
{
  std::cerr << "Recieved signal '" << strsignal(signum) << "' (" 
	    << signum << ")" << std::endl;
  
  switch (signum) {
  case SIGHUP:
  case SIGINT:
  case SIGTERM:
  case SIGABRT:
    exit(1);
  default:
    // Print a back-trace
    print_trace();
    break;
  }

  // Terminate, and signal we should be restarted.
  exit(1);
}

inline void
install_signal_handler()
{
  // These will not cause a back-trace
  signal(SIGINT,  signal_handler);
  signal(SIGHUP,  signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGABRT, signal_handler);
  signal(SIGQUIT, signal_handler);
  // These will cause a back-trace
  signal(SIGILL,  signal_handler);
  signal(SIGSEGV, signal_handler);
  signal(SIGFPE,  signal_handler);
  signal(SIGBUS,  signal_handler);
  signal(SIGSYS,  signal_handler);
}

  
