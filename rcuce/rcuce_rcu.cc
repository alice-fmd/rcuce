#include <rcuce/rcuce_rcu.hh>
#include <rcuce/rcuce_fec.hh>
#include <feeserverxx/fee_dbg.hh>
#include <rcuce/rcuce_bits.hh>
#include <rcuce/rcuce_cb.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_servt.hh>
#include <unistd.h>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <cstdarg>
#include <cstdio>
#define RCU_HWADDR_MASK       0x001ff000
#define RCU_HWADDR_PART_MASK  0x00007000
#define RCU_HWADDR_SEC_MASK   0x001f8000
#define RCU_SEQ_BUSY          0x80000000
#define PATTERN_MISSMATCH     0x01
#define SEQ_ABORT             0x02
#define FEC_TIMEOUT           0x04
#define ALTRO_ERROR           0x08
#define ALTRO_HWADD_ERROR     0x10


namespace {
  bool isInRange(const RcuCE::CodeBook::Memory& memory, 
		 unsigned int address) 
  {
    if (memory._address == RcuCE::CodeBook::_invalid) return false;
    size_t low  = memory._address;
    size_t high = low + memory._size;
    if (address >= low && address < high) return true;
    return false;
  }
  bool isInRange(const RcuCE::CodeBook::Memory& memory, 
		 unsigned int address, unsigned int size) 
  {
    if (memory._address == RcuCE::CodeBook::_invalid) return false;
    if (isInRange(memory, address))            return true;
    if (isInRange(memory, address + size - 1)) return true;
    size_t low  = memory._address;
    size_t high = low + memory._size;
    if (address < low && address + size - 1 > high) return true;
    return false;
  }
}
#define IN_HL(A,R) do { \
  R = isInRange(_code_book._hit_list, A); \
  if (R) WARN(_main, ("Address 0x%04x is in the HITLIST range", A)); \
  } while (false) 
#define IN_HL_RANGE(A,S,R) do {		  \
  R = isInRange(_code_book._hit_list, A, S);				\
  if (R) WARN(_main, ("Address 0x%04x to 0x%04x overlaps the HITLIST range",A,A+S)); \
  } while (false) 



    
    
//====================================================================
//
// RCU Services
std::ostream* RcuCE::RcuService::_log_file = 0;
void
RcuCE::RcuService::LogService(const char* format, ...)
{
  if (!_log_file) return; // Do nothing 
  va_list ap;
  va_start(ap, format);
  char buf[1024];
  vsnprintf(buf, sizeof(buf)-1, format, ap);
  va_end(ap);

  time_t     epoch_now = time(NULL);
  struct tm* local_now = localtime(&epoch_now);
  char dbuf[20];
  strftime(dbuf, sizeof(dbuf)-1, "%Y-%m-%d %H:%M:%S", local_now);
  dbuf[19] = '\0';
  (*_log_file) << dbuf << "," << buf << std::endl;
}
//____________________________________________________________________
namespace RcuCE
{
  /** Error and status register service 
      @ingroup rcuce_rcu_serv */
  struct ERRSTService : public RcuService 
  {
    /** Constructor
	@param rcu Reference to RCU */
    ERRSTService(RcuCE::Rcu& rcu) 
      : RcuService(rcu), _serv("ERRST") 
    { _service = &_serv; }
    /** Get the new value from the RCU 
	@return true on success */
    int Update() 
    {
      DGUARD(9,"ERRST updating");
      unsigned int value;
      _rcu.ReadERRST(value);
      _serv = value;
      if (_serv.Updated()) LogService("0xFFF,0xFF,'ERRST',0x%05x,0", value);
      return 0;
    }
    /** Get the value 
	@return current value */
    unsigned int Value() const { return _serv.Value(); }
  protected: 
    /** Service */
    FeeServer::IntService _serv;
  };
  //____________________________________________________________________
  /** Active front-end card list 
      @ingroup rcuce_rcu_serv */
  struct AFLService : public RcuService 
  {
    /** Constructor
	@param rcu Reference to RCU */
    AFLService(RcuCE::Rcu& rcu) 
      : RcuService(rcu), 
	_serv("AFL"), 
	_processed(true), 
	_last(0x0)
    { _service = &_serv; }
    /** Get the new value from the RCU 
	@return true on success */
    int Update() 
    {
      DGUARD(10,"AFL updating");
      unsigned int value = 0;
      unsigned int old   = _serv.Value();
      byte_array dat(4);
      // unsigned int addr  = _rcu._code_book._afl._address;
      int ret = _rcu.Read(_rcu._code_book._afl ,value);
      DMSG(10, "Read of AFL returned %d", ret);
      if (ret < 0) return ret;
      _serv = value;
      if (_serv.Updated()) _processed = false;
      if (!_processed) {
	_last = old;
	DMSG(10,"AFL=0x%08x (was 0x%08x)", value, _last);
      }
      if (_serv.Updated()) 
	LogService("0xFFF,0x%04x,'AFL',0x%05x,0", 
		   _rcu._code_book._afl._address, value);
      return 0;
    }
    /** Get value 
	@return current value */
    unsigned int Value() const { return _serv.Value(); }
    /** Get the old value */
    unsigned int Last() const { return _last; }
    /** True if we're updated 
	@return true if we're updated */
    bool Updated() const { return !_processed; }
    /** Flag that we have processed this value */
    void Processed() { _processed = true; }
  protected: 
    /** The service */
    FeeServer::IntService _serv;
    /** Whether we have been processed */
    bool _processed;
    /** Last value */ 
    unsigned int _last;
  };

  struct RUNService : public RcuService 
  {
    RUNService(RcuCE::Rcu& rcu) 
      : RcuService(rcu),
	_in_run(false), 
	_last(false)
    {
      _service = 0;
    }
    int Update()
    {
      unsigned int ttc_ctrl;
      int ret  = _rcu.Read(_rcu._code_book._ttc_control,ttc_ctrl);
      if (ret < 0) return ret;
      _in_run  = (ttc_ctrl & (1 << 17)) != 0x0;
      if (_in_run != _last) 
	LogService("0xFFF,0x%04x,'TTC_CONTROL',0x%05x,0", 
		   _rcu._code_book._ttc_control._address, _in_run);
      _last = _in_run;
      return ret;
    }
    bool _in_run;
    bool _last;
  };
  struct TRIGService : public RcuService 
  {
    TRIGService(RcuCE::Rcu& rcu) 
      : RcuService(rcu),
	_is_on(false),
	_last(false)
    {
      _service = 0;
    }
    int Update()
    {
      unsigned int trg_conf;
      int ret = _rcu.Read(_rcu._code_book._trg_conf,trg_conf);
      if (ret < 0) return ret;
      _is_on  = ((trg_conf >> 14) & 0x4) != 0x0;
      if (_is_on != _last) 
	LogService("0xFFF,0x%04x,'TRG_CONF',0x%05x,0", 
		   _rcu._code_book._trg_conf._address, _is_on);
      _last = _is_on;
      return ret;
    }
    bool _is_on;
    bool _last;
  };
      
  //____________________________________________________________________
  struct HITLISTService : public RcuService
  {
    HITLISTService(RcuCE::Rcu& rcu) 
      : RcuService(rcu),
	_is_ok(true),
	_last(true)
    {
      _service = 0;
    }
    /** Get the new value from the RCU 
	@return true on success */
    int Update() 
    {
      DGUARD(9,"HITLIST updating");
      static int count = 0;
      count++;
      
      if (count < 3) return 0;
      
      _is_ok = true;
      count = 0;
      byte_array data;
      size_t n = 0;
      int ret = _rcu.Read(_rcu._code_book._hit_list, data, n,
			  _rcu._code_book._hit_list._size);
      if (ret < 0) return ret;
      for (size_t i = 0; i < n; i++) 
	if (data[i] != 0xff) _is_ok = false;
      if (_is_ok != _last) 
	LogService("0xFFF,0x%04x,'HITLIST',0x%05x,0", 
		   _rcu._code_book._hit_list._address, _is_ok);
      _last = _is_ok;
      return 0;
    }
    bool _is_ok;
    bool _last;
  };
  //__________________________________________________________________
  struct TEMPService : public RcuService 
  {
    TEMPService(RcuCE::Rcu& rcu, bool dcs) 
      : RcuService(rcu), 
	_offset(dcs ? 0 : 4),
	_is_init(false),
	_serv((dcs ? "TEMP_DCS" : "TEMP_PR"))
    { _service = &_serv; }
    void Init()
    {
      std::cout << "Initializing " << _serv.Name() << std::endl;
      int ret = 0;
      unsigned int w1 = 0x11   + _offset;
      unsigned int w2 = 0x51   + _offset;
      unsigned int w3 = 0x5533 + _offset;
      unsigned int w4 = 0x4b23 + _offset;
      if ((ret = _rcu.Write(_rcu._code_book._rcu_temp_cmd, w1) < 0)) return;
      if ((ret = _rcu.Write(_rcu._code_book._rcu_temp_cmd, w2) < 0)) return;
      if ((ret = _rcu.Write(_rcu._code_book._rcu_temp_cmd, w3) < 0)) return;
      if ((ret = _rcu.Write(_rcu._code_book._rcu_temp_cmd, w4) < 0)) return;
      _is_init = true;
    }
    int Update()
    {
      if (!_is_init) return -1;
      int ret  = 0;
      unsigned int w1 = 0xFF000000 + _offset;
      if ((ret = _rcu.Write(_rcu._code_book._rcu_temp_cmd,w1)) < 0) return ret;

      unsigned int temp;
      if ((ret = _rcu.Read(_rcu._code_book._rcu_temp_val,temp) < 0)) return ret;
      // Contents of the register 
      //   bits        Meaning 
      //  ----------+-------------------------------------
      //   0-5         Not used 
      //   6-7         Fractional temperature (0.25C step) 
      //   8-15        Temperature (1C step) 
      unsigned int value = 4*((temp >> 8) & 0xFF) + ((temp >> 6) & 0x3);
      _serv = value;
      if (_serv.Updated()) 
	LogService("0xFFF,0x%04x,'%s',0x%05x,0",
		   _rcu._code_book._rcu_temp_val._address, 
		   _serv.Name().c_str(),
		   _serv.Address(), value);
      return ret;
    }
    /** command offset */
    unsigned int _offset;
    bool _is_init;
    /** The service */
    FeeServer::IntService _serv;
  };
}
//____________________________________________________________________
/** Status service 
    @ingroup rcuce_rcu_serv */
struct STATUSService : public RcuCE::RcuService 
{
  /** Internal class  
      @ingroup rcuce_rcu_serv */
  struct Serv : public FeeServer::ServiceBase 
  {
    /** Internal class  */
    struct Value { 
      /** Number of entries valid */
      // unsigned int _n;
      /** Entries */
      unsigned int _fec[32];
    }  _value;

    /** Constructor */
    Serv() 
      : FeeServer::ServiceBase("FEC_STATUS"), 
	_value()
    {
      // _value._n  = 0;
      for (size_t i = 0; i < 32; i++) _value._fec[i] = 0;
    }
    /** Return the format to use when declaring the DIM service */
    const char* Format() const { return "I:32"; }
    /** Return the size (in bytes) of the service */ 
    size_t Size() const { return sizeof(Value); }
    /** Return the address of the data */
    void* Address() { return &_value; }
  };

  /** Constructor
      @param rcu Reference to RCU */
  STATUSService(RcuCE::Rcu& rcu) 
    : RcuService(rcu), 
      _serv() 
  { 
    _service = &_serv; 
  }
  int Update() 
  {
    DGUARD(4,"STATUS updating");
    int          ret     = 0;
    unsigned int smem_st = 0;
    if (_rcu._code_book._smem_status._address != _rcu._code_book._invalid) {
      if ((ret = _rcu.Read(_rcu._code_book._smem_status, smem_st)) < 0) 
	return ret;
      bool rdy_A = ((smem_st >> 4) & 0x1) == 0x1;
      bool rdy_B = ((smem_st >> 9) & 0x1) == 0x1;
      if (!rdy_A || !rdy_B) return 0;
    }

    const size_t uint_size = sizeof(unsigned int);
    RcuCE::byte_array data(33 * uint_size);
    size_t osize = 0;
    if ((ret < _rcu.Read(_rcu._code_book._status,data,osize,
			 _rcu._code_book._status._size)) < 0) return ret;
    std::vector<unsigned int> words(_rcu._code_book._status._size);
    memcpy(&(words[0]), &(data[0]), uint_size*words.size());

    if (_rcu._code_book._smem_status._address == _rcu._code_book._invalid) {
      size_t n = words[0];
      size_t i = 0;
      for (i = 0; i < n; i++) 
	_serv._value._fec[i] = ((1 << 31) | words[i+1]);
      for (; i < 32; i++) _serv._value._fec[i] = 0;
    }
    else {
      size_t n_a = (smem_st >> 0) & 0xF;
      size_t n_b = (smem_st >> 5) & 0xF;
      for (size_t i = 0; i < n_a; i++) 
	_serv._value._fec[i] = ((1 << 31) | words[i]);
      for (size_t i = n_a; i < 16; i++) 
	_serv._value._fec[i] = 0;
      for (size_t i = 16; i < n_b+16; i++) 
	_serv._value._fec[i] = ((1 << 31) | words[i]);
      for (size_t i = n_b+16; i < 32; i++) 
	_serv._value._fec[i] = 0;
      
      // Clear ready bits 
      if ((ret = _rcu.Exec(_rcu._code_book._rs_smem)) < 0) return ret;
    }
    return 0;
  }
protected: 
  /** The service */
  Serv _serv;
};

      

//====================================================================
RcuCE::Rcu::Rcu(Dcsc& dcsc, FeeServer::Main& main) 
  : ServiceProvider(main, 3, 100), 
    CommandHandler(RcuId),
    BasicFsm(FsmId::Rcu, CommonStates::Idle),
    _code_book(),
    _dcsc(dcsc), 
    _fw_version(0),
    _auto_check(false),
    _factory(0),
    _state_service("RCU_STATE",*this), 
    _errst_service(0), 
    _afl_service(0), 
    _status_service(0),
    _hitlist_service(0),
    _trig_service(0),
    _run_service(0),
    _dcs_temp_service(0), 
    _pr_temp_service(0),
    _enable_flick(true),
    _current_master(master_unknown)

{
  if (!_dcsc.IsOpen()) return;
  int ret = 0;
  // Add some states 
  AddState(CommonStates::Mixed);
  AddState(CommonStates::Running);

  // SetState(CommonStates::Idle);
  for (int i = 0; i < 3; i++) { // Try three times to get FW version
    if ((ret = FindFwVersion(true)) < 0) { 
      DEBUG(_main, ("FW version retrivial failed %s", strerror(-ret)));
      usleep(1000); // Sleep for 1msec
      continue; // return;
    }
    break;
  }
  if (_fw_version == 0) { 
    std::cerr << "Failed to ge the firmware version after 3 tries" << std::endl;
    return;
  }
  std::cout << "RCU firmware version seems to be 0x" 
	    << std::hex << _fw_version << std::dec << std::endl;
  // SetState(CommonStates::Standby);
  // SetState(CommonStates::Mixed);
  _code_book.Configure(_fw_version);
  // _code_book.Print();
  // DcsMaster();
  UpdateState();

  // Add state service 
  if (!AddService(_state_service)) 
    WARN(_main, ("Failed to add service RCU_STATE"));

  // Add ERRor and STatus service - if available
  if (_code_book._errst._address != _code_book._invalid) {
    _errst_service  = new ERRSTService(*this);
    if (!AddService(*_errst_service)) 
      WARN(_main, ("Failed to add service ERRST"));
  }

  
  // Add the AFL service 
  _afl_service    = new AFLService(*this);
  if (!AddService(*_afl_service)) 
    WARN(_main, ("Failed to add service AFL"));
}

//____________________________________________________________________
void
RcuCE::Rcu::MonitorStatusMemory()
{
  // Add HITList service 
  if (_code_book._status._address == _code_book._invalid) return;
  if (_status_service) return;

  // Add the status service
  _status_service = new STATUSService(*this);
  if (!AddService(*_status_service)) 
    WARN(_main, ("Failed to add service STATUS"));
}

//____________________________________________________________________
void
RcuCE::Rcu::MonitorHitList()
{
  // Add HITList service 
  if (_code_book._hit_list._address == _code_book._invalid) return;
  if (_hitlist_service) return;
  
  _hitlist_service  = new HITLISTService(*this);
  if (!AddService(*_hitlist_service)) 
    WARN(_main, ("Failed to add service HITLIST"));
}

//____________________________________________________________________
void
RcuCE::Rcu::MonitorTrigConf()
{
  if (_code_book._trg_conf._address == _code_book._invalid) return;
  if (_trig_service) return;

  _trig_service = new TRIGService(*this);
  if (!AddService(*_trig_service)) 
    WARN(_main, ("Failed to add service TRIG"));
}

//____________________________________________________________________
void
RcuCE::Rcu::MonitorRunState()
{
  if (_code_book._ttc_control._address == _code_book._invalid) return;
  if (_run_service) return;

  _run_service = new RUNService(*this);
  if (!AddService(*_run_service)) 
    WARN(_main, ("Failed to add service RUN"));
}

//____________________________________________________________________
void
RcuCE::Rcu::MonitorTemps()
{
  // Add Temperature services - if available
  if (_code_book._rcu_temp_cmd._address == _code_book._invalid) {
    WARN(_main, ("Cannot monitor temperatures - not available in FW"));
    return;
  }

  INFO(_main, ("Will monitor RCU temperatures"));
  if (!_dcs_temp_service) {
    _dcs_temp_service  = new TEMPService(*this, true);
    if (!AddService(*_dcs_temp_service)) 
      WARN(_main, ("Failed to add service DCS_TEMP"));
    else 
      _dcs_temp_service->Init();
  }

  if (!_pr_temp_service) {
    _pr_temp_service  = new TEMPService(*this, false);
    if (!AddService(*_pr_temp_service)) 
      WARN(_main, ("Failed to add service PR_TEMP"));
    else 
      _pr_temp_service->Init();
  }
}
//____________________________________________________________________
RcuCE::Rcu::~Rcu()
{
  if (_errst_service)   delete _errst_service;
  if (_afl_service)     delete _afl_service;
  if (_status_service)  delete _status_service;
  if (_hitlist_service) delete _hitlist_service;
  if (_trig_service)    delete _trig_service;
  if (_run_service)     delete _run_service;
  if (_dcs_temp_service)delete _dcs_temp_service;
  if (_pr_temp_service) delete _pr_temp_service;
}

//====================================================================
//
// Service handling 
//____________________________________________________________________
int
RcuCE::Rcu::UpdateBasic()
{
  int ret = 0;

  ret = UpdateState();
  
  if (_errst_service) {
    // INFO(_main, ("RCU Updating ERRST..."));
    if ((ret = _errst_service->Update()) < 0) 
      WARN(_main, ("Failed to update ERRST service"));
    if ((_errst_service->Value() & ~RCU_HWADDR_MASK) != 0) { 
      WARN(_main, ("ERRST says that the RCU is in error 0x%x", 
		   _errst_service->Value()));
      ret = -1;
      // SetState(CommonStates::Error);
    }
  }
  if (_dcs_temp_service) { 
    if ((ret = _dcs_temp_service->Update()) < 0) 
      WARN(_main, ("Failed to update DCS_TEMP service"));
  }
  if (_pr_temp_service) { 
    if ((ret = _pr_temp_service->Update()) < 0) 
      WARN(_main, ("Failed to update DCS_TEMP service"));
  }

  // INFO(_main, ("RCU Updating AFL..."));
  //unsigned int old_afl = (_afl_service ? _afl_service->Value() : 0);
  static int afl_errors = 0;
  if (ret >= 0 && _afl_service && (ret = _afl_service->Update()) < 0) {
    WARN(_main, ("Failed to update AFL service"));
    afl_errors++;
    if (afl_errors < 3) { 
      WARN(_main, ("Ignoring this failure"));
      ret = 0;
    }
  }
  else 
    afl_errors = 0;
  
  // INFO(_main, ("RCU Updating STATUS..."));
  if (ret >= 0 && _status_service && (ret = _status_service->Update()) < 0) {
    WARN(_main, ("Failed to update STATUS service"));
  }

  // INFO(_main, ("RCU Updating STATUS..."));
  if (ret >= 0  && (ret = _state_service.Update()) < 0) {
    WARN(_main, ("Failed to update STATE service"));
  }
  return CheckFaults(ret);
}

//____________________________________________________________________
int
RcuCE::Rcu::UpdateSome()
{
  // int ret = 0;
  // if ((ret = UpdateBasic()) < 0) return ret;
  int ret = UpdateBasic();
  if (ret < 0) return CheckFaults(ret);
  
  // INFO(_main, ("RCU Updating Providers..."));
  // Will not update nested providers if we failed above. 
  for (ProviderList::iterator p=_providers.begin(); p != _providers.end(); ++p)
    if ((ret = (*p)->UpdateSome()) < 0) break;
  return CheckFaults(ret);
}

//____________________________________________________________________
int
RcuCE::Rcu::UpdateAll()
{
  INFO(_main, ("RCU Updating..."));
  DGUARD(4,"RCU updating all");
  // std::cout << "Updating all " << std::endl;
  
  // int ret = 0;
  // if ((ret = UpdateSome()) < 0) return ret;
  int ret = UpdateBasic();
  
  if (ret >= 0 && _afl_service && _factory && _afl_service->Updated()) {
    DMSG(2,"AFL is 0x%08x (factory: %p", _afl_service->Value(), _factory);

    // Give a warning if this is changing while we're in ready!
    // Note, that this might be legitimate 
    if (GetState() == CommonStates::Ready) {
      WARN(_main, ("Change of AFL from 0x%08x to 0x%08x while in READY", 
		   _afl_service->Last(), _afl_service->Value()));
    }

    // Let's suspend the monitor thread entirely here
    _main.MonThread().Suspend();
    
    // Here, we should update our list of service provides to enable
    // new cards, or remove cards that have disappeared. 
    unsigned int afl_value = _afl_service->Value();
    for (size_t i = 0; i < sizeof(afl_value)*8; i++) {
      if ((1 << i) & afl_value) _factory->Make(i); 
      else                      _factory->Remove(i);
    }
    _afl_service->Processed();

    // And then re-enable the monitoring thread here
    _main.MonThread().Resume();
  }
  for (ProviderList::iterator p=_providers.begin(); p != _providers.end(); ++p){
    UpdateState();
    if ((ret = (*p)->UpdateAll()) < 0) break;
  }
  return CheckFaults(ret);
}

//====================================================================
//
// State handling
//____________________________________________________________________
int
RcuCE::Rcu::UpdateState()
{
  int          ret         = 0;
  bool         ttc_enabled = false;
  bool         run_active  = false;
  State        next        = CommonStates::Error;
  if (_trig_service) { 
    _trig_service->Update();
    ttc_enabled = _trig_service->_is_on;
  }
  if (_run_service) {
    _run_service->Update();
    run_active = _run_service->_in_run;
  }

  // SetState(CommonStates::Mixed);
  if (ttc_enabled && run_active) { 
    next = CommonStates::Running;
    DdlMaster();
  }    
  else if (ttc_enabled) { 
    next = CommonStates::Ready;
    // DdlMaster();
    DcsMaster();
  }
  else {
    next = CommonStates::Standby;
    DcsMaster();
  }
  if (_hitlist_service) { 
    _hitlist_service->Update();
    if (ttc_enabled && !_hitlist_service->_is_ok) {
      WARN(_main, ("Hit list is corrupted - ERROR state"));
      next = CommonStates::Error;
      DcsMaster();
    }
  }
  if (_n_faults >= _max_faults) { 
    WARN(_main, ("more than allowed errors seen - ERROR state"));
    next = CommonStates::Error;
    DcsMaster();
  }

  if (next != GetState()) { 
    INFO(_main, ("Next state is %s (TTC %s)", State2String(next).c_str(), 
		 ttc_enabled ? "enabled" : "disabled"));
    SetState(next);
  }
  return ret;
}

  

//____________________________________________________________________
void
RcuCE::Rcu::Transition(const State&  src, 
		       const Action& action, 
		       const State&  dest)
{
  DMSG(4, "Switching from state %d to state %d via action %d", 
       src, dest, action);
  if (dest == CommonStates::Ready  ||
      dest == CommonStates::Running) 
    DdlMaster();
  else                               
    DcsMaster();

  if ((src == CommonStates::Ready  || 
       src == CommonStates::Running) &&
      dest == CommonStates::Idle)  { 
    WriteAFL(0x0);
    Reset();
  }

  if ((src == CommonStates::Ready  || 
       src == CommonStates::Running) &&
      dest == CommonStates::Standby)  { 
    Reset();
  }
}
//____________________________________________________________________
void
RcuCE::Rcu::Reset()
{
  ResetERRST();
  ResetERRREG();
  ResetRESREG();
  if (_code_book._reset_trcnt._address != _code_book._invalid) 
    Exec(_code_book._reset_trcnt);
  if (_code_book._reset_trcfg._address != _code_book._invalid) 
    Exec(_code_book._reset_trcfg);
  // if (_code_book._reset_rcu._address != _code_book.invliad) 
  //   Exec(_code_book._reset_rcu);
}
    
//====================================================================
//
// Command handling 
//____________________________________________________________________
int
RcuCE::Rcu::Handle(unsigned char cmd, unsigned short parameter,
		   byte_ptr idata, size_t isize, 
		   byte_array& odata, size_t& osize) 
{
  // DEBUG(_main, ("Handling 0x%x", cmd));
  DGUARD(5, "RCU Handling cmd=0x%x, param=%04x idata=%p, isize=%d, "
	 "odata=%p, osize=%d[%d]", cmd, parameter, &(idata[0]), isize,
	 &(odata[0]), osize, odata.size());
  int ret = 0;
  Dcsc::WordSize ws = Dcsc::Word32;
  switch (cmd) {
    /** Memories */
  case EXEC_INSTRUCTION:
    DMSG(5,"RCU got execute instruction");
  case WRITE_INSTRUCTION: 
    DMSG(5,"RCU got write instruction");
    ret = Write(_code_book._imem, idata, isize, parameter); 
    if (ret >= 0 && cmd == EXEC_INSTRUCTION) ExecEXEC(0);
    break;
  case READ_INSTRUCTION:  
    DMSG(5,"RCU got read instruction");
    ret = Read(_code_book._imem, odata, osize, parameter);  break;
  case WRITE_PATTERN32:   // Fall through
  case WRITE_PATTERN16:   ws = Dcsc::Word16; // Fall through
  case WRITE_PATTERN8:    ws = Dcsc::Word8;  // Fall through
  case WRITE_PATTERN10:   ws = Dcsc::Word8;  
    DMSG(5,"RCU got write pattern instruction");
    ret = Write(_code_book._pmem, idata, isize, parameter, ws); break;
  case READ_PATTERN:      
    DMSG(5,"RCU got read pattern instruction");
    ret = Read(_code_book._pmem, odata, osize, parameter);   break;
  case WRITE_RESULT:      
    DMSG(5,"RCU got write result instruction");
    ret = Write(_code_book._rmem, idata, isize, parameter);  break;
  case READ_RESULT:       
    DMSG(5,"RCU got read result instruction");
    ret = Read(_code_book._rmem, odata, osize, parameter);   break;
  case WRITE_ACL:         
    DMSG(5,"RCU got Write ACL instruction");
    ret = Write(_code_book._acl, idata, isize, parameter);   break;
  case READ_ACL:          
    DMSG(5,"RCU got read ACL instruction");
    ret = Read(_code_book._acl, odata, osize, parameter);    break;
  case WRITE_HEADER:      
    DMSG(5,"RCU got write header instruction");
    ret = Write(_code_book._header, idata, isize, parameter);break;
  case READ_HEADER:       
    DMSG(5,"RCU got read header instruction");
    ret = Read(_code_book._header, odata, osize, parameter); break;
  case WRITE_MEMORY:      
    DMSG(5,"RCU got write memory instruction");
    ret = WriteBlock(parameter, 0xFFFFU, idata, isize, 1, ws); break;
  case READ_MEMORY:       
    DMSG(5,"RCU got read memory instruction");
    ret = ReadBlock(parameter, 0xFFFFU, odata, osize, 1); break;
  case WRITE_MEMBLOCK: {
    DMSG(5,"RCU got write memory block instruction");
    // First word is the address (parameter is the size) 
    size_t int_size = sizeof(unsigned int);
    unsigned int address = *((unsigned int*)idata);
    ret = WriteBlock(address, 0xffffu, idata+int_size, isize-1, 
		     parameter, ws);
    if (ret >= 0) ret += int_size;
  }
    break;
  case READ_MEMBLOCK: {
    DMSG(5,"RCU got read memory block instruction");
    // First word is the address (parameter is the size) 
    unsigned int address = *((unsigned int*)idata);
    ret = ReadBlock(address, 0xFFFFU, odata, osize, parameter);
    DMSG(5, "Read block address=0x%08x, osize=%d, param=%d -> %d", 
	  address, osize, parameter, ret);
    if (ret >= 0) ret = sizeof(unsigned int);
  }
    break;
    // Registers
  case READ_ERRST:   ret = Read(_code_book._errst, odata, osize); break;
  case WRITE_TRGCFG: ret = Write(_code_book._trcfg, idata, isize); break;
  case READ_TRGCFG:  ret = Read(_code_book._trcfg, odata, osize); break;
  case WRITE_PMCFG:  ret = Write(_code_book._pmcfg, idata, isize); break;
  case READ_PMCFG:   ret = Read(_code_book._pmcfg, odata, osize); break;
  case WRITE_AFL:    ret = Write(_code_book._afl, idata, isize); break;
  case READ_AFL:     ret = Read(_code_book._afl, odata, osize); break;
    // Commands 
  case RESET:           
    DMSG(5,"RCU got reset instruction");
    switch (parameter) { 
    case 1:  ret = Exec(_code_book._reset_fec);    break;
    case 2:  ret = Exec(_code_book._reset_rcu);    break;
    default: ret = Exec(_code_book._reset_global); break;
    }
    break;
  case L1TRG_SELECT:    
    DMSG(5,"RCU got L1 trigger select instruction");
    switch (parameter) {
    case 1:  ret = Exec(_code_book._enable_aux); break;
    case 2:  ret = Exec(_code_book._enable_ttc); break;
    default: ret = Exec(_code_book._enable_cmd); break;
    }
    break;
  case SEND_L1_TRIGGER: ret = Exec(_code_book._trigger_l1); break;
  case EXEC:            ret = ExecEXEC(parameter); break;
  case STOP_EXEC:       ret = ExecABORT(); break;
  case CHECK_ERROR:     ret = ExecCheck(odata, osize); break;
  case EN_AUTO_CHECK:   _auto_check = (parameter != 0); ret = 0; break;
#if 1
  case GET_FWVERS:      
    DMSG(5,"RCU got ge read FW version instruction");
    osize += 4;
    if (odata.size() < osize) odata.resize(osize);
    memcpy(&(odata[osize-4]), &_fw_version, 4);
    break;
#endif
  }
  DEXEC(5, 
	if (osize > 0) {
	  printf("Returning to caller\n\t");
	  for (size_t i = 0; i < osize; i++) {
	    if (i != 0 && i % 10 == 0) printf("\n\t%3d",int(i));
	    printf(" %02x", unsigned(odata[i]));
	  }
	  if (osize % 10 == 0) printf("\n");
	});
  DMSG(5, "Will return %d bytes to caller", osize);
  EndHandle();
  return ret;
}


//====================================================================
//
// Memory I/O
//____________________________________________________________________
int
RcuCE::Rcu::Write(CodeBook::Memory& m, byte_ptr idata, 
		  size_t isize, size_t n, Dcsc::WordSize wsize) 
{
  DGUARD(8, "Write(0x%04x,%p,%d,%d,%d)", 
	 m._address, idata, isize, n, wsize);
  return WriteBlock(m._address, m._size, idata, isize, n, wsize);
}
//____________________________________________________________________
int
RcuCE::Rcu::Read(CodeBook::Memory& m, byte_array& odata, 
		 size_t& osize, size_t n)
{
  DGUARD(8, "Read(0x%04x,%p,%d,%d)",
	 m._address, &(odata[osize]), osize, n);
  return ReadBlock(m._address, m._size, odata, osize, n);
}
//____________________________________________________________________
int
RcuCE::Rcu::Write(CodeBook::Register& r, byte_ptr idata, size_t isize)
{
  DGUARD(8,"Write(0x%04x,%p,%d)", r._address, idata, isize);
  if (r._address == CodeBook::_invalid) return -BAD_PARAMETER;
  return WriteBlock(r._address, 1, idata, isize, 1, Dcsc::Word32);
}
//____________________________________________________________________
int
RcuCE::Rcu::Write(CodeBook::Register& r, unsigned int val)
{
  DGUARD(8,"Write(0x%04x,%d)", r._address, val);
  if (r._address == CodeBook::_invalid) return -BAD_PARAMETER;

  // bool check = false;
  // IN_HL(r._address, check);
  // if (check) return -EINVAL;

  int ret = _dcsc.Write(r._address, val);
  return (ret < 0 ? -DCSC : ret);
}

//____________________________________________________________________
int
RcuCE::Rcu::Read(CodeBook::Register& r, byte_array& odata, size_t& osize)
{
  DGUARD(8,"Read(0x%04x,%p,%d)", r._address, &(odata[0]), osize);
  if (r._address == CodeBook::_invalid) return -BAD_PARAMETER;
  return ReadBlock(r._address, 1, odata, osize, 1);
}
//____________________________________________________________________
int
RcuCE::Rcu::Read(CodeBook::Register& r, unsigned int& ret)
{
  DGUARD(8,"Read(0x%04x,%p,%d)", r._address);
  if (r._address == CodeBook::_invalid) return -BAD_PARAMETER;
  int ret1 = _dcsc.Read(r._address, ret);
  
  return (ret1 < 0 ? -DCSC: ret1);
}

//____________________________________________________________________
int
RcuCE::Rcu::Exec(CodeBook::Command& c, unsigned int arg) 
{
  DGUARD(8,"Exec(0x%04x)", c._address);
  if (c._address == CodeBook::_invalid) return -BAD_PARAMETER;

  // bool check = false;
  // IN_HL(c._address, check);
  // if (check) return -EINVAL;

  int ret = _dcsc.Write(c._address, arg);
#if 0
  if (c._address == _code_book._reset_global._address || 
      c._address == _code_book._reset_rcu._address) {
    SetState(CommonStates::Standby);
  }
#endif
  return (ret < 0 ? -DCSC : ret);
}


//====================================================================
//
// Special I/O
//____________________________________________________________________
int
RcuCE::Rcu::ExecEXEC(unsigned int addr) 
{
  DGUARD(7,"ExecEXEC(%d)", addr);
  if (addr >= _code_book._imem._size) return -BAD_PARAMETER;
  // Perhaps we need a mutex?
  ResetERRST();
  int ret;
  if ((ret = Exec(_code_book._exec, addr)) < 0) return ret;
  if (!_auto_check) return ret;
  return CheckERRST();
}
//____________________________________________________________________
int
RcuCE::Rcu::ExecABORT() 
{
  DGUARD(7,"ResetABORT()");
  // Perhaps we need a mutex?
  int ret = Exec(_code_book._abort, 0);
  return (ret >= 0 ? 0 : ret);
}
//____________________________________________________________________
int
RcuCE::Rcu::ExecCheck(byte_array& ret, size_t& osize)
{
  DGUARD(7,"ExecCheck(%p,%d)", &(ret[0]), osize);
  int res = CheckERRST();
  if (ret.size() < osize + sizeof(res)) ret.resize(osize + sizeof(res));
  ret[osize++] = res;
  return res;
}
//____________________________________________________________________
int
RcuCE::Rcu::ResetERRST() 
{
  DGUARD(7,"ResetERRST()");
  if (_code_book._reset_errst._address == _code_book._invalid) return 0;
  return Exec(_code_book._reset_errst, 0);
}
//____________________________________________________________________
int
RcuCE::Rcu::ReadERRST(unsigned int& errst)
{
  DGUARD(7,"ReadERRST(%d)",errst);
  errst = 0;
  if (_code_book._errst._address == _code_book._invalid) return 0;
  return Read(_code_book._errst,errst);
}
//____________________________________________________________________
int
RcuCE::Rcu::CheckERRST()
{
  if (_code_book._errst._address == _code_book._invalid) return SUCCESS;
  unsigned int errst;
  int          ret  = 0;
  size_t       busy = 0;
  const size_t max  = 3;
  //size_t     iter = 0;
  do { 
    if ((ret = Read(_code_book._errst, errst)) < 0) continue;
    errst  &= ~RCU_HWADDR_MASK;
    if (errst <= 0) break;

    if (errst & RCU_SEQ_BUSY) {
      if (++busy < max) continue;
      if (busy == max) {
	usleep(10);
	continue;
      }
      ret = -EBUSY;
    }
    else if (errst & SEQ_ABORT)         ret = -IMEM_ABORTED;
    else if (errst & FEC_TIMEOUT)       ret = -BUS_TIMEOUT;
    else if (errst & ALTRO_ERROR)       ret = -BUS_ERROR;
    else if (errst & ALTRO_HWADD_ERROR) ret = -ADDRESS_ERROR;
    else if (errst & PATTERN_MISSMATCH) ret = -BAD_PATTERN;
  } while (ret >= 0 && errst > 0);
  return ret;
}
//____________________________________________________________________
int
RcuCE::Rcu::ReadAFL(unsigned int& ret)
{
  DGUARD(4,"ReadAFL(%d)", ret);
  return Read(_code_book._afl, ret);
}
//____________________________________________________________________
int
RcuCE::Rcu::WriteAFL(const unsigned int& val)
{
  DGUARD(4,"WriteAFL(%d)", val);
  int ret =  Write(_code_book._afl,val);
#ifdef DUPLICATE_TO_SC_ACTFEC
  if (ret == 0 && 
      (_code_book._afl._address != _code_book._sc_actfec._address)) 
    ret = _dcsc.Write(_code_book._sc_actfec._address, val);
#endif
  return ret;
}
//____________________________________________________________________
int
RcuCE::Rcu::DcsMaster()
{
  DGUARD(7,"DcsMaster");
  if (_code_book._dcs_master._address == _code_book._invalid) return SUCCESS;

  // Do not write master if already correct.
  if (_current_master == master_dcs) return SUCCESS;

  if (Exec(_code_book._dcs_master, 0x0) < 0) {
    ERROR(_main, ("no access to RCU"));
    return -DCSC;
  }
  _current_master = master_dcs;
  return SUCCESS;
}

//____________________________________________________________________
int
RcuCE::Rcu::DdlMaster()
{
  DGUARD(7,"DdlMaster");
  if (_code_book._ddl_master._address == _code_book._invalid) return SUCCESS;
  
  // Do not write master if already correct.
  if (_current_master == master_ddl) return SUCCESS;
  
  if (Exec(_code_book._ddl_master, 0x0) < 0) {
    ERROR(_main, ("no access to RCU"));
    return -DCSC;
  }
  _current_master = master_ddl;
  return SUCCESS;
}


//====================================================================
//
// I2C interface for front-end cards 
bool
RcuCE::Rcu::CanSwitch() const
{
  return _code_book._sc_switch._address != _code_book._invalid;
}

//____________________________________________________________________
int
RcuCE::Rcu::SwitchLines(bool on)
{
  DGUARD(7,"SwitchLines() [0x%x]", _code_book._sc_switch._address);
  if (_code_book._sc_switch._address == _code_book._invalid) return SUCCESS;
  int ret = Exec(_code_book._sc_switch, (on ? 0x1 : 0x0));
  if (ret < 0) 
    WARN(_main, ("Failed to turn switched lines %s", (on ? "on" : "off")));
  return ret;
}
//____________________________________________________________________
int
RcuCE::Rcu::ResetERRREG()
{
  DGUARD(7,"ResetERRREG() [0x%x]", _code_book._sc_reset._address);
  int ret = Exec(_code_book._sc_reset, 0x0);
  if (ret < 0) WARN(_main, ("Failed to reset I2C error register"));
  return ret;
}
//____________________________________________________________________
int
RcuCE::Rcu::ReadERRREG(bool& not_active, bool& no_ack)
{
  DGUARD(7,"ReadERRREG(%d,%d)",not_active,no_ack);
  unsigned int val;
  int retcode = SUCCESS;
  not_active = false;
  no_ack     = false;
  if (!(_code_book._sc_error._address ==  _code_book._invalid)) { 
    if ((retcode = Read(_code_book._sc_error, val) < 0)) {
      WARN(_main, ("Failed to read error register"));
      return retcode;
    }
    if (val != 0) WARN(_main, ("Contents of I2C error register: 0x%08x", val));
    not_active = (val & (0x1 << 0));
    no_ack     = (val & (0x1 << 1));
  }
  else if (!(_code_book._sc_debug._address == _code_book._invalid)) { 
    if ((retcode = Read(_code_book._sc_debug, val) < 0)) {
      WARN(_main, ("Failed to read debug register"));
      return retcode;
    }
    if ((val & 0xFF) != 0) {
      WARN(_main, ("Contents of I2C debug register: 0x%02x", val & 0xFF));
      not_active = false;
      no_ack     = true;
    }
  }
    
  return retcode;
}

//____________________________________________________________________
int
RcuCE::Rcu::ResetRESREG()
{
  DGUARD(7,"ResetERRREG() [0x%x]", _code_book._sc_clear._address);
  int retcode = Exec(_code_book._sc_clear, 0x0);
  if (retcode < 0) WARN(_main, ("Failed to reset result register"));
  return retcode;
}
//____________________________________________________________________
int
RcuCE::Rcu::ReadRESREG(unsigned int& ret, bool switched)
{
  DGUARD(7,"ReadRESREG(%d)",ret);
  CodeBook::Register&  dest = (switched ? 
			       _code_book._int_mode :
			       _code_book._sc_result);
  int retcode = Read(dest, ret);
  if (retcode < 0)
    WARN(_main, ("Failed to reset result register (0x%x, %s)", 
		 dest._address, (switched ? "switched" : "straight")));
  return retcode;
}
//____________________________________________________________________
int
RcuCE::Rcu::WriteSCADD(unsigned int data, bool switched)
{
  DGUARD(7,"WriteSCADD(0x%x) [%s]",data, (switched ? "switched" : "straight"));
  CodeBook::Register& dest = (switched ? 
			      _code_book._sc_result : 
			      _code_book._sc_address);
  int ret = Write(dest, data);
  if (ret < 0)
    WARN(_main, ("Failed to write address 0x%04x to slow control address "
		 "register 0x%04x (%d)", data, dest._address, ret));
  return ret;
}
//____________________________________________________________________
int
RcuCE::Rcu::WriteSCDAT(unsigned int data, bool switched)
{
  DGUARD(7,"WriteSCDAT(0x%x) [%s]",data, (switched ? "switched" : "straight"));
  CodeBook::Register& dest = (switched ? 
			      _code_book._sc_error : 
			      _code_book._sc_data);
  int ret = Write(dest, data);
  if (ret < 0)
    WARN(_main, ("Failed to write data 0x%04x to slow control "
		 "data register 0x%04x (%d)", data, dest._address, ret));
  return ret;
}
//____________________________________________________________________
int
RcuCE::Rcu::ExecSC()
{
  DGUARD(7,"ExecSC()");
  int ret = Exec(_code_book._sc_exec, 0x0);
  if (ret < 0) 
    WARN(_main, ("Failed to execute I2C command (%d)", ret));
  return ret;
}

//____________________________________________________________________
#define ENCODE_ADDR(RNW,BCAST,FEC,REG) \
  (((RNW   & 0x1) << 14) | \
   ((BCAST & 0x1) << 13) | \
   ((FEC   & 0x1F) << 8) | \
   ((REG   & 0xFF)))
#define ENCODE_CMD(RNW,BCAST,FEC,REG) \
  ((_code_book._sc_old_exec._address) | \
   ((RNW   & 0x1)  << 11) | \
   ((BCAST & 0x1)  << 10) | \
   ((FEC   & 0x1F) <<  5) | \
   ((REG   & 0x1F)))

//____________________________________________________________________
int
RcuCE::Rcu::ExecViaI2C(bool          rnw, 
		       bool          bcast, 
		       unsigned char fec, 
		       unsigned char reg, 
		       unsigned int  val, 
		       bool          switched)
{
  DGUARD(7,"ExecI2C(%s,%s,0x%x,0x%x,0x%x)",(rnw ? "read" : "write"), 
	 (bcast ? "broadcast" : ""), unsigned(fec),unsigned(reg), val);
  int ret = 0;
  DEBUG(_main, ("Executing via I2C for fec=%x and reg=%x", fec, reg));
  usleep(10); // Explicit wait
  // Use command register for 5 bit access
  if (/* reg > 0x1F && */ 
      _code_book._sc_address._address != _code_book._invalid) {

    // Make the address (8bit register address) 
    unsigned int addr = ENCODE_ADDR((rnw?1:0),(bcast?1:0),fec,reg);

    if (rnw) { 
      if ((ret = ResetERRREG()) < 0) return ret;
      if ((ret = ResetRESREG()) < 0) return ret;
      val = 0;
    }
    // Write address to register 
    if ((ret = WriteSCADD(addr, switched)) < 0) return ret;
    if ((ret = WriteSCDAT(val,  switched)) < 0) return ret;
    if (rnw) if ((ret = ResetERRREG()) < 0) return ret;

    // Now, send the command to do the slow control read-out 
    ret = ExecSC();
  }
  else {
    // 5 bit writing
    // DEBUG(_main, ("Doing 5bit I2C communication"));
    if (reg > 0x7F) {
      WARN(_main,("Firmware version %d does not support I2C address widths"
		  " > 8bit (was 0x%02x)", _fw_version, reg));
      return -BAD_PARAMETER;
    }
    unsigned int cmd = ENCODE_CMD((rnw?1:0),(bcast?1:0),fec,reg);
    if ((ret = _dcsc.Write(cmd, val)) < 0) 
      WARN(_main, ("Failed to execute command 0x%04x "
		   "(fec 0x%02x, register 0x%02x) (%d)",
		   unsigned(cmd), unsigned(fec), unsigned(reg), ret));
  }
  return ret;
}

		    
//____________________________________________________________________
int
RcuCE::Rcu::ReadViaI2C(unsigned char fec, unsigned char reg, 
		       unsigned short& val)
{
  DGUARD(7,"ReadViaI2C(0x%x,0x%x,%d)",unsigned(fec),unsigned(reg),val);
  DEBUG(_main, ("Read Register 0x%x from FEC 0x%x", 
		unsigned(reg), unsigned(fec)));
  int ret = 0;
  // Switch lines (?!)
  bool switched = (fec >= 0x10 && CanSwitch());
  if ((ret = ResetERRREG()) < 0) return ret;
  if ((ret = SwitchLines(switched)) < 0) return ret;
  if ((ret = ExecViaI2C(true, false, fec, reg, 0, switched)) < 0) return ret;

  usleep(10);
  // Read error resgister 
  bool not_act, no_ack;
  if ((ret = ReadERRREG(not_act, no_ack)) < 0) return ret;
  if (not_act || no_ack) { 
    WARN(_main, ("Error set in I2C register %s%s", 
		 (not_act ? "'not active'" : ""), 
		 (no_ack ? " 'no acknowledge'" : "")));
    ResetERRREG();
    if (not_act) return -EBADF;
    if (no_ack)  return -EAGAIN;
  }

  // Read result register 
  unsigned int retval;
  if ((ret = ReadRESREG(retval, switched)) < 0) return ret;

  unsigned short data = retval & 0xffff;
  unsigned char  rfec = (retval >> 16) & 0x1F;
  DMSG(8,"Read 0x%x from FEC 0x%x (0x%x) register 0x%x", data, 
       unsigned(fec), unsigned(rfec), unsigned(reg));
  DEBUG(_main, ("Read 0x%x from FEC 0x%x (0x%x) register 0x%x", data, 
		unsigned(fec), unsigned(rfec), unsigned(reg)));
  if (rfec != fec) {
    WARN(_main, ("Mismatch in FEC address, got 0x%02x but expected 0x%02x", 
		 rfec, fec));
    return -ADDRESS_ERROR;
  }
  val = data;
 
  return 0;
}

//____________________________________________________________________
int
RcuCE::Rcu::WriteViaI2C(unsigned char fec, unsigned char reg, 
			unsigned short val)
{
  DGUARD(8,"WriteViaI2C(0x%x,0x%x,%d)",unsigned(fec),unsigned(reg),val);
  int  ret      = 0;
  int  ret1     = 0;
  bool switched = false; // (fec >= 0x10 && CanSwitch());
  if ((ret  = ResetERRREG()) < 0) return ret;
  if ((ret  = SwitchLines(switched)) < 0) return ret;
  
  if ((ret = ExecViaI2C(false,false,fec,reg,val, switched)) < 0) return ret;

  bool not_act, no_ack;
  if ((ret  = ReadERRREG(not_act, no_ack)) < 0) return ret;
  if (not_act)	return -NOT_ACTIVE;
  if (no_ack)   return -NO_ANSWER;
  return ret1;
}

//====================================================================
//
// Fec register acccess via ALTRO bus
//____________________________________________________________________
int
RcuCE::Rcu::SimpleCheckERRST(bool bnota)
{
  int ret = 0;
  if (_code_book._errst._address != _code_book._invalid) {
    unsigned int errst = 0;
    if ((ret = ReadERRST(errst)) < 0) return ret;
    errst  &= ~RCU_HWADDR_MASK; // Filter out HW address 
    if      (errst & RCU_SEQ_BUSY)      return -BUS_BUSY;
    else if (errst & SEQ_ABORT)         return -IMEM_ABORTED;
    else if (errst & FEC_TIMEOUT)       return -BUS_TIMEOUT;
    else if (errst & ALTRO_ERROR)       return -BUS_ERROR;
    else if (errst & ALTRO_HWADD_ERROR) return -ADDRESS_ERROR;
    else if (errst & PATTERN_MISSMATCH) return -BAD_PATTERN;
    return 0;
  }
  if (_code_book._fec_err_b._address != _code_book._invalid && 
      _code_book._fec_err_a._address != _code_book._invalid) { 
    unsigned int busbsy = 0;
    if ((ret = _dcsc.Read(0x5116, busbsy)) < 0) return ret;
    if ((busbsy & 0x1) != 0) return -BUS_BUSY;
    
    unsigned int vfecerr = 0;
    CodeBook::Register& fecerr = (bnota ? 
				  _code_book._fec_err_b : 
				  _code_book._fec_err_a);
    if ((ret = Read(fecerr, vfecerr)) < 0) return ret;
    if (vfecerr != 0) return -BUS_ERROR;

    return 0;
  }
  return 0;
}
typedef unsigned int uint32_t;
//____________________________________________________________________
int
RcuCE::Rcu::ReadViaBus(unsigned char   fec, 
		       unsigned char   chip, 
		       unsigned char   channel, 
		       unsigned char   reg, 
		       unsigned short& res)
{
  DGUARD(8,"ReadViaBus(0x%x,0x%x,0x%x,0x%x,%d)",
	 unsigned(fec),unsigned(chip),unsigned(channel),unsigned(reg),res);
  // Base stuff for IMEM 
  static uint32_t base_imem[] = { 0x400000, 0x500000, 0x390000 };
  uint32_t addr      = (((fec&0x1f) << 7)|((chip&0x7) << 4)|(channel&0xf));  
  uint32_t inst      = (reg & 0xff);
  uint32_t imem[]    = { base_imem[0] | addr | inst, 
			 base_imem[1], 
			 base_imem[2] };
  bool     bnota     = (fec & 0x10) != 0;
  size_t         n   = 3;
  unsigned char* dat = reinterpret_cast<unsigned char*>(&(imem[0]));
  int            ret = 0;
  
  // First read the ERRST register to see if the IMEM interface is
  // busy 
  if ((ret = SimpleCheckERRST(bnota)) < 0) return ret;

  // Write to the IMEM 
  if ((ret = Write(_code_book._imem, dat, n*sizeof(uint32_t), 
		   n, Dcsc::Word32)) < 0) return ret;
  
  // Execute the command 
  if ((ret = ExecEXEC(0)) < 0) return ret;
  
  // Now, read back the result 
  byte_array odata(4);
  size_t osize = 0;
  if ((ret = Read(_code_book._rmem, odata, osize, 1)) < 0) return ret;
  
  // Convert the result back to an interger 
  uint32_t ires = *(reinterpret_cast<uint32_t*>(&(odata[0])));
  res           = (ires & 0xFFFF);
  return ret;
}

//____________________________________________________________________
int
RcuCE::Rcu::WriteViaBus(unsigned char  fec,
			unsigned char  chip, 
			unsigned char  channel, 
		        unsigned char  reg, 
			unsigned short val)
{
  DGUARD(8,"WriteViaBus(0x%x,0x%x,0x%x,0x%x,%d)",
	 unsigned(fec),unsigned(chip),unsigned(channel),unsigned(reg),val);
  // Base stuff for IMEM 
  static uint32_t base_imem[] = { 0x600000u, 0x700000u, 0x390000u };

  // First encode the message 
  uint32_t addr      = (((fec&0x1f) << 7)|((chip&0x7) << 4)|(channel&0xf));
  uint32_t inst      = (reg & 0xff);
  uint32_t data      = (val & 0xfffff);
  uint32_t imem[]    = { base_imem[0] | addr | inst, 
			 base_imem[1] | data, 
			 base_imem[2] };
  bool     bnota     = (fec & 0x10) != 0;
  size_t   n         = 3;
  unsigned char* dat = reinterpret_cast<unsigned char*>(&(imem[0]));
  int            ret = 0;
  
  // First read the ERRST register to see if the IMEM interface is
  // busy 
  if ((ret = SimpleCheckERRST(bnota)) < 0) return ret;
  
  // Write to the IMEM 
  if ((ret = Write(_code_book._imem, dat, n*sizeof(uint32_t), 
		   n, Dcsc::Word32)) < 0) return ret;
  
  // Execute the command 
  return ExecEXEC(0);
}




//====================================================================
//
// Block access to the RCU
//____________________________________________________________________
int
RcuCE::Rcu::ReadBlock(unsigned int address, size_t m_size, 
		      byte_array& odata, size_t& osize, 
		      size_t n)
{
  DGUARD(5,"ReadBlock(0x%x,%d,%p,%d,%d)",address,m_size,&(odata[0]),osize,n);
  if (address > 0xffff) {
    ERROR(_main, ("Address 0x%x exceeds RCU memory", address));
    return -BAD_PARAMETER;
  }
  if (m_size > 0 && n > m_size) { 
    ERROR(_main, ("Requested size %d  exceeds size of partition @ 0x%x" 
		  ", truncted to %d", n, m_size, address, m_size));
    n = m_size;
  }
  if (address + n > 0xffff) { 
    ERROR(_main, ("Requested end (0x%x+0x%x=0x%x) exceeds size of partition "
		  "@ 0x%x, truncted to %d", address, n, address+n, address, 
	 0x10000 - address));
    n = 0x10000 - address;
  }
  
  const size_t int_size = sizeof(unsigned int);
  if (odata.size() < osize+n*int_size) {
    DMSG(5, "Resizing odata to %d from %d", osize+n*int_size, odata.size());
    odata.resize(osize+n*int_size);
  }
  
  int ret = _dcsc.Read(address, ((unsigned int*)&(odata[osize])), n);
  DMSG(5, "Read %d words from RCU buffer 0x%x, result %d", 
	n, address, ret);

  if (ret > 0 && ret != int(n)) 
    ERROR(_main, ("Inconsistent size %d != %d", ret, n));
  if (ret >= 0) {  
    ret = 0; // Return zero, since we're not eating payload
    osize += n*int_size;
  }
  DMSG(5, "Returning %d, and now osize=%d", ret, osize);
  return ret;
}

//____________________________________________________________________
int
RcuCE::Rcu::WriteCommandBuffer(unsigned int address, 
			       const unsigned char* idata, 
			       size_t n, Dcsc::WordSize w_size)
{
  DGUARD(8,"WriteCommandBuffer(0x%x,%p,%d,%d)",
	 address,idata, n, w_size);
  if (w_size != Dcsc::Word32 && w_size != Dcsc::Word16) { 
    ERROR(_main, ("invalid word size: %d", w_size));
    return 0;
  }

  const unsigned char*  ptr  = &(idata[0]);
  int ret = _dcsc.Write(address, (const unsigned int*)(ptr), n, w_size);
  if (ret >= 0) {
    DEBUG(_main, (
	 "wrote %d words (%d) to RCU buffer 0x%x with result %d", 
	 n, w_size, address, ret));
    ret = n;
  }
  return ret;
}

//____________________________________________________________________
int
RcuCE::Rcu::WriteBlock(unsigned int address, size_t m_size, 
		       const unsigned char* idata, size_t isize, 
		       size_t n, Dcsc::WordSize w_size)
{
  DGUARD(8,"WriteBlock(0x%x,%d,%p,%d,%d,%d)",
	 address,m_size, idata, isize, n, w_size);
  // bool check = false;
  // IN_HL_RANGE(address, isize, check);
  // if (check) return -EINVAL;

  if (address == _code_book._monitor_on) {
    INFO(_main, ("Enabling FEC monitoring"));
    Fec::Enable();
    return 0;
  }
  if (address == _code_book._monitor_off) {
    INFO(_main, ("Disabling FEC monitoring"));
    Fec::Disable();
    return 0;
  }

  if (m_size > 0 && n > m_size) return -BAD_PARAMETER;

  size_t words_per_int = 1;
  switch (w_size) {
  case Dcsc::Word8:      words_per_int = 4; break;
  case Dcsc::Swapped16:  // Fall through
  case Dcsc::Word16:     words_per_int = 2; break;
  case Dcsc::Swapped32:  // Fall through
  case Dcsc::Word32:     words_per_int = 1; break;
  case Dcsc::Word10:     words_per_int = 3; break;
  }
  
  // Check if we're writing to ACTFEC - and if so, get the current
  // value 
  int ret = 0;
  unsigned int old_afl = 0;
  if (_enable_flick && address == _code_book._afl._address) { 
    if ((ret = ReadAFL(old_afl)) < 0) { 
      WARN(_main, ("Failed to read AFL (%d)!", ret));
      return ret; 
    }
  }

  size_t min_buf_size = ((n-1) / words_per_int + 1) * sizeof(unsigned int);
  if (min_buf_size > isize) return -ENODATA;
  
  ret = WriteCommandBuffer(address, idata, n, w_size);
  // Return number of words (in natural size of bytes) that we wrote 
  DEBUG(_main, (
       "wrote %d words (%d) to RCU buffer 0x%x with result %d, "
       "returning %d",  n, w_size, address, ret, 
       (ret < 0 ? ret : min_buf_size)));

  // Check if we're writing to AFL 
  if (_enable_flick && address == _code_book._afl._address) { 
    const unsigned int target = *(reinterpret_cast<const int*>(idata));
    DEBUG(_main, ("Writing to ACTFEC! 0x%08x - was 0x%08x", target, old_afl));
    // Now write old again, wait a while and write target again 
    WARN(_main, ("Implicit flicker of CARDSW lines done by CE"));
    int     ret_w1 = Write(_code_book._afl,old_afl);
    int     ret_w2 = Write(_code_book._afl,target);
    if (ret_w1 < 0 || ret_w2 < 0) 
      WARN(_main, ("Failed an AFL write (old=%d, new=%d)", ret_w1, ret_w2));
#ifdef DUPLICATE_TO_SC_ACTFEC
    if (ret_w1 >= 0 && ret_w2 >= 0 &&  
	(_code_book._afl._address != _code_book._sc_actfec._address)) 
      ret = Write(_code_book._sc_actfec, target);
#endif
  }
#if 0
  if (address == _code_book._reset_global._address || 
      address == _code_book._reset_rcu._address) {
    SetState(CommonStates::Standby);
  }
#endif

  return (ret < 0 ? ret : min_buf_size);
}

//====================================================================
//
// Various utilities 

//____________________________________________________________________
int
RcuCE::Rcu::FindFwVersion(bool autodetect)
{
  DGUARD(8,"FindFwVersion");
  // Check if interface is open.
  int ret = 0;
  _fw_version = 0;
  if (!_dcsc.IsOpen()) return -DCSC;
  // Leave control to DDL
  DcsMaster();
  if ((ret = DdlMaster()) < 0)  return ret;

  INFO(_main, ("Try to get FW version from 0x%04x", _code_book._fw_vers_new));
  if ((ret = _dcsc.Read(_code_book._fw_vers_new, _fw_version)) < 0 ||
      _fw_version == 0) { 
    WARN(_main, ("Reading FW version register 0x%04x failed "
		 "(%d<0 || 0x%06x==0)", _code_book._fw_vers_new, ret, 
		 _fw_version));
  }
  
  if (ret < 0 || _fw_version == 0) { 
    // Try to read the register 
    INFO(_main, ("Try to get FW version from 0x%04x", _code_book._fw_vers));
    if ((ret = _dcsc.Read(_code_book._fw_vers, _fw_version)) < 0 ||
	_fw_version == 0) { 
      WARN(_main, ("Reading FW version register 0x%04x failed, "
		 "(%d<0 || 0x%06x==0)", _code_book._fw_vers, ret, 
		 _fw_version));
      // Retry with us as master. 
      DcsMaster();
      INFO(_main, ("Try to get FW version from 0x%04x (DCS master)", 
		   _code_book._fw_vers));
      ret = _dcsc.Read(_code_book._fw_vers, _fw_version);
      if (ret < 0) 
	WARN(_main, ("Second trial at reading FW version failed"));
    }
  }

  if (ret < 0 || _fw_version == 0) {
    WARN(_main, ("Trying old version register 0x%04x", 
		 _code_book._fw_vers_old));
    ret = _dcsc.Read(_code_book._fw_vers_old, _fw_version);
  }
  
  // Leave control with DDL
  if ((ret = DdlMaster()) < 0) return ret;

  // Got a firmware version
  if (_fw_version > 0) {
    INFO(_main, ("Firmware version 0x%08x, leaving DDL as master", 
	  _fw_version));
    return _fw_version;
  }

  // We haven't got a version register
  INFO(_main, ("RCU firmware without version register"));

  // If autodetect is turned off, return 1
  if (!autodetect) return 1;

  // Read ACTFEC 
  INFO(_main, ("probing MSM access"));
  unsigned int old_afl = 0; 
  if ((ret = _dcsc.Read(_code_book._afl_addr, old_afl)) < 0) { 
    WARN(_main, ("Reading active front-end-card list failed"));
    return ret;
  }

  // Make DDL master 
  unsigned short m = _code_book._ddl_master._address;

  // Try with DDL master, and if that fails with DCS as master. 
  for (size_t i = 0; i < 2; i++) {
    unsigned int test = ~old_afl;
    unsigned int read = 0;
    if ((ret = WriteAFL(test)) < 0) {
      WARN(_main, ("Can not write test pattern to AFL"));
      return ret;
    }
    if ((ret = _dcsc.Read(_code_book._afl_addr, read)) < 0) {
      WARN(_main, ("Can not read AFL"));
      return ret;
    }
    
    if (read == test) {
      if (m == _code_book._ddl_master._address) 
	INFO(_main, ("This RCU firmware versions seems to allow MSM "
	      "access while the DDL is Altro Bus master. "
	      "DDL set to default Altro Bus master"));
      else 
	INFO(_main, ("This RCU firmware versions does not seem to allow MSM "
	      "access while the DDL is Altro Bus master. "
	      "DCS set to default Altro Bus master"));
      break;
    }
    else if (m == _code_book._ddl_master._address) {
      m = _code_book._dcs_master._address;
      if ((ret = DcsMaster()) < 0) return ret;
    }
  }

  // Restore 
  if ((ret = WriteAFL(old_afl)) < 0) {
    WARN(_main, ("Can not restore AFL"));
    return ret;
  }
  _fw_version = (m == _code_book._ddl_master._address ? 1 : 0);
  INFO(_main, ("Firmware version set to %d", _fw_version));
  return _fw_version; 
}


//____________________________________________________________________
//
// EOF
//
