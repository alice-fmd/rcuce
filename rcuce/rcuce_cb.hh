#ifndef RCUCE_CODEBOOK_HH
#define RCUCE_CODEBOOK_HH
#include <cctype>

namespace RcuCE
{
  /** @class CodeBook rcuce_cb.hh <rcuce/rcuce_cb.hh>
      Structure that holds the address, size, and width of the RCU
      commands, registers, and memories.
      @ingroup rcuce_rcu 
  */
  struct CodeBook 
  {
    /** Some defaults */
    enum {
      _afl_addr    = 0x8000,
      _fw_vers_old = 0x8006,
      _fw_vers     = 0x8008, 
      _fw_vers_new = 0x5106,
      _monitor_on  = 0xF001,
      _monitor_off = 0xF002,
      _invalid     = 0xFFFF
    };
    struct Element {
      Element(unsigned short a) : _address(a) {}
      virtual ~Element() {} 
      void invalidate() { _address = _invalid; }
      virtual void print(const char* name) const = 0;
      /** Address */
      unsigned short _address;
    };

    /** Structure for commands */
    struct Command : public Element {
      Command(unsigned short a) : Element(a) {}
      void print(const char* name) const;
      void set(unsigned short a) { _address = a; }
    };
    /** Structure for registers */ 
    struct Register : public Element {
      Register(unsigned short a, unsigned int m) : Element(a), _mask(m) {}
      /** Set new values */ 
      void set(unsigned short a, unsigned int m) 
      {
	_address = a;
	_mask    = m;
      }
      void invalidate() { Element::invalidate(); _mask = 0; }
      void print(const char* name) const;
      /** Mask of register values */
      unsigned int   _mask;
    };
    /** Structure for memories */
    struct Memory : public Element { 
      Memory(unsigned short a, unsigned int m, unsigned short s) 
	: Element(a), _size(s), _mask(m) {}
      /** Set new values */ 
      void set(unsigned short b, unsigned short e, unsigned int m=0)
      {
	_address = b;
	_size    = e - _address + 1;
	_mask    = m;
      }
      void invalidate() { Element::invalidate(); _size = 0; _mask = 0; }
      void print(const char* name) const;
      /** Number of words in memory */
      unsigned short _size;
      /** Mask of memory values */
      unsigned int _mask;
    };
    
    /** Create the codebook */
    CodeBook();
    /** Configure the code book */
    void Configure(const unsigned int fw_version);
    /** Print configuration to standard out */
    void Print() const;
    const char* ElementName(const Element& e) const;
    void ListElements() const;

    /** Version number */ 
    int _version;
    
    /** @{ 
	@name Bus access */
    /** Result memory */
    Memory _rmem;
    /** Instruction memory */
    Memory _imem;
    /** Execute command */
    Command _exec;
    /** Abort command */
    Command _abort;
    /** Last address */
    Register _iradd;
    /** Last data */
    Register _irdat;
    /** @} */

    /** @{ 
	@name */
    /** Pattern memory */
    Memory _pmem;
    /** Pattern memory configuration */
    Register _pmcfg;
    /** @} */

    /** @{ 
	@name Slow control */
    /** Result of slow control read commands */
    Register _sc_result;
    /** Error register of slow control */
    Register _sc_error;
    /** Error register of slow control */
    Register _sc_debug;
    /** Address for slow control commands */
    Register _sc_address;
    /** Data for slow control commands */
    Register _sc_data;
    /** Active front-end cards for slow control */
    Register _sc_actfec;
    /** Swithc lines */ 
    Command  _sc_switch;
    /** Execute */
    Command  _sc_exec;
    /** Reset slow control errors */
    Command  _sc_reset;
    /** Clear result register */
    Command  _sc_clear;
    /** Old 5bit command interface */
    Command  _sc_old_exec;
    /** @} */

    /** @{ 
	@name Monitor */
    /** Interrupt enable */
    Register _int_mode;
    /** Type of interrupts */
    Register _int_type;
    /** Status of status memory */
    Register _smem_status;
    /** Reset AFL */ 
    Command _rst_afl;
    /** Reset RDO */ 
    Command _rst_rdo;
    /** Reset ready bits in status memory */
    Command _rs_smem;
    /** Status memory */
    Memory _status;
    /** @} */
    
    /** @{ 
	@name Cards */
    /** Active front-end cards */ 
    Register _afl;
    /** Read-out front-end cards */
    Register _rdo;
    /** Active channel list */
    Memory _acl;
    /** @} */
    
    /** @{ 
	@name TTC interface */ 
    Memory _ttcrx;
    /** @} */

    /** @{ 
	@name Hit list */
    Memory _hit_list;
    /** @} */

    /** @{ 
	@name Misc */
    /** Common data header */
    Memory _header;
    /** Error and status */
    Register _errst;
    /** Trigger configuration */
    Register _trcfg;
    /** Trigger counter */
    Register _trcnt;
    /** Last event length */
    Register _lwadd;
    /** Last channel read out in ach branch */
    Register _chadd;
    /** Reset the status */
    Command _reset_errst;
    /** Reset trigger configutation */
    Command _reset_trcfg;
    /** Reset trigger counter */
    Command _reset_trcnt; 
    /** Reset DMEM1 */
    // Command _reset_dmem1
    /** Reset DMEM2 */
    // Command _reset_dmem2
    /** Firmware version register */
    Register _fw_version;
    /** Make DCS master of bus */
    Command _dcs_master;
    /** Make DDL master of bus */
    Command _ddl_master;
    /** Enable TTC triggers */
    Command _enable_ttc;
    /** Enable AUX triggers */
    Command _enable_aux;
    /** Enable command triggers */
    Command _enable_cmd;
    /** Send an L1 */
    Command _trigger_l1;
    /** Reset both RCU and FEC */
    Command _reset_global;
    /** Reset only FEC */
    Command _reset_fec;
    /** Reset only RCU */
    Command _reset_rcu;
    /** @} */

    /** @{ 
	@name New registers in RCU II */
    /** Trigger configuration */
    Register _trg_conf;
    /** ALTRO bus interface */
    Register _altro_if;
    /** Read out mode */
    Register _rdo_mod;
    /** ALTRO coniguration 1 */
    Register _altro_cfg_1;
    /** ALTRO coniguration 2 */
    Register _altro_cfg_2;
    /** Bus errors - branch B*/
    Register _fec_err_a;
    /** Bus errors - branch B*/
    Register _fec_err_b;
    /** Read-out errors */
    Register _rdo_err;
    /** Bus grant register */
    Register _rcu_bus;
    /** ALTRO control bus */
    Register _altro_bus;
    /** ALTRO control bus during transfer */
    Register _altro_bus_trsf;
    /** ATLRO bus busy */
    Register _altro_bus_busy;
    /** Multi buffer counter */
    Register _meb_cnt;
    /** Software trigger counter */
    Register _sw_trg_cnt;
    /** Auxillary trigger counter */
    Register _aux_trg_cnt;
    /** TTC L2 accept counter */
    Register _ttc_l2a_cnt;
    /** TTC L2 reject counter */
    Register _ttc_l2r_cnt;
    /** data strobe counter branch A */
    Register _dstb_cnt_a;
    /** data strobe counter branch B */
    Register _dstb_cnt_b;
    /** Transfer counter branch A */
    Register _trsf_cnt_a;
    /** Transfer counter branch A  */
    Register _trsf_cnt_b;
    /** acknowledge counter branch A  */
    Register _ack_cnt_a;
    /** acknowledge counter branch A */
    Register _acl_cnt_b;
    /** Control strobe counter branch A */
    Register _cstb_cnt_a;
    /**  Control strobe counter branch A */
    Register _cstb_cnt_b;
    /** Last block length branch A */
    Register _last_dstb_a;
    /** Last block length branch B */
    Register _last_dstb_b;
    /** Address mismatch counter */
    Register _add_mm_cnt;
    /** Block length mismatch counter */
    Register _bl_mm_cnt;
    /** Arbitrator states */
    Register _abd_state;
    /** Read out states */
    Register _rdo_state;
    /** Instruction memory/sequencer state */
    Register _imem_state;
    /** Event manager state */
    Register _evm_state;
    /** Data assembler state */
    Register _da_state;
    /** Back-plane version register */
    Register _bp_version;
    /** Back-plane version register */
    Register _rcu_id;
    /** Clear RDOERR register */
    Command _clear_rdrx_reg;
    /** Request DCS interrupt */
    Command _arb_iter_irq;
    /** Configure front-end card firmware */
    Command _conf_fec;
    /** TTC control */
    Register _ttc_control;
    /** Reset TTC module */
    Command _ttc_reset;
    /** Event information from TTC */
    Register _ttc_event_info;
    /** Event errors from TTC */
    Register _ttc_event_error;
    /** Execution of RCU temperature command */
    Register _rcu_temp_cmd; 
    /** RCU temperature result register */
    Register _rcu_temp_val;
    /** @} */
  };
}

#endif 
//
// EOF
//

