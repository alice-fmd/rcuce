#ifndef RCUCE_SERV_HH
#define RCUCE_SERV_HH
#include <list>
#include <vector>
#include <string>

// Forward declaration
namespace FeeServer 
{
  class ServiceBase;
  class Main;
}

namespace RcuCE
{
  /** @defgroup rcuce_serv Services 

      This group of classes defines a way for the user to setup
      services.  The class ServiceProvider is a container of services,
      as well as a container of other ServiceProvider objects.  In
      this way, a hierarcy of services can be built up.  This is
      exploited in the Fec interface to make the services of a the
      Front-end cards belong to the RCU service provider.

      @ingroup rcuce_core */

  //__________________________________________________________________
  /** @class Service rcuce_serv.hh <rcuce/rcuce_serv.hh> 
      @brief A wrapper FeeServer::ServiceBase 
      @ingroup rcuce_serv
  */
  class Service 
  {
  public:
    /** Destructor */ 
    virtual ~Service();
    /** update the service value */
    virtual int Update() = 0;
    /** Get pointer to service */ 
    FeeServer::ServiceBase* FeeService() { return _service; }
    /** Get then name */ 
    const char* Name() const;
    /** Whether we should always update this service.  This is
	meaningful only for services that can safely be updated no
	matter what state we're in - for example, a state of a finite
	state machine.  User classes should overload this member
	function to return @c true for such services. 
	@return @c true if this service should be updated no matter
	what. */ 
    virtual bool Always() const { return false; }
  protected:
    /** Protected constructor */
    Service();
    /** Protected copy constructor */
    Service(const Service& service);
    /** Protected assignement operator */
    Service& operator=(const Service& service);
    /** The fee server service wrapped */
    FeeServer::ServiceBase* _service;
  };

  //__________________________________________________________________
  /** @class ServiceProvider rcuce_serv.hh <rcuce/rcuce_serv.hh>
      @brief Base class for service providers. 
      @ingroup rcuce_serv
  */
  class ServiceProvider 
  {
  public:
    /** Destructor */ 
    virtual ~ServiceProvider();
    
    /** Update all services 
	@return 0 on success, error code otherwise */ 
    virtual int UpdateAll();
    /** Update some services - in particular those that return true
	when calling the member function Service::Always from it.
	@return 0 on success, error code otherwise */ 
    virtual int UpdateSome();

    /** Add a service to the list 
	@param service Service to add */
    virtual bool AddService(Service& service);
    /** Remove a service from the list */ 
    virtual bool RemoveService(Service& service);
    /** Remove all services from the list 
	Note, that it is up to the derived class to free the actual
	service objects */
    virtual bool RemoveAllServices();

    /** Add a service provider to the list 
	@param provider Provider to add */
    virtual bool AddProvider(ServiceProvider& provider);
    /** Remove a provider from the list */ 
    virtual bool RemoveProvider(ServiceProvider& provider);
    /** Remove all providers from the list 
	Note, that it is up to the derived class to free the actual
	provider objects */
    virtual bool RemoveAllProviders();

    /** Return all known services in the string array 
	@param names string array to return in */
    virtual void GetServiceNames(std::vector<std::string>& names);
    /** Get the number of faults seen so far */ 
    unsigned int GetNFaults() { return _n_faults; }
    /** Get the maximum number of faults */ 
    unsigned int GetMaxFaults() { return _max_faults; }
  protected: 
    /** Protected constructor */ 
    ServiceProvider(FeeServer::Main& m, 		    
		    unsigned int max_faults=3,
		    unsigned int wait=0);
    /** 
     * Check that we have not exceeded our number of faults.  If the
     * passed return code @a ret is non-zero, then increment the
     * number of faults seen so far.  If the number of faults seens so
     * far exceeds the number of accepted faults, return the passed
     * error code @a ret.   If not, return 0. 
     * 
     * @param ret Return code from some check.
     * 
     * @return 0 if there's no fault or we have not exceeded our
     *         number of accepted faults yet.  Otherwise, return the
     *         last seen fault. 
     */    
    virtual int CheckFaults(int ret);
    /** Reference to FeeServer */ 
    FeeServer::Main& _main;
    /** Type of array of services */ 
    typedef std::list<Service*> ServiceList;
    /** Type of array of sub-service providers */ 
    typedef std::list<ServiceProvider*> ProviderList;
    /** List of serivces */ 
    ServiceList _services;
    /** List of providers */
    ProviderList _providers;
    /** Do not erase the iterator in a  Remove operation. */
    bool _no_erase;
    /** Wait bewteen sub-updates */
    unsigned int _wait;
    /** Maximum number of faults */ 
    unsigned int _max_faults;
    /** Current number of faults */ 
    unsigned int _n_faults;
  private:
  };
}

#endif
//
// EOF
//

  
  
