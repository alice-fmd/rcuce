
#include <rcuce/rcuce_dcsc.hh>
#include <feeserverxx/fee_dbg.hh>
#include <rcuce/rcuce_bits.hh>
#include <dcsc/dcscMsgBufferInterface.h>
#include <cerrno>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <fstream>
#include <unistd.h>

//____________________________________________________________________
RcuCE::Dcsc::Dcsc(const std::string& file, bool verbose)
  : CommandHandler(DcscId),
    _attr(FeeServer::Mutex::Attributes(FeeServer::Mutex::Attributes::Checking)),
    _mutex("dcsc_mutex", _attr), 
    _is_open(false), 
    _use_single(false)
{
  std::cout << "Opening \"" << file << "\" for DCSC access" << std::endl;
  int ret = initRcuAccess(NULL); // file.empty() ? NULL : file.c_str());
  if (ret < 0) return;
  _is_open = true;
  unsigned int dcsc_options = (PRINT_RESULT_HUMAN_READABLE          |
			       (verbose ? PRINT_COMMAND_BUFFER : 0) |
			       (verbose ? PRINT_RESULT_BUFFER  : 0) |
			       (verbose ? PRINT_COMMAND_RESULT : 0) |
			       (verbose ? PRINT_SPLIT_DEBUG    : 0));
  setDebugOptions(dcsc_options);
}

//____________________________________________________________________
RcuCE::Dcsc::~Dcsc() 
{
  if (!_is_open) { 
    std::cerr << "Dcsc not open!" << std::endl;
    return;
  }
  releaseRcuAccess();
  _is_open = false;
}

namespace {
  int DecodeError(int ecode)
  {
    if (ecode >= 0) return RcuCE::Dcsc::SUCCESS;
    switch (-ecode) { 
    case ETIMEDOUT:       return -RcuCE::Dcsc::TIMEOUT;
    case EBADFD:          return -RcuCE::Dcsc::NOT_OPEN;
    case EIO:             return -RcuCE::Dcsc::WRONG_SIZE;
    case EMISS_MARKER:    return -RcuCE::Dcsc::NO_MARKER;
    case ENOTARGET:       return -RcuCE::Dcsc::NO_TARGET;
    case ENOBUSGRANT:     return -RcuCE::Dcsc::BUS_BLOCKED;
    case EMISS_ENDMARKER: return -RcuCE::Dcsc::NO_END;
    case EFAULT:          return -RcuCE::Dcsc::BAD_PARAMETER;
    case EINVAL:          return -RcuCE::Dcsc::BAD_PARAMETER;
    }
    return -RcuCE::Dcsc::UNKNOWN;
  }
}
    
//____________________________________________________________________
int
RcuCE::Dcsc::Write(u32_t address, u32_t data)
{
  DGUARD(9,"Write(0x%x,%d)",address,data);
  if (!_is_open) { 
    std::cerr << "Dcsc not open!" << std::endl;
    return -NOT_OPEN;
  }
  int ret = SUCCESS;
  try { 
    FeeServer::Guard g(_mutex);
    // printf("rcuSingleWrite(0x%x,%d) => %d\n", address, data, ret);
    int ret1 = rcuSingleWrite(address, data);
    if (ret1 == -1) ret = -TIMEOUT;
    else            ret = DecodeError(ret1);
  } 
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    ret = -CANNOT_LOCK;
  }
  return ret;
}

//____________________________________________________________________
int
RcuCE::Dcsc::Read(u32_t address, u32_t& data)
{
  DGUARD(9,"Read(0x%x,%d)",address,data);
  if (!_is_open) { 
    std::cerr << "Dcsc not open!" << std::endl;
    return -NOT_OPEN;
  }
  int ret = SUCCESS;
  try {
    FeeServer::Guard g(_mutex);
    // printf("rcuSingleRead(0x%x,%p) => %d\n", address, data, ret);
    int ret1 = rcuSingleRead(address, &data);
    if (ret1 == -1) ret = -TIMEOUT;
    else            ret = DecodeError(ret1);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    ret = -CANNOT_LOCK;
  }
  return ret;
}

//____________________________________________________________________
int
RcuCE::Dcsc::FastWrite(u32_t address, u32_t data)
{
  return rcuSingleWrite(address, data);
}

//____________________________________________________________________
int
RcuCE::Dcsc::Write(u32_t address, const unsigned int* data, size_t size,
		   WordSize wsize)
{
  DGUARD(9,"Write(0x%x,%p,%d,%d)",address,data,size,wsize);
  if (!_is_open) { 
    std::cerr << "Dcsc not open!" << std::endl;
    return -NOT_OPEN;
  }
  unsigned int* ptr  = const_cast<unsigned int*>(data);
  int           flag = 0;
  unsigned int  mask = 0;
  switch (wsize) {
  case Word8:     flag =  1; mask = 0xff;       break;
  case Word16:    flag =  2; mask = 0xffff;     break;
  case Word10:    flag =  3; mask = 0x3ff;      break;
  case Word32:    flag =  4; mask = 0xffffffff; break;
  case Swapped16: flag = -2; mask = 0xffff;     break;
  case Swapped32: flag = -4; mask = 0xffffffff; break;
  }
  int ret = 0;
  try {
    FeeServer::Guard g(_mutex);
    if (!_use_single) 
      ret = rcuMultipleWrite(address, ptr, size, flag);
    else {
      //printf("rcuMultipleWrite(0x%x,%p,%d,%d) => %d\n",
      //       address,size,data,flag,ret);
      int    ret1 = 0;
      u32_t  addr = address;
      u32_t  val  = 0;
      size_t i    = 0;
      for (i = 0; i < size && ret1 >= 0; i++) {
	val  =  ((*ptr) & mask);
	ret1 =  Write(addr,val);
	ptr  += flag;
	addr++;
      }
      if (ret1 >= 0) ret = i;
      else           ret = ret1;
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    ret = -CANNOT_LOCK;
  }
  return DecodeError(ret);
}


//____________________________________________________________________
int
RcuCE::Dcsc::Read(u32_t address, unsigned int* data, size_t size)
{
  DGUARD(9,"Read(0x%x,%p,%d)",address,data,size);
  if (!_is_open) { 
    std::cerr << "Dcsc not open!" << std::endl;
    return -NOT_OPEN;
  }
  int ret = SUCCESS;
  try {
    FeeServer::Guard g(_mutex);
    if (!_use_single) 
      ret =  rcuMultipleRead(address, size, data);
    else {
      // printf("rcuMultipleRead(0x%x,%d,%p) => %d", address, size,
      // data, ret);
      size_t i    = 0;
      ret         = 0;
      DMSG(10,"Reading %d 32bit words (0x%x-0x%x) using single reads", 
	   size, address, address+size-1);
      for (u32_t addr = address; addr < address + size; addr++, i++) 
	ret = Read(addr, data[i]);
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    ret = -CANNOT_LOCK;
  }
  return DecodeError(ret);
}

//____________________________________________________________________
bool
RcuCE::Dcsc::CanHandle(unsigned char group, unsigned char /*cmd*/) const
{
  return group == DcscId;
}

//____________________________________________________________________
int 
RcuCE::Dcsc::Handle(unsigned char cmd,          unsigned short param, 
		    byte_ptr      /*payload*/,  size_t  /*isize*/, 
		    byte_array&   /*odata*/,    size_t& /*osize*/)
{
  switch (cmd) {
  case SET_OPTION_FLAG:    setDebugOptionFlag(param); return 0;
  case CLEAR_OPTION_FLAG:  clearDebugOptionFlag(param); return 0;
  case USE_SINGLE_WRITE:   _use_single = (param != 0); return 0;
  }
  return 0;
}

//====================================================================
RcuCE::DcscDummy::DcscDummy(u32_t version, const std::string& dump,
			    unsigned short msleep) 
  : _mem(0x10000), 
    _version(version),
    _rev_version(0), 
    _sleep(msleep),
    _dump(dump)
{
  // Fill with zeros
  std::fill(_mem.begin(), _mem.end(), 0);
  _is_open = true;
  unsigned int day   = ((_version >> 16) & 0xFF);
  unsigned int month = ((_version >> 8)  & 0xFF);
  unsigned int year  = ((_version >> 0)  & 0xFF);
  _rev_version       = (year << 16) | (month << 8) | day;
  std::cout << "Initialised DCSC dummy with version 0x" 
	    << std::setfill('0') << std::hex << std::setw(6) 
	    << _version     << " (yy/mm/dd=" 
	    << std::setw(2) << year << "/" 
	    << std::setw(2) << month << "/" 
	    << std::setw(2) << day << ")" 
	    << std::setfill(' ') << std::dec << std::endl;
  unsigned int fw_addr = (_rev_version >= 0x080400 ? 0x5106 : 
			  _rev_version <= 0x060504 ? 0x8006 : 0x8008);
  _mem[fw_addr] = _version;
  if (_rev_version < 0x080400) return;

  // Fill IMEM
  std::fill(&(_mem[0]), &(_mem[0x1000]), 0x3FFFFF);
  // Fill ACL 
  std::fill(&(_mem[0x1001]), &(_mem[0x2000]), 0x0);
  // Fill RMEM 
  std::fill(&(_mem[0x2001]), &(_mem[0x4000]), 0x1FFFFFF);

  if (_dump.empty()) return;
  
  std::ifstream file(_dump.c_str());
  if (!file) {
    std::cerr << "No dump file (" << _dump 
	      << ") found - using defaults" << std::endl;
    return;
  }
  char* ptr = reinterpret_cast<char*>(&(_mem[0]));
  file.read(ptr, sizeof(u32_t)*_mem.size());
#if 0
  _mem.resize(0);
  file.setf(std::ios_base::hex);
  std::copy(std::istream_iterator<u32_t>(file), std::istream_iterator<u32_t>(),
	    std::back_inserter(_mem));
#endif
  file.close();
  std::cout << "Read " << _mem.size() << " words from " << _dump << std::endl;
} 

//____________________________________________________________________
RcuCE::DcscDummy::~DcscDummy(){}
//____________________________________________________________________
void
RcuCE::DcscDummy::CleanUp()
{
  DGUARD(9,"CleanUp in DCSC dummy");
  if (_dump.empty()) return;
  
  std::ofstream file(_dump.c_str());
  if (!file) { 
    std::cerr << "Failed to open dump file \"" << _dump << "\"" << std::endl;
    return;
  }
  char* ptr = reinterpret_cast<char*>(&(_mem[0]));
  file.write(ptr, sizeof(u32_t)*_mem.size());

#if 0
  file.setf(std::ios_base::hex);
  std::copy(_mem.begin(), _mem.end(), std::ostream_iterator<u32_t>(file,"\n"));
#endif
  file.close();
  std::cout << "Wrote " << _mem.size() << " words to " << _dump << std::endl;
}

// 200 miliseconds
#define DUMMYSLEEP 200*1000 
//____________________________________________________________________
int 
RcuCE::DcscDummy::Write(u32_t address, u32_t data) 
{ 
  int ret = 1;
  try {
    FeeServer::Guard g(_mutex);
    if (address < _mem.size()) _mem[address] = data;
    if (_rev_version >= 0x080400 && address == 0x5304)
      SetRmemFromImem();
    if (address == 0x8010) 
      SetResultFromCmd();
    // Sleep a while 
    usleep(_sleep * 1000);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    ret = -CANNOT_LOCK;
  }
  return ret;
}

//____________________________________________________________________
int 
RcuCE::DcscDummy::Read(u32_t address, u32_t& data) 
{
#if 0
  std::cout << "RcuCE::DcscDummy::Read(0x" 
	    << std::hex << address << std::dec << "," 
	    << &data << ")" << std::endl;
#endif
  // std::cout << "Read address 0x" << std::hex << address 
  //           << std::dec << std::endl;
  int ret = 1;
  try {
    FeeServer::Guard g(_mutex);
    if ((_rev_version >= 0x080400 && address == 0x5106) || 
	(_rev_version <= 0x060504 && address == 0x8006) || 
	address == 0x8008) {
      data = _version; // 60504;
      std::cout << "Reading FW version from 0x" << std::hex << address 
		<< std::dec << " returning " << data << std::endl;
    }
    else if (address == 0x7800)                            data = 0x0;
    else if (_rev_version < 0x080400 && address == 0x5106) ret  = -BAD_PARAMETER;
    else if (_rev_version > 0x060504 && address == 0x8006) ret  = -BAD_PARAMETER;
    else if (address < _mem.size())                        data = _mem[address];
    // Sleep a while 
    usleep(_sleep * 1000);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    ret = -CANNOT_LOCK;
  }
  return ret;
}

//____________________________________________________________________
int 
RcuCE::DcscDummy::Write(u32_t address, const unsigned int* data, size_t n, 
			WordSize ws) 
{
  // int ret = 0;
  // DGUARD(1,"Write(0x%x,%p,%d,%d)",address,data,n,ws);
#if 0
  std::cout << "RcuCE::DcscDummy::Write(0x" 
	    << std::hex << address << std::dec << "," 
	    << data << ","
	    << n << ","
	    << ws << ")" << std::endl;
#endif
  try {
    FeeServer::Guard g(_mutex);

    if (!data) return -BAD_PARAMETER;
    if (address >= _mem.size()) return -BAD_PARAMETER;
    if (ws != Word8 && ws != Word16 && ws != Word32) {
      std::cerr << "DcscDummy does not support 10bit words" << std::endl;
      return -BAD_PARAMETER;
    }
    
    int size = (address + n < _mem.size() ? n : _mem.size() - address);
    if (ws == Word32) {
      std::copy(data, &(data[size]), &(_mem[address]));
      if (_rev_version >= 0x080400 && address == 0x5304)
	SetRmemFromImem();
      // Sleep a while 
      usleep(_sleep * 1000);
      return size;
    }
  
    unsigned char* ptr = (unsigned char*)(data);
    for (int i = 0; i < size; i++) {
      _mem[i] = (ws == Word8 ? *ptr : *((unsigned short*)ptr));
      ptr += (ws == Word8 ? 1 : 2);
    }
    if (_rev_version >= 0x080400 && address == 0x5304)
      SetRmemFromImem();
    // Sleep a while 
    usleep(_sleep * 1000);
    return size;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -CANNOT_LOCK;
  }
  return 0;
}

//____________________________________________________________________
int 
RcuCE::DcscDummy::Read(u32_t address, unsigned int* data, size_t n) 
{
#if 0
  std::cout << "RcuCE::DcscDummy::Read(0x" 
	    << std::hex << address << std::dec << "," 
	    << data << ","
	    << n << ")" << std::endl;
#endif
  try {
    FeeServer::Guard g(_mutex);

    if (!data)                  return -BAD_PARAMETER;
    if (address >= _mem.size()) return -BAD_PARAMETER;
    int size = (address + n < _mem.size() ? n : _mem.size() - address);
    std::copy(&(_mem[address]), &(_mem[address+n]), data);
    // Sleep a while 
    usleep(_sleep * 1000);
    return size;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -CANNOT_LOCK;
  }
  return 0;
}

//____________________________________________________________________
void
RcuCE::DcscDummy::SetResultFromCmd()
{
  unsigned int scadd = _mem[0x8005];
  unsigned int fec   = (scadd >> 8) & 0x1f;
  unsigned int res   = _mem[0x8002];
  _mem[0x8002]       = (fec << 16) | (res & 0xFFFF);
  // std::cout << "Setting result register from SCADD to 0x"  
  // 	       << std::hex << std::setfill('0') << std::setw(6) 
  // 	       << _mem[0x8002] 
  // 	       << std::dec << std::setfill(' ') << std::endl;
}

//____________________________________________________________________
void
RcuCE::DcscDummy::SetRmemFromImem()
{
  // Loop over IMEM and make results
  const size_t imem_first    = 0x0;
  const size_t imem_last     = 0x1000;
  const size_t rmem_first    = 0x2000;
  //const size_t rmem_last   = 0x4000;
  const size_t imem_size     = imem_last - imem_first + 1;
  //const size_t rmem_size   = rmem_last - rmem_first + 1;
  const size_t imem_fec_wr1  = (0x2 << 20);
  //const size_t imem_fec_wr2= imem_fec_wr1;
  const size_t imem_fec_rd1  = (0x0 << 20);
  //const size_t imem_fec_rd2= imem_fec_rd2;
  const size_t imem_fec_cmd1 = (0x1 << 20);
  const size_t imem_stop     = (0x3 << 20) | (0x8 << 16);
  const size_t imem_end      = (0x3 << 20) | (0xf << 16);
  const size_t rmem_error    = (0x1 << 20);
  const size_t rmem_stop     = (0x6 << 21); 
  const size_t rmem_end      = (0x7 << 21);
  const size_t rmem_fec_wr   = imem_fec_wr1  << 1;
  const size_t rmem_fec_rd   = imem_fec_rd1  << 1;
  const size_t rmem_fec_cmd  = imem_fec_cmd1 << 1;
  // std::cout << "Setting RMEM from IMEM" << std::endl;
  
  bool atend = false;
  for (size_t i = 0; i < imem_size && !atend; i++) {
    unsigned int ii    = i+imem_first;
    unsigned int ri    = i+rmem_first;
    unsigned int what1 = (_mem[ii] & 0xf00000);
    unsigned int what2 = (_mem[ii] & 0xff0000);
    if      (what1 == imem_fec_wr1)
      _mem[ri] = rmem_fec_wr | (_mem[ii] & 0xfffff);
    else if (what1 == imem_fec_rd1) 
      _mem[ri] = rmem_fec_rd | (_mem[ii] & 0xfffff);
    else if (what1 == imem_fec_cmd1) 
      _mem[ri] = rmem_fec_cmd | (_mem[ii] & 0xfffff);
    else if (what2 == imem_stop) {
      _mem[ri] = rmem_stop;
      atend    = true;
    }
    else if (what2 == imem_end) {
      _mem[ri] = rmem_end;
      atend    = true;
    }
    else {
      std::cerr << "Unknown IMEM instruction: 0x" << std::hex << what1 
		<< std::dec << std::endl;
      _mem[i] = (0xf << 21) | rmem_error;
    }
    // std::cout << std::hex << std::setfill('0') 
    // 	         << "0x" << std::setw(4) << ii  << " => " 
    // 	         << "0x" << std::setw(4) << ri  << ": "
    // 	         << "0x" << std::setw(4) << _mem[ii]  << " => " 
    // 	         << "0x" << std::setw(4) << _mem[ri]  << std::endl;
    // if (i % 4 == 0) 
    //   std::cout << "  0x" << std::setw(4) << ri << ":";
    // std::cout << " 0x" << std::setw(8) << _mem[ri] << std::flush;
    // if (i % 4 == 3) std::cout << std::endl;
  }
  // std::cout << std::setfill(' ') << std::dec << std::endl;
  
}

//____________________________________________________________________
//
// EOF
//
