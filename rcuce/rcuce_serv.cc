#include <rcuce/rcuce_serv.hh>
#include <feeserverxx/fee_serv.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_dbg.hh>
#include <unistd.h>
#include <algorithm>

//____________________________________________________________________
RcuCE::Service::Service()
  : _service(0)
{}

//____________________________________________________________________
RcuCE::Service::Service(const Service& service)
  : _service(service._service)
{}

//____________________________________________________________________
RcuCE::Service&
RcuCE::Service::operator=(const Service& service)
{
  _service = service._service;
  return *this;
}

//____________________________________________________________________
RcuCE::Service::~Service()
{
  // if (_service) delete _service;
}

//____________________________________________________________________
const char* 
RcuCE::Service::Name() const 
{ 
  return (_service?_service->Name().c_str():""); 
}

//====================================================================
RcuCE::ServiceProvider::ServiceProvider(FeeServer::Main& m, 
					unsigned int max_faults,
					unsigned int wait)
  : _main(m), _services(0), _providers(0), _no_erase(false), _wait(wait), 
    _max_faults(max_faults), _n_faults(0)
{}

//____________________________________________________________________
RcuCE::ServiceProvider::~ServiceProvider()
{
  RemoveAllServices();
  RemoveAllProviders();
}

//____________________________________________________________________
int 
RcuCE::ServiceProvider::CheckFaults(int ret)
{
  if (ret < 0) { 
    _n_faults++;
    WARN(_main, ("Provider %p got a fault %d (%d faults so far, %d accepted)", 
		 this, ret, _n_faults, _max_faults));
    if (_n_faults > _max_faults) { 
      WARN(_main, ("Provider %p got too many (%d) faults (%d)", 
		   this, ret, _n_faults, _max_faults));
      _n_faults = 0;
      return ret;
    }
  }
  return 0;
}

//____________________________________________________________________
int
RcuCE::ServiceProvider::UpdateAll()
{
  int ret  = 0;
  int mret = 0;
  for (ServiceList::iterator i=_services.begin(); i != _services.end(); ++i) {
    if ((ret = (*i)->Update()) < 0) {
      WARN(_main, ("Update of %s failed", (*i)->Name()));
      mret = ret;
    }
    usleep(1000 * _wait);
  }
  for (ProviderList::iterator i=_providers.begin(); i!= _providers.end();++i){
    if ((ret = (*i)->UpdateAll()) < 0) {
      WARN(_main, ("Update of provider %p failed %d", (*i), ret));
      mret = ret;
    }
    usleep(1000 * _wait);
  }
  return CheckFaults(mret);
}
//____________________________________________________________________
int
RcuCE::ServiceProvider::UpdateSome()
{
  int ret  = 0;
  int mret = 0;
  for (ServiceList::iterator i = _services.begin(); i != _services.end(); ++i){
    if (!(*i)->Always()) continue;
    if ((ret = (*i)->Update()) < 0) {
      WARN(_main, ("Update of %s failed", (*i)->Name()));
      mret = ret;
    }
    usleep(1000 * _wait);
  }
  
  for (ProviderList::iterator i=_providers.begin(); i!= _providers.end();++i){
    if ((ret = (*i)->UpdateSome()) < 0) {
      WARN(_main, ("Update of provider %p failed: %d", (*i), ret));
      mret = ret;
    }
    usleep(1000 * _wait);
  }
  return CheckFaults(mret);
}


//____________________________________________________________________
bool
RcuCE::ServiceProvider::AddService(Service& s)
{
  ServiceList::iterator i = std::find(_services.begin(), _services.end(), &s);
  if (i != _services.end()) { 
    INFO(_main, ("Service %p already exists", &s));
    return false;
  }
  
  _services.push_back(&s);
  if (s.FeeService()) { 
    INFO(_main, ("Publishing Service %p", &s));
    return (_main.Publish(*(s.FeeService())) >= 0);
  }
  else 
    INFO(_main, ("Not publishing Service %p", &s));
  return true;
}

//____________________________________________________________________
bool
RcuCE::ServiceProvider::RemoveService(Service& s)
{
  ServiceList::iterator i = std::find(_services.begin(), _services.end(), &s);
  if (i == _services.end()) return false;
  
  if (!_no_erase) _services.erase(i);
  INFO(_main, ("Unpublishing %p", &s));
  if (s.FeeService()) _main.Unpublish(s.FeeService()->Name());
  return true;
}

//____________________________________________________________________
bool
RcuCE::ServiceProvider::RemoveAllServices()
{
  DEBUG(_main, ("We have %d services to remove", _services.size()));
  _no_erase = true;
  for (ServiceList::iterator i = _services.begin(); i != _services.end(); ++i){
    DEBUG(_main, ("Will remove service @ %p", *i));
    // if (!*i) continue;
    RemoveService(*(*i));
  }
  _no_erase = false;
  _services.erase(_services.begin(),_services.end());
  return true;
}

//____________________________________________________________________
bool
RcuCE::ServiceProvider::AddProvider(ServiceProvider& s)
{
  ProviderList::iterator i = std::find(_providers.begin(),_providers.end(),&s);
  if (i != _providers.end()) {
    INFO(_main, ("Provider %p already exists", &s));
    return false;
  }
  
  _providers.push_back(&s);
  INFO(_main, ("Added provider %p", &s));
  return true;
}

//____________________________________________________________________
bool
RcuCE::ServiceProvider::RemoveProvider(ServiceProvider& s)
{
  ProviderList::iterator i = std::find(_providers.begin(),_providers.end(),&s);
  if (i == _providers.end()) return false;
  
  INFO(_main, ("Removing provider %p", &s));
  if (!_no_erase) _providers.erase(i);
  return true;
}

//____________________________________________________________________
bool
RcuCE::ServiceProvider::RemoveAllProviders()
{
  for (ProviderList::iterator i = _providers.begin(); 
       i != _providers.end(); ++i)
    RemoveProvider(*(*i));
  // _providers.erase(_providers.begin(),_providers.end());
  return true;
}

//____________________________________________________________________
void
RcuCE::ServiceProvider::GetServiceNames(std::vector<std::string>& names)
{
  for (ServiceList::iterator i = _services.begin(); i != _services.end(); ++i)
    names.push_back((*i)->Name());
  for (ProviderList::iterator i=_providers.begin(); i != _providers.end();++i)
    (*i)->GetServiceNames(names);
}

  

//____________________________________________________________________
//
// EOF
//
