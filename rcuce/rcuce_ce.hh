#ifndef RCUCE_CE_HH
#define RCUCE_CE_HH
#include <feeserverxx/fee_ce.hh>
#include <rcuce/rcuce_dcsc.hh>
#include <rcuce/rcuce_handler.hh>
#include <rcuce/rcuce_serv.hh>
#include <rcuce/rcuce_bfsm.hh>

/** @namespace RcuCE 
    Name space for all RcuCE classes */
namespace RcuCE 
{
  /** @defgroup rcuce_core Core classes */
  // Forward decl 
  class CommandHandler;

  // Error codes
  enum {  
    SUCCESS, 
    INCONSISTENCY,
    PROTOCOL_ERROR, 
    BAD_PARAMETER,
    NOT_ENOUGH_DATA
    
  };
  //__________________________________________________________________
  /** @class ControlEngine rcuce_ce.hh <rcuce/rcuce_ce.hh>
      @brief RCU control engine 
      @ingroup rcuce_core */ 
  class ControlEngine : public FeeServer::ControlEngine, 
			public ServiceProvider,
			public CommandHandler, 
			public BasicFsmParent 
  {
  public:
    /** Constructor 
	@param main Reference to FeeServer main object 
	@param wait Time (in milliseconds) to wait between each update */
    ControlEngine(FeeServer::Main& main, unsigned int wait=2000);
    /** Destructor */ 
    virtual ~ControlEngine();

    /** Main thread of the control engine */
    virtual void Start();

    /** Get the version number of this CE */
    const char* VersionString() const;
    /** Get the package name of this CE */
    const char* PackageString() const;

    /** Command handler 
        @param idata Input data - encodes the command. 
        @param isize Size of @a idata 
        @param odata On return, contains the output data 
        @param osize On return, contains the size of @a odata */
    virtual short Issue(const byte_array&  idata, 
                        size_t             isize, 
                        byte_array&        odata, 
			size_t&            osize);

    /** Check if this handler can handle the command @a cmd with
	parameters @a param. 
	@param group The command group
	@param cmd   The command that we should handle
	@return true if this object can handle the command. */
    virtual bool CanHandle(unsigned char group, unsigned char cmd) const;
    /** Handle a command 
	@see CeGroup
	@param cmd      Command. 
	@param param    Parameter to command @a cmd.
	@param payload  The payload of the command. 
	@param isize    Maximum number of bytes in payload. 
	@param odata    Output data array. 
	@param osize    Number of previously set bytes in @a odata.
	                On return, it contains the new number of valid
			bytes in @a odata.
	@return Number of bytes processed from @a payload or negative
	error code in case of trouble. */
    virtual int Handle(unsigned char cmd,      unsigned short param, 
		       byte_ptr      payload,  size_t  isize, 
		       byte_array&   odata,    size_t& osize);

    /** Notification of update rate */ 
    virtual void UpdateRate(unsigned int rate) {  _wait = rate; }

    /** Clean up handler */ 
    virtual void CleanUp();

    /** Add a command handler */
    virtual bool AddHandler(CommandHandler& handler);
    /** Remove a command handler */
    virtual bool RemoveHandler(CommandHandler& handler);

#if 0
    /** Add a fsm */
    virtual bool AddFsm(BasicFsm& fsm);
    /** Remove a fsm */
    virtual bool RemoveFsm(BasicFsm& fsm);
#endif
  protected: 
#if 0
    /** Find an FSM that can handle action @a action, with identifier
	@a id, and address @a addr, and parameters @a param. 
	@param action  Action to perform 
	@param id      Identifier 
	@param addr    Address 
	@param param   Parameters 
	@return Pointer to FSM or null if not found */
    BasicFsm* FindFsm(unsigned int   action, 
		      unsigned char  id, 
		      unsigned short addr, 
		      unsigned short param) const;
    /** Find an FSM that has a state service with name @a name. 
	@param name Name to look for 
	@return a pointer to the FSM or null if no such FSM was found */
    BasicFsm* FindFsm(const std::string& name) const;
#endif

    /** Process the incoming data. 
	@param payload The payload 
	@param isize   Input size (in bytes) 
	@param odata   Output data 
	@param osize   On return the total number of bytes in @a odata 
	@return number of bytes eaten away from @a payload */
    virtual int ProcessCommands(byte_ptr payload, size_t isize, 
				byte_array& odata, size_t& osize);
    /** @{ 
	@name Commands that we understand */
    /** Handle the SET_SERVICE_VALUE command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @return Number of bytes read from @a payload
        @see CeGroup */
    int SetServiceValue(byte_ptr payload,unsigned short param, size_t isize);
    /** Handle the READ_DEFAULT_SERVICES command 
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return 0
        @see CeGroup */
    int GetServices(byte_array& odata,size_t& osize);
    /** Handle the READ_VALID_FECS command 
        @param param   The parameter
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return Number of bytes read from @a payload
        @see CeGroup */
    int GetFecs(unsigned short param, byte_array& odata, size_t& osize);
    /** Handle the GET_HIGHLEVEL_CMDS command 
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return 0
        @see CeGroup */
    int GetHighLevelCommands(byte_array& odata,size_t& osize);
    /** Handle the EN_SERVICE_UPDATE command 
        @param enable  Whether to enable or not.
        @return Number of bytes read from @a payload
        @see CeGroup */
    int EnableServices(bool enable);
    /** Handle the FORCE_CH_UPDATE command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @return Number of bytes read from @a payload
        @see CeGroup */
    int UpdateService(byte_ptr payload, unsigned short param, size_t isize);
    /** Handle the TRIGGER_TRANSITION command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @return Number of bytes read from @a payload
        @see CeGroup */
    int TriggerTransition(byte_ptr payload, unsigned short param, size_t isize);
    /** Handle the GET_STATES command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return Number of bytes read from @a payload
        @see CeGroup */
    int GetStates(byte_ptr payload, unsigned short param, size_t isize,
		  byte_array& odata, size_t& osize);
    /** Handle the GET_TRANSITIONS command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return Number of bytes read from @a payload
        @see CeGroup */
    int GetTransitions(byte_ptr payload, unsigned short param, size_t isize, 
		       byte_array& odata, size_t& osize);
    /** Handle the CONFIGURE command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return Number of bytes read from @a payload
        @see CeGroup */
    int Configure(byte_ptr payload, unsigned short param, size_t isize,
		  byte_array& odata, size_t& osize);
    /** Handle the VERIFICATION command 
        @param payload The payload
        @param param   The parameter
        @param isize   Input size of @a payload (for checks)
        @param odata   Output data array 
        @param osize   On input, the number of valid words in @a odata,
                       on output possibly updated.
        @return Number of bytes read from @a payload
        @see CeGroup */
    int Verification(byte_ptr payload, unsigned short param, size_t isize,
		     byte_array& odata, size_t& osize);
    /** Handle the CONFIGURE_END command 
        @param payload The payload
        @param isize   Input size of @a payload (for checks)
        @return Number of bytes read from @a payload
        @see CeGroup */
    int EndConfigure(byte_ptr payload, size_t isize);
    /** Handle the EXTERNAL_CONFIGURATION command 
        @param param   The parameter
        @return 0
        @see CeGroup */
    int ExternalConfigure(unsigned short param); 
    /** @} */
  protected:
    void PrintInputBuffer(byte_ptr payload, size_t isize, 
			  byte_ptr cur, size_t ncur) const;
    /** Interface to the lower level driver */ 
    // Dcsc _dcsc;
    /** Typedef of handler list */
    typedef std::list<CommandHandler*> HandlerList;
    /** List of command handlers */
    HandlerList _handlers;
#if  0
    /** Typedef of fsm list */
    typedef std::list<BasicFsm*> FsmList;
    /** List of command handlers */
    FsmList _fsms;
#endif
    /** State service */
    StateService _state;
    /** Whether to relax command versioning checks */
    bool _relax_check;
    /** The current configuration nesting level */
    int _configure_lvl;
    /** Time to wait, in milliseconds between each wait */
    unsigned int _wait;
  };
}

#endif
//
// EOF
//
 
