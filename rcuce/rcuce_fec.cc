#include <rcuce/rcuce_fec.hh>
#include <rcuce/rcuce_rcu.hh>
#include <rcuce/rcuce_serv.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_dbg.hh>
#include <sstream>
#include <iomanip>
#include <cstdio>
#include <cstdarg>
#include <unistd.h>

//____________________________________________________________________
bool RcuCE::Fec::_enabled = true;
std::ostream* RcuCE::Fec::_log_file = 0;

//____________________________________________________________________
RcuCE::Fec::Fec(unsigned char addr, FeeServer::Main& main, Rcu& rcu)
  : ServiceProvider(main,3,10), 
    _rcu(rcu), 
    _address(addr), 
    _cleanup()
{
  DGUARD(4,"Constructing FEC 0x%02x", _address);
}

//____________________________________________________________________
RcuCE::Fec::~Fec()
{
  DGUARD(2,"Removing all services from FEC 0x%02x", _address);
  RemoveAllServices();
  for (CleanupList::iterator i = _cleanup.begin(); i != _cleanup.end(); ++i) {
    delete *i;
  }
  _cleanup.erase(_cleanup.begin(),_cleanup.end());
}

//____________________________________________________________________
int
RcuCE::Fec::Read(unsigned char addr, unsigned short& ret)
{
  DGUARD(4,"Read FEC 0x%02x register 0x%x", _address, addr);
  // return 0;
  return _rcu.ReadViaI2C(_address, addr, ret);
}

//____________________________________________________________________
int
RcuCE::Fec::ReadBus(unsigned char addr, unsigned short& ret)
{
  DGUARD(4,"Read FEC 0x%02x register 0x%x", _address, addr);
  // return 0;
  return _rcu.ReadViaBus(_address, 0, 0, addr, ret);
}

//____________________________________________________________________
std::string 
RcuCE::Fec::MakeServiceName(const std::string& name) const
{
#if 0
  std::stringstream s;
  s << std::setfill('0') << std::setw(2) << _address 
    << "_" << name;
  static std::string ret;
  ret = s.str();
#endif 
  char buf[1024];
  sprintf(buf, "%02d_%s", _address, name.c_str());

  return std::string(buf);
}

//__________________________________________________________________
bool
RcuCE::Fec::AddStatusService() 
{
  DGUARD(2,"Adding status service");
  std::string n = MakeServiceName("FAULT_CNT");
  // std::cout << "New service name is " << n.c_str() << std::endl;
  Service* s = 0;
  _cleanup.push_back(s = new StatusService(*this, n));
  if (!s) { 
    std::cerr << "Failed to make StatusService " << n << std::endl;
    _cleanup.pop_back();
    return 0;
  }
  if (!AddService(*s)) { 
    delete s;
    _cleanup.pop_back();
    return 0;
  }
  return !(s == 0);
}

//__________________________________________________________________
int
RcuCE::Fec::UpdateAll()
{
  if (!_enabled) { 
    INFO(_main, ("FEC updates temporarily disabled, sleeping a while"));
    usleep(100);
    return 0;
  }
  return ServiceProvider::UpdateAll();
}

//__________________________________________________________________
int
RcuCE::Fec::UpdateSome()
{
  if (!_enabled) {
    INFO(_main, ("FEC updates temporarily disabled, sleeping a while"));
    usleep(100);
    return 0;
  }
  return ServiceProvider::UpdateSome();
}
//____________________________________________________________________
void
RcuCE::Fec::LogService(const char* format, ...)
{
  if (!_log_file) return; // Do nothing 
  va_list ap;
  va_start(ap, format);
  char buf[1024];
  vsnprintf(buf, sizeof(buf)-1, format, ap);
  va_end(ap);

  time_t     epoch_now = time(NULL);
  struct tm* local_now = localtime(&epoch_now);
  char dbuf[20];
  strftime(dbuf, sizeof(dbuf)-1, "%Y-%m-%d %H:%M:%S", local_now);
  dbuf[19] = '\0';
  (*_log_file) << dbuf << "," << buf << std::endl;
}

//====================================================================
RcuCE::Fec::StatusService::StatusService(Fec& fec, const std::string& name)
  : _serv(name), _fec(fec)
{
  _service = &_serv;
}

//____________________________________________________________________
int
RcuCE::Fec::StatusService::Update()
{
  _serv = std::max(int(_fec.GetNFaults()) - 
		   int(_fec.GetMaxFaults()), 0);
  return 0;
}


//====================================================================
//
// Factory
//____________________________________________________________________
RcuCE::FecFactory::FecFactory(FeeServer::Main& m, Rcu& r)
  : _main(m), 
    _rcu(r), 
    _fecs()
{}

//____________________________________________________________________
RcuCE::Fec*
RcuCE::FecFactory::Make(unsigned char addr) 
{
  DGUARD(3,"Will make object for FEC 0x%02x", addr);
  FecMap::iterator i = _fecs.find(addr);
  if (i != _fecs.end()) return i->second;

  DMSG(2,"Didn't get FEC # 0x%02x from cache, will make it", unsigned(addr));
  Fec* fec = MakeFec(addr);
  if (!fec) return 0;

  _rcu.AddProvider(*fec);
  return _fecs[addr] = fec;
}

//____________________________________________________________________
void
RcuCE::FecFactory::Remove(unsigned char addr) 
{
  DGUARD(3,"Will remove object for FEC 0x%02x", addr);
  FecMap::iterator i = _fecs.find(addr);
  if (i == _fecs.end()) return;

  
  DMSG(2,"Removing provider for  FEC # 0x%02x", unsigned(addr));
  _rcu.RemoveProvider(*(i->second));
  delete i->second;
  _fecs.erase(i);
}

//____________________________________________________________________
//
// EOF
//
