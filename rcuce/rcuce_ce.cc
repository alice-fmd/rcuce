#include <rcuce/rcuce_ce.hh>
#include <feeserverxx/fee_dbg.hh>
#include <rcuce/rcuce_handler.hh>
#include <rcuce/rcuce_bits.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_packet.hh>
#include <cerrno>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <sstream>
#include <iomanip>
#define MAIN FeeServer::ControlEngine::_main

//____________________________________________________________________
RcuCE::ControlEngine::ControlEngine(FeeServer::Main& main, unsigned int wait)
  : FeeServer::ControlEngine(main), 
    ServiceProvider(main), 
    CommandHandler(0x1), 
    BasicFsmParent(FsmId::Ce),
    _handlers(), 
    // _fsms(),
    _state("MAIN_STATE", *this),
    _relax_check(false),
    _configure_lvl(0), 
    _wait(wait)
    // _dcsc()
{
  AddHandler(*this);
  // AddFsm(*this);
  AddService(_state);
}

//____________________________________________________________________
RcuCE::ControlEngine::~ControlEngine()
{}

//____________________________________________________________________
void
RcuCE::ControlEngine::CleanUp()
{
  DGUARD(5,"Clean-up");
  for (HandlerList::iterator i = _handlers.begin(); i!=_handlers.end(); ++i) {
    if (!(*i) || (*i) == this) continue;
    (*i)->CleanUp();
  }
}

//____________________________________________________________________
const char*
RcuCE::ControlEngine::VersionString() const
{
  return PACKAGE_VERSION;
}

//____________________________________________________________________
const char*
RcuCE::ControlEngine::PackageString() const
{
  return PACKAGE_NAME;
}

//____________________________________________________________________
void
RcuCE::ControlEngine::Start()
{
  // If we couldn't open the interface, return immediately, so that we
  // never signal ready to FeeServer 
  // if (!_dcsc.IsOpen()) return; 
  DGUARD(5,"CE start");
  SetState(CommonStates::Idle);
  
  // Signal that we're ready. 
  Ready();
  SetState(CommonStates::Standby);

  // From now on, we free-rolling in our own thread. 
  sleep(2);
  INFO(MAIN, ("Starting periodic reads"));
  while (true) {
    DGUARD(10,"Updating all");
    State state = GetState();
    int   ret   = 0;
    switch (state) { 
    case CommonStates::Idle:
    case CommonStates::Error:
    case CommonStates::Standby:
    case CommonStates::Mixed:
      ret = UpdateSome(); break;
    case CommonStates::Ready:
      ret = UpdateAll(); break;
    case CommonStates::Downloading:
      break;
    }
    if (ret < 0) return;
    usleep(_wait * 1000);
    TestCancel();
  }
}

//____________________________________________________________________
void 
RcuCE::ControlEngine::PrintInputBuffer(byte_ptr payload, size_t isize, 
				       byte_ptr cur, size_t) const
{
  // const size_t int_size  = sizeof(unsigned int);
  // byte_ptr     sob       = payload;
  byte_ptr     ptr       = payload;
  size_t       processed = 0;
  unsigned int word      = 0;

  enum { 
    kInit, 
    kHeader, 
    kPayload
  } state = kInit;

  std::cout << " # byte |    word    |        bytes            \n"
	    << "--------+------------+-------------------------"
	    << std::endl;
  do { 
    if ((processed % 4) == 0) {
      word = *((unsigned int*)ptr);

      std::cout << "\n " << std::setfill(' ') << std::dec 
		<< std::setw(6) << processed << " | ";

      if (state == kInit) { 
	state = kHeader;
	
	unsigned char  group  = GetGroup(word);
	unsigned char  cmd    = GetCommand(word);
	unsigned short param  = GetParameter(word);

	std::cout << std::setw(10) << "Header" << std::hex   << " | "        
		  << "group=0x"  << std::setw(1) << short(group) 
		  << " cmd=0x"   << std::setw(2) << short(cmd)
		  << " param=0x" << std::setw(4) << param 
		  << std::flush;
      }
      else if ((word & EndMask) == EndMarker) {
	std::cout << std::setw(10) << "Trailer" << " | " << std::flush;
	state = kInit;
      }
      else 
	std::cout << "0x" << std::setfill('0') << std::hex 
		  << std::setw(8) << word << " | " << std::flush;
    }
    std::cout << " 0x" << std::setw(2) << unsigned(*ptr) 
	      << (ptr == cur ? '*' : ' ');
    ptr++;
    processed++;
  } while (ptr < payload+isize);
}

//____________________________________________________________________
int
RcuCE::ControlEngine::ProcessCommands(byte_ptr payload, size_t isize, 
				      byte_array& odata, size_t& osize)
{
  DGUARD(5,"ProcessCommands(%p,%d,%p,%d[%d])",payload,isize,&(odata[0]),
	 osize, odata.size());
  int          ret       = 0;
  const size_t int_size  = sizeof(unsigned int);
  byte_ptr     sob       = payload;
  byte_ptr     ptr       = payload;
  size_t       processed = 0;

  // Check that we've actually got data. 
  if (isize < int_size) {
    WARN(MAIN,("Less than %d bytes in payload (%d bytes)",int_size, isize));
    ret = -BAD_PARAMETER;
    return ret; // break;
  }

  // Loop over the input data. 
  do {
    // Check the consistency of the data pointer.
    if (ptr != sob + processed) {
      WARN(MAIN,("Current pointer %p not consistent with bookkeeping %p+%d=%p",
	    ptr, sob, processed, sob+processed));
      PrintInputBuffer(payload, isize, ptr, processed);
      ret = -INCONSISTENCY;
      break;
    }
    
    // If we have less than a word left, we're done. 
    if (isize - processed < int_size) {
      WARN(MAIN, ("Less than %d bytes in payload (%d bytes)", 
	    int_size, isize-processed));
      PrintInputBuffer(payload, isize, ptr, processed);
      ret = 0;
      continue;
    }
    
    // Get the command header. 
    unsigned int header = *((unsigned int*)ptr);
    // If the header is empty, try the next word
    if (header == 0) {
      ptr       += int_size;
      processed += int_size;
      continue;
    }

    //  If the header is not a command, fail.
    if ((header & CeMask) != OurCommand) {
      INFO(MAIN, ("0x%x Not an RcuCE command", header));
      DMSG(4,"0x%x Not an RcuCE command", header);
      PrintInputBuffer(payload, isize, ptr, processed);
      ret = -PROTOCOL_ERROR;
      break;
    }
    ptr       += int_size;
    processed += int_size;
    
    // Now, extract the command, command id, and parameter from the
    // header 
    unsigned char  group  = GetGroup(header);
    unsigned char  cmd    = GetCommand(header);
    unsigned short param  = GetParameter(header);

    // Check all command handlers 
    bool is_handled = false;
    for (HandlerList::iterator i = _handlers.begin(); 
	 i != _handlers.end(); i++) {
      if (!(*i)->CanHandle(group,cmd)) continue;
      ret = (*i)->Handle(cmd, param, ptr, isize - processed, odata, osize);
      is_handled = true;
    }
    if (ret > int(isize - processed)) {
      WARN(MAIN, ("return %d  bigger than remaining idata %d", ret, 
	   (isize - processed)));
      PrintInputBuffer(payload, isize, ptr, processed);
      ret = -PROTOCOL_ERROR;
      break;
    }
    if (!is_handled) {
      WARN(MAIN, ("No-one will handle the command!"));
      PrintInputBuffer(payload, isize, ptr, processed);
      ret = -BAD_PARAMETER;
      break;
    }
    // if (ret == 0) continue;

    // If we got an empty configure command, we don't check for the
    // end-marker, since it's probably not there. 
    if (cmd == CONFIGURE && param <= 0) {
      DMSG(4,"Not checking trailer for command 0x%x w/param %d "
	   "returning %d", cmd, param, ret);
      return ret;
    }

    // Increment pointer with the amount of bytes eaten by handler.
    processed += ret;
    ptr       += ret;
    ret       =  0;

    // Check that we have an end-of-block marker. 
    unsigned int trailer = *((unsigned int*)ptr);
    if ((trailer & EndMask) != EndMarker && !_relax_check) {
      WARN(MAIN, ("trailer 0x%x of command 0x%x (w/param %d) "
		  "does not match an end marker 0x%x", 
		  trailer, cmd, param, EndMarker));
      PrintInputBuffer(payload, isize, ptr, processed);
      ret = -PROTOCOL_ERROR;
      break;
    }
    // Now eat the end-marker 
    processed += int_size;
    ptr       += int_size;
  } while (ret >= 0 && isize > processed);
  return ret;
}


//____________________________________________________________________
short 
RcuCE::ControlEngine::Issue(const byte_array&  idata, 
			    size_t             isize, 
			    byte_array&        odata, 
			    size_t&            osize) 
{
  DGUARD(5,"Issue(%p,%d,%p,%d[%d])", &(idata[0]), isize, &(odata[0]), &osize,
	 odata.size());
  DEBUG(MAIN, ("Handling issue from feeserver core"));
  int            ret          = 0;
  const byte_ptr ptr          = const_cast<const byte_ptr>(&(idata[0]));
  const size_t   int_size     = sizeof(unsigned int);
  size_t         trailer_size = 0;

  // Check if last word is a trailer, and if so, exclude it from the
  // payload sent to the handlers.  
  unsigned int last = (*((unsigned int*)(ptr + isize - int_size)));
  if (isize > 2 * int_size && (last & EndMask) == EndMarker)
    trailer_size = int_size;
  
  // Set the output size to zero
  osize = 0;

 
  DEXEC(5,
	size_t processed = 0;
	PrintInputBuffer(ptr, isize, ptr, processed);
	);

#if 0
  std::cout << "Block received from feeserver:\n";
  const unsigned int* iptr = (const unsigned int*)(&(idata[0]));
  for (size_t i = 0; i < isize / 4; i++) 
    std::cout << std::setw(8) << i << ": 0x" 
	      << std::hex << std::setfill('0') << std::setw(8) << iptr[i] 
	      << std::dec << std::setfill(' ') << std::endl;
#endif    

  ret = ProcessCommands(ptr, isize-trailer_size, odata, osize);

  // return (ret >= 0 ? processed : ret);
  return (ret >= 0 ? 0 : ret);
}

//____________________________________________________________________
bool
RcuCE::ControlEngine::AddHandler(CommandHandler& s)
{
  HandlerList::iterator i = std::find(_handlers.begin(),_handlers.end(),&s);
  if (i != _handlers.end()) return false;
  
  _handlers.push_back(&s);
  return true;
}

//____________________________________________________________________
bool
RcuCE::ControlEngine::RemoveHandler(CommandHandler& s)
{
  HandlerList::iterator i = std::find(_handlers.begin(),_handlers.end(),&s);
  if (i == _handlers.end()) return false;
  
  _handlers.remove(*i);
  return true;
}

//____________________________________________________________________
bool
RcuCE::ControlEngine::CanHandle(unsigned char group, 
				unsigned char /*cmd*/) const
{
  return group == CeId;
}

//____________________________________________________________________
int
RcuCE::ControlEngine::Handle(unsigned char cmd,      unsigned short param, 
			     byte_ptr      payload,  size_t  isize, 
			     byte_array&   odata,    size_t& osize)
{
  const size_t int_size = sizeof(unsigned int);
  DGUARD(4,"Handling command 0x%x", cmd);
  switch (cmd) {
  case SET_BUF_PRINT_SIZE:     return int_size; // Eat 1 word 
  case EN_SERVICE_UPDATE:      return EnableServices(param != 0); 
  case SET_SERVICE_VALUE:      return SetServiceValue(payload,param,isize);
  case READ_DEFAULT_SERVICES:  return GetServices(odata,osize);
  case READ_VALID_FECS:        return GetFecs(param,odata,osize);
  case RELAX_CMD_VERS_CHECK:   _relax_check = (param != 0); return 0;
  case SET_LOGGING_LEVEL:      return 0;
  case GET_HIGHLEVEL_CMDS:     return GetHighLevelCommands(odata,osize);
  case FORCE_CH_UPDATE:        return UpdateService(payload,param,isize);
  case TRIGGER_TRANSITION:     return TriggerTransition(payload,param,isize);
  case GET_STATES:             return GetStates(payload,param,isize,
						odata,osize);
  case GET_TRANSITIONS:        return GetTransitions(payload,param,isize, 
						     odata,osize);
  case CONFIGURE:              return Configure(payload,param,isize,
						odata,osize);
  case VERIFICATION:           return Verification(payload,param,isize,
						   odata,osize);
  case CONFIGURE_END:          return EndConfigure(payload,isize);
  case EXTERNAL_CONFIGURATION: return ExternalConfigure(param); 
  }
  return -BAD_PARAMETER;
}
#if 0
//____________________________________________________________________
bool
RcuCE::ControlEngine::AddFsm(BasicFsm& s)
{
  FsmList::iterator i = std::find(_fsms.begin(),_fsms.end(),&s);
  if (i != _fsms.end()) return false;
  
  _fsms.push_back(&s);
  return true;
}

//____________________________________________________________________
bool
RcuCE::ControlEngine::RemoveFsm(BasicFsm& s)
{
  FsmList::iterator i = std::find(_fsms.begin(),_fsms.end(),&s);
  if (i == _fsms.end()) return false;
  
  _fsms.remove(*i);
  return true;
}

//____________________________________________________________________
RcuCE::BasicFsm*
RcuCE::ControlEngine::FindFsm(unsigned int   action, 
			      unsigned char  id, 
			      unsigned short addr, 
			      unsigned short param) const
{
  for (FsmList::const_iterator i = _fsms.begin(); i != _fsms.end(); ++i) 
    if ((*i)->TryHandle(action, id, addr, param)) return *i;
  return 0;
}
//____________________________________________________________________
RcuCE::BasicFsm*
RcuCE::ControlEngine::FindFsm(const std::string& name) const
{
  for (FsmList::const_iterator i = _fsms.begin(); i != _fsms.end(); ++i) {
    const StateService* s = (*i)->GetStateService();
    if (!s) continue;
    if (name == s->Name()) return (*i);
  }
  return 0;
}
#endif

//____________________________________________________________________
int
RcuCE::ControlEngine::SetServiceValue(byte_ptr       /*payload*/, 
				      unsigned short param,
				      size_t         isize)
{
  if (isize < param + sizeof(int)) {
    WARN(MAIN, ("Not enough data (%d < %d + %d)",isize,param,sizeof(int)));
    return -NOT_ENOUGH_DATA;
  }
  return param + sizeof(int);
}
//____________________________________________________________________
int
RcuCE::ControlEngine::GetServices(byte_array& odata, size_t& osize)
{
  std::vector<std::string> names;
  GetServiceNames(names);
  for (size_t i = 0; i < names.size(); i++) {
    odata.resize(osize + names[i].size()+1);
    std::copy(names[i].begin(),names[i].end(),&(odata[osize]));
    osize += names[i].size();
    odata[osize] = ':';
    osize++;
  }
  return 0;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::GetFecs(unsigned short /*param*/,
			      byte_array&    /*odata*/, 
			      size_t&        /*osize*/)
{
  return 0;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::GetHighLevelCommands(byte_array&    /*odata*/, 
					   size_t&        /*osize*/)
{
  return 0;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::EnableServices(bool /* enable */)
{
  // if (enable) Trigger(CommonActions::Start);
  // else        Trigger(CommonActions::Stop);
  return sizeof(unsigned int);
}
  
//____________________________________________________________________
int
RcuCE::ControlEngine::UpdateService(byte_ptr       payload, 
				    unsigned short param, 
				    size_t         isize)
{
  if (isize < param) {
    WARN(MAIN, ("Not enough data (%d < %d)",isize,param));
    return -NOT_ENOUGH_DATA;
  }
  std::string n(&(payload[0]), &(payload[param]));
  DMSG(3,"Would update %s", n.c_str());
  return param;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::TriggerTransition(byte_ptr       payload, 
					unsigned short param, 
					size_t         isize)
{
  if (isize < param) {
    WARN(MAIN, ("Not enough data (%d < %d)",isize,param));
    return -NOT_ENOUGH_DATA;
  }
  std::vector<char> v(&(payload[0]), &(payload[param]));
  std::stringstream s(&(v[0]));
  std::string fsm;
  std::string act;
  s >> fsm >> act;
  DMSG(4,"Will send action %s to %s", act.c_str(), fsm.c_str());
  BasicFsm* bfsm = FindFsm(fsm);
  if (!bfsm) return param;
  bfsm->Trigger(bfsm->String2Action(act.c_str()));
  
  return param;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::GetStates(byte_ptr       /*payload*/, 
				unsigned short param, 
				size_t         isize, 
				byte_array&    /*odata*/, 
				size_t&        /*osize*/)
{
  if (isize < param) {
    WARN(MAIN, ("Not enough data (%d < %d)",isize,param));
    return -NOT_ENOUGH_DATA;
  }
  return param;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::GetTransitions(byte_ptr       /*payload*/, 
				     unsigned short param, 
				     size_t         isize, 
				     byte_array&    /*odata*/, 
				     size_t&        /*osize*/)
{
  if (isize < param) {
    WARN(MAIN, ("Not enough data (%d < %d)",isize,param));
    return -NOT_ENOUGH_DATA;
  }
  return param;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::Configure(byte_ptr       payload, 
				unsigned short param, 
				size_t         isize, 
				byte_array&    odata, 
				size_t&        osize)
{
  DGUARD(4, "Doing a CONFIGURE (%d,%d)", param, isize);
  const size_t int_size = sizeof(unsigned int);
  // 2 int's for the address and checksum and 2 ints for the end and address
#ifdef REQUIRE_WHOLE_CONFIG_BLOCK
  int term1 = 2;
#else 
  int term1 = (param <= 0 ? 0 : 2);
#endif
  if (isize < param + (term1 + 2) * int_size) {
    WARN(MAIN, ("Not enough data (%d < %d + (%d + 2) * %d)",
		isize,param,term1,int_size));
    return -NOT_ENOUGH_DATA;
  }
  size_t        block_size = param - 2 * int_size;
  unsigned int* end_ptr    = (unsigned int*)(payload + param);
  if (param > 0 && GetCommand(*end_ptr) != CONFIGURE_END) {
    WARN(MAIN, ("Configure command does not end in CONFIGURE_END @ %p",
		payload+isize-2*int_size));
    return -PROTOCOL_ERROR;
  }

  unsigned int hw_address;
  unsigned int check_sum;
  memcpy(&hw_address,payload,            int_size);
  memcpy(&check_sum, payload + int_size, int_size);
  if (param > 0 && GetParameter(*end_ptr) != hw_address) {
    WARN(MAIN, ("End hw 0x%08x address does not correspond to configure " 
		"hw addres 0x%08x", hw_address, GetParameter(*end_ptr)));
    return -PROTOCOL_ERROR;
  }

  int old_lvl = _configure_lvl;
  _configure_lvl++;

  // @todo Here, we should send the action "GO_CONFIGURE" to the
  // relevant state machine. 
  unsigned char  fsm_id    = GetFsmId(hw_address);
  unsigned short fsm_addr  = GetFsmAddress(hw_address);
  unsigned short fsm_param = GetFsmParameter(hw_address);
  unsigned int   fsm_act   = CommonActions::Configure;
  BasicFsm*      fsm       = FindFsm(fsm_act,fsm_id,fsm_addr,fsm_param);
  if (fsm) fsm->Trigger(fsm_act);
  
  // Call ourselves again - but this time we're in another state. 
  int ret = 0;
  if (param > 2 * int_size) {
    // If we have data in the block, then we execute the commands
    // there, and we assume that the payload contains all the commands
    // we need. 
    ret = ProcessCommands(payload + 2 * int_size, block_size, odata, osize);
    _configure_lvl--;
  }

  if (ret < 0) {
    WARN(MAIN, ("Configure command failed with %s", strerror(-ret)));
    return ret;
  }
  if (param > 0 && ret != int(block_size)) {
    WARN(MAIN, ("Inconsistent size %d returned from configure "
		"block (expected %d)", ret, block_size));
    return -INCONSISTENCY;
  }

  if (param > 0 && _configure_lvl != old_lvl) {
    WARN(MAIN, ("Unbalanced CONFIGURE/CONFIGURE_END blocks %d != %d", 
		_configure_lvl, old_lvl));
    return -PROTOCOL_ERROR;
  }
  ret = param + (term1 + 2) * int_size;
  // @todo Here, we should send the command "GO_READY" to the relevant
  // state machine. 
  if (param > 0 && fsm) fsm->Trigger(CommonActions::ConfigureDone);

  return ret;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::Verification(byte_ptr       /*payload*/, 
				   unsigned short param, 
				   size_t         isize, 
				   byte_array&    /*odata*/, 
				   size_t&        /*osize*/)
{
  DGUARD(4, "Doing a VERIFICATION (%d,%d)", param, isize);
  const size_t int_size = sizeof(unsigned int);
  // 2 int's for the address and checksum
  if (param > 0 && isize < param + 2 * int_size) {
    WARN(MAIN, ("Not enough data (%d < %d + 2 * %d)",
		isize,param,int_size));
    return -NOT_ENOUGH_DATA;
  }
  if (_configure_lvl <= 0) {
    WARN(MAIN, ("Verification block outside of configuration block!"));
    return -NOT_ENOUGH_DATA;
  }
  // Verification not used yet! 
  int ret = (param > 0 ? param + 2 * int_size : 0);
  WARN(MAIN, ("Verification not implemented yet - "
	      "ignoring it and returning %d", ret));
    
  return ret;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::EndConfigure(byte_ptr payload, size_t isize)
{
  DGUARD(4,"Doing an END_CONFIGURE (%d,%d)", isize);
  const size_t int_size = sizeof(unsigned int);
  // 2 int's for the address and checksum and 2 ints for the end and address
  if (isize < int_size) {
    WARN(MAIN, ("Not enough data (%d < %d)",isize,int_size));
    return -NOT_ENOUGH_DATA;;
  }

  unsigned int hw_address;
  memcpy(&hw_address,payload,            int_size);
  DMSG(4,"Got CONFIGURE_END for 0x%x @ configure level %d", 
       hw_address,_configure_lvl);
  _configure_lvl--;

  unsigned char  fsm_id    = GetFsmId(hw_address);
  unsigned short fsm_addr  = GetFsmAddress(hw_address);
  unsigned short fsm_param = GetFsmParameter(hw_address);
  unsigned int   fsm_act   = CommonActions::ConfigureDone;
  BasicFsm*      fsm       = FindFsm(fsm_act,fsm_id,fsm_addr,fsm_param);
  if (fsm) fsm->Trigger(fsm_act);
  return int_size;
}
//____________________________________________________________________
int
RcuCE::ControlEngine::ExternalConfigure(unsigned short /*param*/)
{
  WARN(MAIN, ("External configuration not implemented yet!"));
  return 0;
}
  
//____________________________________________________________________
//
// EOF
//

