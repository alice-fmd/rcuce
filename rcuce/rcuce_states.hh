#ifndef RCUCE_STATES_HH
#define RCUCE_STATES_HH

namespace RcuCE
{
  /** @defgroup rcuce_states States of the various state machines. 

      In this group are enumerations of the states of the various
      state machines in this library.  Users can add more states and
      actions to the already defined state machines by calling
      StateMachine::AddState and StateMachine::AddTransition. 
  */
  /** Known FSM ids */ 
  namespace FsmId 
  {
    enum { 
      /** Control engine */ 
      Ce = 0, 
      /** Rcu */ 
      Rcu, 
      /** Front-end card */ 
      Fec, 
      /** ALTRO */ 
      Altro, 
      /** Channel */ 
      Channel
    };
  }
  
  /** @name Utility functions 
      @ingroup rcuce_states 
      This is a set of inline utility functions for state machine
      handling */
  /** Get the finite state machine identifier */
  inline unsigned char GetFsmId(unsigned int data) { 
    return ((data >> 12) & 0xf);
  }
  /** Get the address for the finite state machine */ 
  inline unsigned short GetFsmAddress(unsigned int data) { 
    return (data & 0xFFF);
  }
  /** Get the parameter for the finite state machine */ 
  inline unsigned short GetFsmParameter(unsigned int data) { 
    return ((data > 16) & 0xFFFF);
  }
  /** Make a FSM header 
      @param id 
      @param address 
      @param param 
      @return the FSM header */ 
  inline unsigned int MakeFsmHeader(unsigned char id, unsigned short address, 
				    unsigned short param) { 
    return ((address & 0xfff) | ((id & 0xf) << 12) | ((param & 0xffff) << 16));
  }

  //__________________________________________________________________
  /** @name Basic (common) states and actions 
      @ingroup rcuce_states */
  /** Common states */ 
  namespace CommonStates { 
    enum {
      /** Idle state (or off) */ 
      Idle = 1,
      /** Standby state */ 
      Standby, 
      /** When downloading configurations */ 
      Downloading, 
      /** When configured */ 
      Ready, 
      /** Error state */ 
      Mixed,
      /** Mixed parent state */ 
      Error, 
      /** Running state */
      Running
    };
  }
  /** Common actions */ 
  namespace CommonActions { 
    enum {
      /** Go to the idle state */
      GoIdle = 1,
      /** Go to the standby state */
      GoStandby,
      /** Configure */ 
      Configure, 
      /** End of configuration */ 
      ConfigureDone, 
    };
  }
}
#endif
//
// EOF
//


    
  
  
