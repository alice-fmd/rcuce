#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_scmd.hh>
#include <feeserverxx/fee_servt.hh>
#include <rcuce/rcuce_ce.hh>
#include <rcuce/rcuce_rcu.hh>
#include <rcuce/rcuce_fec.hh>
#include <rcuce/rcuce_dcsc.hh>
#include <feeserverxx/fee_dbg.hh>
#include <rcuce/rcuce_sig.hh>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cerrno>
#include <cstring>

namespace {
  bool _emul = false;
}

//____________________________________________________________________
struct Fec : public RcuCE::Fec
{
  //__________________________________________________________________
  struct State : public RcuCE::Service 
  {
    State(const std::string s, int& state) 
      : _serv(s), _state(state) 
    { _service = &_serv; }
    int Update() {
      _serv = _state;
      return 0;
    }
    FeeServer::IntService _serv;
    int& _state;
  };
  //__________________________________________________________________
  Fec(unsigned short addr, FeeServer::Main& m, RcuCE::Rcu& r) 
    : RcuCE::Fec(addr, m, r), 
      _csr0(0),
      _temp(0), 
      _ac(0),
      _dc(0),
      _av(0),
      _dv(0),
      _state_service(0), 
      _state(0)
  {
    _csr0 = AddService<int,FeeServer::IntTrait>("CSR0", 0x13);
    _temp = AddService<int,FeeServer::IntTrait>("TEMP", 0x6);
    _ac   = AddService<int,FeeServer::IntTrait>("AC",   0x7);
    _dc   = AddService<int,FeeServer::IntTrait>("DC",   0x8);
    _av   = AddService<int,FeeServer::IntTrait>("AV",   0x9);
    _dv   = AddService<int,FeeServer::IntTrait>("DV",   0xA);
    std::stringstream s;
    s << std::setfill('0') << std::setw(2) << addr << "_STATE";
    _state_service = new State(s.str(), _state);
    AddService(*_state_service);
  }
  //__________________________________________________________________
  virtual ~Fec() 
  {
    RemoveService(*_state_service);
    delete _state_service;
  }
  Fec(const Fec&);
  Fec& operator=(const Fec&);
  //__________________________________________________________________
  int UpdateAll() 
  {
    DGUARD(2,"Updating all services for 0x%02x", _address);
    int ret = 0;
    ret = _temp->Update();
    ret = _csr0->Update();
    ret = _ac->Update();
    ret = _dc->Update();
    ret = _av->Update();
    ret = _dv->Update();
    _state = 0;
    if (ret < 0) {
      switch (-ret) {
      case EBADF:  _state = 1; break; // "1" means dead 
      case EAGAIN: _state = 2; break; // "2" means not-responding
      default:     _state = 0; break; // "0" OK
      }
    }
    if (_emul) { 
      RcuCE::Service*        s[] = { _temp, _ac, _dc, _av, _dv, 0 };
      RcuCE::Service**       p   = s;
      FeeServer::IntService* is  = 0;
      while (*p) { 
	is = dynamic_cast<FeeServer::IntService*>((*p)->FeeService());
	if (is) *is = int(float(rand()) / RAND_MAX * 0x3FF);
	p++;
      }
    }
    return 0;
  }
  //__________________________________________________________________
  RcuCE::Service* _csr0;
  RcuCE::Service* _temp;
  RcuCE::Service* _ac;
  RcuCE::Service* _dc;
  RcuCE::Service* _av;
  RcuCE::Service* _dv;
  State* _state_service;
  int _state;
};

//____________________________________________________________________
struct FecFactory : public RcuCE::FecFactory 
{
  FecFactory(FeeServer::Main& m, RcuCE::Rcu& r) 
    : RcuCE::FecFactory(m,r)
  {}
  RcuCE::Fec* MakeFec(unsigned char addr) 
  {
    std::cout << "Making FEC 0x" << std::setfill('0') 
	      << std::hex << int(addr) << std::setfill(' ') 
	      << std::dec << std::endl;
    return new Fec(addr, _main, _rcu);
  }
};

template <typename T>
void
str2val(const char* str, T& val)
{
  std::stringstream s(str);
  if (str[0] == '0') { 
    if (str[1] == 'x') s << std::hex;
    else               s << std::oct;
  }
  s >> val;
}

//____________________________________________________________________
int 
main(int argc, char** argv) 
{
  std::string  name     = "FMD-FEE_0_0_0";
  std::string  detector = "FMD";
  std::string  dns      = "localhost";
  std::string  dump     = "";
  bool         debug    = false;
  bool         verbose  = false;
  bool         info     = false;
  unsigned int fwvers   = 0x408;
  unsigned int trace    = 0;
  for (int i = 1; i < argc; i++) {
    std::cout << "Proccesing option " << i << "/" << argc << ": " 
	      << argv[i] << std::endl;
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': 
	std::cout << "Usage: " << argv[0] << " OPTIONS\n\n" 
		  << "Options:\n" 
		  << "\t-h\t\tThis help\n" 
		  << "\t-n NAME\t\tSet name of server\n" 
		  << "\t-N HOST\t\tSet the DIM DNS host name\n"
		  << "\t-D DETECTOR\tSet detector name\n" 
		  << "\t-e\t\tEmulation of DCSC\n"
		  << "\t-d\t\tEnable debug output\n"
		  << "\t-M FILE\t\tMemory dump file\n"
		  << "\t-V NUM\t\tSet RCU FW version number for emulation\n"
		  << "\t-t LEVEL\t\tSet trace level\n"
		  << std::endl;
	return 0;
      case 'n': name     = argv[i+1]; i++; break;
      case 'N': dns      = argv[i+1]; i++; break;
      case 'D': detector = argv[i+1]; i++; break;
      case 'M': dump     = argv[i+1]; i++; break;
      case 'e': _emul    = true; break;
      case 'd': debug    = true; break;
      case 'v': verbose  = true; break;
      case 'i': info     = true; break;
      case 'V': str2val(argv[++i], fwvers); break;
      case 't': str2val(argv[++i], trace); break;
      default: 
	std::cerr << argv[0] << ": unknown option " << argv[i] << std::endl;
	return 1;
      }
    }
  }
  DTRACE(trace);
  //EndOptions
  
  install_signal_handler();
  
  std::cout << "Creating feeserver" << std::endl;
  FeeServer::Main m(name, detector, dns);
  unsigned int mask = (FeeServer::Message::Warning | 
		       FeeServer::Message::Error | 
		       FeeServer::Message::AuditFailure | 
		       FeeServer::Message::AuditSuccess |
		       FeeServer::Message::Alarm);
  // m.GetLogLevel();
  if (debug) mask |= FeeServer::Message::Debug;
  if (info)  mask |= FeeServer::Message::Info;
  m.SetLogLevel(mask);

  // m.AddCommand(new UpdateServer(m));
  m.AddCommand(new FeeServer::RestartServer(m));
  // m.AddCommand(new FeeServer::RebootMachine(m));
  // m.AddCommand(new FeeServer::ShutdownMachine(m));
  m.AddCommand(new FeeServer::ExitServer(m));
  m.AddCommand(new FeeServer::SetDeadband(m));
  m.AddCommand(new FeeServer::GetDeadband(m));
  m.AddCommand(new FeeServer::SetIssueTimeout(m));
  m.AddCommand(new FeeServer::GetIssueTimeout(m));
  m.AddCommand(new FeeServer::SetUpdateRate(m));
  m.AddCommand(new FeeServer::GetUpdateRate(m));
  m.AddCommand(new FeeServer::SetLogLevel(m));
  m.AddCommand(new FeeServer::GetLogLevel(m));
  m.AddCommand(new FeeServer::ShowServices(m));
  //EndFee

  RcuCE::ControlEngine ce(m);
  m.SetCtrlEngine(ce);
  
  RcuCE::Dcsc* dcsc = 0;
  if (_emul) dcsc = new RcuCE::DcscDummy(fwvers, dump);
  else       dcsc = new RcuCE::Dcsc("",verbose);

  RcuCE::Rcu rcu(*dcsc, m);
  FecFactory f(m, rcu);
  rcu.SetFecFactory(f);

  ce.AddProvider(rcu);
  ce.AddHandler(rcu);
  ce.AddHandler(*dcsc);
  ce.AddFsm(rcu);
  //EndRCU
  
  std::cout << "Running fee server " << std::endl;
  return m.Run();
}
//
// EOF
//
