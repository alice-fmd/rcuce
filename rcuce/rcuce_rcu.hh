#ifndef RCUCE_RCU_HH
#define RCUCE_RCU_HH
#include <rcuce/rcuce_handler.hh>
#include <rcuce/rcuce_bfsm.hh>
#include <rcuce/rcuce_dcsc.hh>
#include <rcuce/rcuce_cb.hh>
#include <feeserverxx/fee_serv.hh>
#include <iosfwd>

namespace FeeServer 
{
  class Main;
}

namespace RcuCE
{

  // Forward declaration 
  class Dcsc;
  class ERRSTService;
  class AFLService;
  class FecFactory;
  class HITLISTService;
  class TRIGService;
  class RUNService;
  class TEMPService;

  /** @defgroup rcuce_rcu RCU classes 

      In this group are classes for accesing the RCU, as well as
      services, command handling, and state machines for the RCU. 
      
      @ingroup rcuce_core */
  /** @class Rcu rcuce_rcu.hh <rcuce/rcuce_rcu.hh> 
      @brief Class to read/write RCU registers and memories.  Users
      can overload this class to do more stuff, if needed. 
      @ingroup rcuce_rcu */
  class Rcu : public ServiceProvider, 
	      public CommandHandler, 
	      public BasicFsm
  {
  public:
    /** Code book */
    CodeBook _code_book;

    /**
     * Error codes 
     */
    enum { 
      SUCCESS,      // Success 
      UNKNOWN,      // Unknown error 
      DCSC,         // Error from DCSC interface 
      BAD_PARAMETER, // Bad parameter passed 
      IMEM_ABORTED,
      BUS_BUSY,
      BUS_TIMEOUT, 
      BUS_ERROR, 
      ADDRESS_ERROR, 
      BAD_PATTERN,
      NOT_ACTIVE,
      NO_ANSWER
    };
    /** 
     * Constructor 
     *
     * @param dcsc Interface 
     * @param main Main object 
     */
    Rcu(Dcsc& dcsc, FeeServer::Main& main);
    /** Destructor */ 
    virtual ~Rcu();

    /** 
     * Add a service to check the hit list for errors, and make the
     * state change appropriately 
     */
    void MonitorHitList();
    /** 
     * Add a service to monitor the trigger configuration. 
     * 
     */
    void MonitorTrigConf();
    /** 
     * Add a service to update the running state (from TTCrx)
     * 
     */
    void MonitorRunState();
    /** 
     * Add a service to update the interrupt handlers status memory
     * 
     */
    void MonitorStatusMemory();
    /** 
     * Add a service to monitor RCU temperatures
     * 
     */
    void MonitorTemps();
    /** 
     * Update some services - in particular those that return true
     * when calling the member function Service::Always from it.
     *
     * @return 0 on success, error code otherwise 
     */ 
    virtual int UpdateSome();
    /** 
     * Update all services 
     *
     * @return negative value on error 
     */
    virtual int UpdateAll();
    /** 
     * Set the FecFactory object to use 
     *
     * @param factory Reference to the factory 
     */
    void SetFecFactory(FecFactory& factory) { _factory = &factory; }

    /** 
     * @{ 
     * @name Command handler member functions 
     */
    /** 
     * Handle a command 
     * @see RcuGroup
     *
     * @param cmd       Command. 
     * @param parameter Parameter to command @a cmd.
     * @param idata     The payload of the command. 
     * @param isize     Maximum number of bytes in payload. 
     * @param odata     Output data array. 
     * @param osize     Number of previously set bytes in @a odata.
     *                  On return, it contains the new number of valid
     *                  bytes in @a odata.
     * @return Number of bytes processed from @a payload or negative
     * error code in case of trouble. 
     *
     * @note This is executed in a separate thread 
     */
    virtual int Handle(unsigned char  cmd, 
		       unsigned short parameter,
		       byte_ptr    idata, size_t isize, 
		       byte_array& odata, size_t& osize);
    /** @} */
    
    /** @{ 
     * @name General Memory, Register, and Command access */
    /** 
     * Write a memory 
     *
     * @param m Memory descriptor 
     * @param idata Bytes to write to the memory. 
     * @param isize Number of bytes in @a idata
     * @param n     Number of words (of size @a wsize) to write 
     * @param wsize Word size 
     *
     * @return Bytes eaten from @a idata on success, or negative
     * error code on failure 
     *
     *
     * - -1       If passed data size (isize) would exceed memory size 
     * - -ENODATA If not enough data 
     * - -ENOLCK   Cannot get mutually exclusive lock 
     * - -EINVAL   Invalid data size 
     * - -1        (ETIMEDOUT) time out writing data 
     * - -EBADFD   Bad file descriptor 
     * - -EIO      Bad I/O 
     * - -EFAULT   Fault on buffers 
     */
    int Write(CodeBook::Memory& m, byte_ptr idata, size_t isize, 
	      size_t n, Dcsc::WordSize wsize=Dcsc::Word32);
    /** 
     * Read a memory 
     *
     * @param m Memory descriptor 
     * @param odata Byte array to copy to.
     * @param osize Number of valid bytes in @a odata. On return, the
     *              new number of valid bytes.
     * @param n     Number of words to read. 
     *
     * @return Bytes eaten from @a idata on success, or negative
     * error code on failure 
     *
     * - 0         Interface not open 
     * - -ENOLCK   Cannot get mutually exclusive lock 
     * - -EINVAL   Invalid data size 
     * - -1        (ETIMEDOUT) time out writing data 
     * - -EBADFD   Bad file descriptor 
     * - -EIO      Bad I/O 
     * - -EFAULT   Fault on buffers 
     * -  ESPIPE   Bad read 
     */
    int Read(CodeBook::Memory& m, byte_array& odata, size_t& osize, size_t n);
    /** 
     * Read a register
     * 
     * @param r     Register descriptor
     * @param value On return, the value of register
     * 
     * @return bytes read, or negative error code in case of failure
     *
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -ESPIPE    Failed to read result buffer 
     */
    int Read(CodeBook::Register& r, unsigned int& value);
    /**
     * Write to a register 
     * 
     * @param r     Register descriptor. 
     * @param idata Bytes to write to the register. 
     * @param isize Number of bytes in @a idata 
     *
     * @return Number of bytes eaten from @a idata, or negative error
     * code 
     *
     * - -1       If passed data size (isize) would exceed memory size 
     * - -ENODATA If not enough data 
     * - -ENOLCK   Cannot get mutually exclusive lock 
     * - -EINVAL   Invalid data size 
     * - -1        (ETIMEDOUT) time out writing data 
     * - -EBADFD   Bad file descriptor 
     * - -EIO      Bad I/O 
     * - -EFAULT   Fault on buffers 
     */
    int Write(CodeBook::Register& r, byte_ptr idata, size_t isize);
    /** 
     * Write to a register 
     *
     * @param r     Register descriptor. 
     * @param value Value to write.
     *
     * @return Number of bytes eaten from @a idata, or negative error
     * code 
     *
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Bad register
     */
    int Write(CodeBook::Register& r, unsigned int value);
    /** 
     * Read from a register 
     *
     * @param r     Register descriptor. 
     * @param odata Byte array to read value into.
     * @param osize Number of valid bytes in @a odata. On return the
     *              new number of valid bytes.
     *
     * @return Zero or positive on succes, or negative error code 
     *
     * - 0         Interface not open 
     * - -ENOLCK   Cannot get mutually exclusive lock 
     * - -EINVAL   Invalid data size 
     * - -1        (ETIMEDOUT) time out writing data 
     * - -EBADFD   Bad file descriptor 
     * - -EIO      Bad I/O 
     * - -EFAULT   Fault on buffers 
     * -  ESPIPE   Bad read 
     */
    int Read(CodeBook::Register& r, byte_array& odata, size_t& osize);
    /** 
     * Execute a command 
     *
     * @param c Command descriptor 
     * @param arg Arguments (if any) 
     *
     * @return positve or zero on success, negative error code
     * otherwise 
     * 
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */ 
    int Exec(CodeBook::Command& c, unsigned int arg=0);
    /** @} */

    /** 
     * @{ 
     * @name Specialised I/O on memories, registers, and commands 
     */
    /** 
     * Execute instruction memory 
     *
     * @param arg Offset in instruction memory 
     * 
     * @return 0 on success, error code otherwise 
     *
     * - -EBUSY      Interface busy 
     * - -ETIMEDOUT  FEC timeout 
     * - -EIO        Altro or Altro HW address error 
     * - -EBADF      Pattern memory mismatch 
     * - -1          (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK     Cannot get mutually exclusive lock on device
     * - -EBADFD     Bad file descriptor 
     * - -EIO        Bad input/output or bad status 
     * - -ESPIPE     Failed to read result buffer 
     */
    int ExecEXEC(unsigned int arg);
    /** 
     * Abort execution of instruction memory 
     *
     * @return 0 on success, error code otherwise
     *
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */
    int ExecABORT();
    /** 
     * Run a check on the Error and status register 
     *
     * @param ret Return byte array. 
     * @param osize Return size of @a ret 
     *
     * @return 0 on success, negative error code otherise 
     *
     * - -EBUSY      Interface busy 
     * - -ETIMEDOUT  FEC timeout 
     * - -EIO        Altro or Altro HW address error 
     * - -EBADF      Pattern memory mismatch 
     * - -1          (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK     Cannot get mutually exclusive lock on device
     * - -EBADFD     Bad file descriptor 
     * - -EIO        Bad input/output or bad status 
     * - -ESPIPE     Failed to read result buffer 
     */
    int ExecCheck(byte_array& ret, size_t& osize);
    /** 
     * Reset error and status register 
     * 
     * @return positve or zero on success, negative error code
     * otherwise 
     * 
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */
    int ResetERRST();
    /** 
     * Read the error and status register. 
     *
     * @param ret On return the contents of the ERRST register 
     * 
     * @param 0 or more on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     * - -ESPIPE    Bad read
     */
    int ReadERRST(unsigned int& ret);
    /** 
     * Run a check on the Error and status register 
     * 
     * @return 0 on success, negative error code otherise 
     *
     * - -EBUSY      Interface busy 
     * - -ETIMEDOUT  FEC timeout 
     * - -EIO        Altro or Altro HW address error 
     * - -EBADF      Pattern memory mismatch 
     * - -1          (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK     Cannot get mutually exclusive lock on device
     * - -EBADFD     Bad file descriptor 
     * - -EIO        Bad input/output or bad status 
     * - -ESPIPE     Failed to read result buffer 
     */
    int CheckERRST();
    /** 
     * Read the active front-end card list
     *
     * @param ret On return, contains the active front-end card
     * list. 
     *
     * @return 0 or more on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     * - -ESPIPE    Bad read
     */
    int ReadAFL(unsigned int& ret);
    /** 
     * Write the active front-end card list
     *
     * @param val Return value
     *
     * @param 0 or more on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */ 
    int WriteAFL(const unsigned int& val);
    /** 
     * Make the DCS card the altro bus master 
     *
     * @return 0 on success, error code otherwise
     * 
     * - -EIO    No access to RCU 
     */
    int DcsMaster();
    /** 
     * Make the SIU card the altro bus master 
     * 
     * @return 0 on success, error code otherwise
     * 
     * - -EIO    No access to RCU 
     */
    int DdlMaster();
    /** @} */

    /** 
     * @{ 
     * @name @f$ I^2C@f$ interface 
     */
    /** 
     * Can we switch lines ? 
     */ 
    bool CanSwitch() const;
    /** 
     * Switch lines (? - needed for 8bit registers?) 
     * 
     * @return positive or 0 on success, negative error code otherise 
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */
    int SwitchLines(bool on=true);
    /** 
     * Reset the @f$ I^2C@f$ error register  
     * 
     * @return positive or 0 on success, negative error code otherise 
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */
    int ResetERRREG();
    /** 
     * Read the @f$ I^2C@f$ error register 
     * 
     * @param not_active Is true on return if the addressed front-end
     * card is not on. 
     * @param no_ack Is true on return if the addressed front-end
     * card is not responding.
     *
     * @return 0 or more on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     * - -ESPIPE    Bad read
     */
    int ReadERRREG(bool& not_active, bool& no_ack);
    /** 
     * Read the @f$ I^2C@f$ return register 
     *
     * @param ret On return, contains the data returned by the
     * front-end card. 
     * @param switched I2C registers are switched. 
     *
     * @return 0 or more on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     * - -ESPIPE    Bad read
     */
    int ReadRESREG(unsigned int& ret, bool switched=false);
    /** 
     * Reset the result register 
     *
     * @return positive or 0 on success, negative error code otherise 
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     */
    int ResetRESREG();
    /** 
     * Write to slow control address register 
     *
     * @param data Data to write 
     * @param switched I2C registers are switched. 
     *
     * @return positive or 0 on success, negative error code otherise 
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     */
    int WriteSCADD(unsigned int data, bool switched=false);
    /** 
     * Write to slow control data register 
     *
     * @param data Data to write 
     * @param switched I2C registers are switched. 
     *
     * @return positive or 0 on success, negative error code otherise 
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     */
    int WriteSCDAT(unsigned int data, bool switched=false);
    /** 
     * Execute slow control commands written to SCADD and SCDAT 
     *
     * @return positive or 0 on success, negative error code otherise 
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     */
    int ExecSC();
    /** 
     * Execute FEC read/write via  @f$ I^2C@f$
     *
     * @param rnw   Read-not-write
     * @param bcast Broadcast?
     * @param fec   Front-end card 
     * @param reg   Register 
     * @param val   Value to write (only for 5bit access and @a rnw false)
     * @param switched I2C registers are switched. 
     *
     * @return 0 or more on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     */
    int ExecViaI2C(bool rnw, bool bcast, unsigned char fec, unsigned char reg,
      	   unsigned int val=0, bool switched=false);
    /** 
     * Read a FEC register via  @f$ I^2C@f$
     * @param fec Front-end card 
     * @param reg Register 
     * @param ret Return value
     *
     * @return 0 on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     * - -ESPIPE    Bad read
     * - -EBADF     Mismatch in return address or not active 
     * - -EAGAIN    No acknowledge 
     */
    int ReadViaI2C(unsigned char fec, unsigned char reg, unsigned short& ret);
    /** 
     * Wrote a FEC register via  @f$ I^2C@f$
     * @param fec Front-end card 
     * @param reg Register 
     * @param ret Value to write 
     *
     * @return 0 on success, error code otherwise
     *
     * - >0         Number of word in result buffer 
     * - 0          Interface not open 
     * - -1         (ETIMEDOUT)  Time-out on write 
     * - -ENOLCK    Cannot get mutually exclusive lock on device
     * - -EBADFD    Bad file descriptor 
     * - -EIO       Bad input/output or bad status 
     * - -EINVAL    Invalid register
     * - -ESPIPE    Bad read
     * - -EBADF     FEC not active 
     * - -EAGAIN    FEC does not acknowledge 
     */
    int WriteViaI2C(unsigned char fec, unsigned char reg, unsigned short ret);
    /** @} */


    /** 
     * @{ 
     * @name Fec interface via ALTRO bus 
     */ 
    /** 
     * 
     * 
     * @param fec 
     * @param chip 
     * @param channel 
     * @param reg 
     * @param ret 
     * 
     * @return 
     */
    int ReadViaBus(unsigned char   fec, 
		   unsigned char   chip, 
		   unsigned char   channel, 
		   unsigned char   reg, 
		   unsigned short& ret);
    /** 
     * 
     * 
     * @param fec 
     * @param chip 
     * @param channel 
     * @param reg 
     * @param val 
     * 
     * @return 
     */
    int WriteViaBus(unsigned char  fec, 
		    unsigned char  chip, 
		    unsigned char  channel, 
		    unsigned char  reg, 
		    unsigned short val);
    /** @} */

    /** 
     * @{ 
     * @name Utility functions for memory/register I/O 
     */
    /** 
     * Read a block of memory 
     * @param address Address to start reading at. 
     * @param m_size  Size of memory partition. 
     * @param odata   Array to read into.  On entry, this contains @a
     *                osize valid bytes of data.  On exit, the read
     * 	              data is appended, and @a osize is updated
     * 	              accordingly.  The member function resizes @a
     * 	              odata as needed. 
     * @param osize   On entry, valid number of bytes in @a odata. On
     *                exit new valid number of bytes in @a odata. 
     * @param n       Number of 32bit words to read from the memory.
     * @return 0 on success, negative error code otherise 
     */
    int ReadBlock(unsigned int address, size_t m_size, 
		  byte_array& odata, size_t& osize, size_t n);
    /** 
     * Write to an RCU memory location 
     * @param address Address to write to. 
     * @param idata   The data to write. 
     * @param n       Number of words to write to memory.  Size of
     *                the words is given by the @a w_size parameter. 
     * @param w_size  Size of words. 		       
     * @return Number of bytes written on success, negative error
     *      	code otherise 
     *
     * - 0         Invalid word size 
     * - -ENOLCK   Cannot get mutually exclusive lock 
     * - -EINVAL   Invalid data size 
     * - -1        (ETIMEDOUT) time out writing data 
     * - -EBADFD   Bad file descriptor 
     * - -EIO      Bad I/O 
     * - -EFAULT   Fault on buffers 
     */ 
    int WriteCommandBuffer(unsigned int address, const unsigned char* idata, 
			   size_t n, Dcsc::WordSize w_size);
    /** 
     * Write a block of data to the RCU memory. 
     *
     * @param address Address to write to. 
     * @param m_size  Size of the memory partition. 
     * @param idata   The data to write 
     * @param isize   Total size of @a idata 
     * @param n       Number of words to write to memory.  Size of
     *                the words is given by the @a w_size parameter. 
     * @param w_size  Size of words. 		       
     *
     * @return Number of bytes written on success, negative error
     *      	code otherise 
     * 
     * - -1       If passed data size (isize) would exceed memory size 
     * - -ENODATA If not enough data 
     * - -ENOLCK   Cannot get mutually exclusive lock 
     * - -EINVAL   Invalid data size 
     * - -1        (ETIMEDOUT) time out writing data 
     * - -EBADFD   Bad file descriptor 
     * - -EIO      Bad I/O 
     * - -EFAULT   Fault on buffers 
     * 
     * Note, if address == CodeBook::_monitor_on or
     * CodeBook::_monitor_off, then nothing is really written to the
     * hardware - and everything succeeds.
     * 
     * If address == CodeBook::_afl._address and flick is enabled,
     * then it is done via a read, then write 0x0, and then write
     * again.
     */ 
    int WriteBlock(unsigned int address, size_t m_size, 
		   const unsigned char* idata, size_t isize, size_t n, 
		   Dcsc::WordSize w_size);
    /** @} */

    /** 
     * Get the current state 
     *
     * @return The state 
     */
    void Transition(const State&  src, 
		    const Action& action, 
		    const State&  dest);
    /** Get the state machine state service */ 
    const StateService* GetStateService() const { return &_state_service; }

    /** 
     * Function to find the firmware version of the RCU. 
     *
     * @param autodetect  IF true, try to automatically detect the
     *                    RCU version by probing. 
     * @return 0 on success, negative error code otherise 
     */
    int FindFwVersion(bool autodetect);

    /** 
     * Toggle wether to enable auto-flicker of CARDSW lines 
     */
    void EnableAFLFlicker(bool enable=true) { _enable_flick = enable; }
  protected:
    /** Not implemented */ 
    Rcu(const Rcu&);
    /** Not implemented */ 
    Rcu& operator=(const Rcu&);

    /** 
     * Update the state of the RCU state machine, based on the values
     * of certain registers.
     */
    virtual int UpdateState();
    /** 
     * Update the basic services. 
     *
     * @return 0 on success, error code otherwise 
     */ 
    virtual int UpdateBasic();
    /** 
     * Simple check on ERRST 
     *
     * @param bnota if true, then it's for branch B 
     */ 
    int SimpleCheckERRST(bool bnota);
    void Reset();
    
    /** Low-level interface */
    Dcsc& _dcsc;
    /** Firmware version */
    unsigned int _fw_version;
    /** Auto check */
    bool _auto_check;
    /** The fec factory */ 
    FecFactory* _factory;

    /** 
     * @{ 
     * @name Services 
     */
    StateService    _state_service;
    ERRSTService*   _errst_service;
    AFLService*     _afl_service;
    Service*        _status_service;
    HITLISTService* _hitlist_service;
    TRIGService*    _trig_service;
    RUNService*     _run_service;
    TEMPService*    _dcs_temp_service;
    TEMPService*    _pr_temp_service;
    /** @} */

    /** 
     * Wether to flicker (explicit configure of BCs) CARDSW lines
     * when turnining on new cards in AFL 
     */  
    bool _enable_flick;

    enum {
      master_unknown, 
      master_ddl, 
      master_dcs 
    } _current_master;
  };

  /** 
   * @defgroup rcuce_rcu_serv Special RCU services 
   *
   * This is some classes for particular RCU services.  
   * @ingroup rcuce_rcu
   *
   * @ingroup rcuce_serv 
   */
  //____________________________________________________________________
  /** 
   * @struct RcuService 
   *
   * @brief Just ensure that we have a reference to an RCU 
   *
   * @ingroup rcuce_rcu_serv 
   */
  struct RcuService : public Service
  {
    /** constructor 
      @param rcu reference to Rcu object */
    RcuService(RcuCE::Rcu& rcu) : _rcu(rcu) {}
    /** Destructor */
    virtual ~RcuService() {}
    static void LogService(const char* format, ...);
    static void SetLogFile(std::ostream* lf) { _log_file = lf; }
  protected:
    /* Reference to Rcu object */
    RcuCE::Rcu& _rcu;
    /** Output log file */
    static std::ostream* _log_file;
  };
}

#endif

