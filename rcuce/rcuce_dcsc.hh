#ifndef RCUCE_DCSC_HH
#define RCUCE_DCSC_HH
#include <feeserverxx/fee_lock.hh>
#include <rcuce/rcuce_handler.hh>
#include <string>
#include <vector>

namespace RcuCE
{
  /** @defgroup rcuce_dcsc DCS Card interface classes 
      @ingroup rcuce_core 
   */

  /** @class Dcsc rcuce/rcuce_dcsc.hh <rcuce/rcuce_dcsc.hh>
      @brief Interface to DCS Card message buffer library. 
      This class is defined to allow locking of the interface to
      prevent bad concurrent access to the lower level interface. 
      @ingroup rcuce_dcsc
  */
  class Dcsc : public CommandHandler
  {
  public:
    /** 
     * Error codes.  Member functions that return negative error code
     * can return the negative value of any of these codes. 
     */
    enum { 
      SUCCESS,     //! All is good 
      UNKNOWN,     //! Unknown error 
      NOT_OPEN,    //! Interface not open 
      CANNOT_LOCK, //! Failed to aquire MuTeX
      TIMEOUT,     //! Timeout while waiting for read signal
      WRONG_SIZE,  //! Sizes don't match 
      NO_MARKER,   //! No marker 
      NO_TARGET,   //! No target 
      BUS_BLOCKED, //! Cannot get bus 
      NO_END,      //! No end marker 
      BAD_PARAMETER, //! Bad parameter passed
    };
      

    /** A 32bit unsigned integer */
    typedef unsigned int u32_t;

    /** Constructor 
	@param file Device file to open 
	@param verbose Be verbose */
    Dcsc(const std::string& file="", bool verbose=false);
    /** Destructor */
    virtual ~Dcsc();
    
    /** 
     * @{ 
     * @name Low-level interface 
     */
    /** 
     * Whether the interface is open 
     *
     * @return true if the device was opened properly 
     */
    bool IsOpen() const { return _is_open; }

    /** 
     * Write a single location (32bit word)
     *
     * @param addr      16 bit address in RCU memory space
     * @param data      data word
     *
     * @return >=0 on success, negative error code otherwise 
     */
    virtual int Write(u32_t addr, u32_t data);
    /**
     * Read a single location (32bit word)
     * @param addr      16 bit address in RCU memory space
     * @param data      buffer to receive the data
     *
     * @return >=0 on success, negative error code otherwise 
     */
    virtual int Read(u32_t addr, u32_t& data);

    /** Fast write - no checks on open! */ 
    virtual int FastWrite(u32_t addr, u32_t data);
    /** 
     * @} 
     */

    /** Word sizes */
    enum WordSize {
      /** 8 bits */
      Word8  = 1, 
      /** 16 bits */
      Word16 = 2, 
      /** 10 bits (compressed) */
      Word10 = 3,
      /** 32 bits */
      Word32 = 4, 
      /** Swapped 16 bits */
      Swapped16 = 5, 
      /** Swapped 32 bits */
      Swapped32 = 6 
    };

    /** 
     * @{ 
     * @name Block read/write interface 
     */
    /** 
     * Write a number of 32bit words beginning at a location. 
     *
     * The function takes care for the size of the MIB and splits the 
     * operation if the amount of data to write exceeds the MIB size.  
     * The function expects data in little endian byte order
     *
     * @param address   16 bit address in RCU memory space
     * @param data      buffer containing the data
     * @param size      The size of data (in 32bit words)
     * @param wsize     Size of one word in bytes, given by enum.
     *
     * @return Number of words (>= 0, in the size specifed by @a wsize)
     * written on success, negative error code otherwise 
     */
    virtual int Write(u32_t address, const unsigned int* data, size_t size, 
		      WordSize wsize);

    /**
     * Read a number of 32bit words beginning at a location.
     * @param address   16 bit address in RCU memory space
     * @param data      buffer to receive the data, the function will
     *                  expects the it to be large enough to hold @a
     *                  size 32 bit words. 
     * @param size      number of words to write
     *
     * @return          number of 32bit words which have been read,
     *                  neg. error code if failed 
     */
    virtual int Read(u32_t address, unsigned int* data, size_t size);


    /** Check if this handler can handle the command @a cmd with
	parameters @a param. 
	@param group The command group
	@param cmd   The command that we should handle
	@return true if this object can handle the command. */
    virtual bool CanHandle(unsigned char group, unsigned char cmd) const;
    /** Handle a command 
	@see DcscGroup
	@param cmd      Command. 
	@param param    Parameter to command @a cmd.
	@param payload  The payload of the command. 
	@param isize    Maximum number of bytes in payload. 
	@param odata    Output data array. 
	@param osize    Number of previously set bytes in @a odata.
	                On return, it contains the new number of valid
			bytes in @a odata.
	@return Number of bytes processed from @a payload or negative
	error code in case of trouble. */
    virtual int Handle(unsigned char cmd,    unsigned short param, 
		       byte_ptr    payload,  size_t  isize, 
		       byte_array& odata,    size_t& osize);
  protected:
    FeeServer::Mutex::Attributes _attr;
    /** Lock */ 
    FeeServer::Mutex _mutex;
    /** Whether we could open the interface */
    bool _is_open;
    /** Use single word I/O */
    bool _use_single;
  };

  //__________________________________________________________________
  /** @class DcscDummy rcuce/rcuce_dcsc.hh <rcuce/rcuce_dcsc.hh>
      @brief Dummy interface to DCS Card message buffer library. 
      This class just reads and writes in memory.  Usefull for
      simulations.
      @ingroup rcuce_dcsc
  */
  struct DcscDummy : public Dcsc 
  {
    DcscDummy(u32_t version=60504, const std::string& dump=std::string(), 
	      unsigned short msleep=1);
    ~DcscDummy();
    void CleanUp();
    int Write(u32_t address, u32_t data);
    int FastWrite(u32_t address, u32_t data) { return Write(address, data); }
    int Read(u32_t addresss, u32_t& data);
    int Write(u32_t address, const unsigned int* data, size_t n, WordSize ws);
    int Read(u32_t address, unsigned int* data, size_t n); 
    void SetRmemFromImem();
    void SetResultFromCmd();
    std::vector<u32_t> _mem;
    u32_t              _version;
    u32_t              _rev_version;
    unsigned short     _sleep;
    std::string        _dump;
  };
}

#endif
//
// EOF
//
