#ifndef RCUCE_FSM_HH
#define RCUCE_FSM_HH
#include <vector>
#include <map>
#include <iosfwd>
#include <feeserverxx/fee_lock.hh>

namespace RcuCE
{
  //__________________________________________________________________
  /** @defgroup rcuce_fsm Finite State Machine classes 

      An example of a statemachine specification is shown below (@ref
      test_fsm.cc) 

      First, we define two enumerations for the states and actions. 
      @dontinclude test_fsm.cc
      @skip struct Fsm
      @until //EndActions
      
      Then in the constructor we set up the states and allowed
      transitions. 
      @until //EndCTor 

      And finally, we implement (simple) handlers for state
      transitions and illegal actions 
      @until //EndFsm

      We can define a custom StateMachinePlotter class to output a
      nice @b Dot graph.  

      @skip struct FsmPlotter 
      @until FsmPlotter(
      
      All we need to do, is to define the names of the states and
      action as C strings.

      @until }//EndActionName

      If we want to, we can also attach comments to the states.

      @until }//EndStateComment

      If we want we can add stuff at the end of the graph - for
      example for transitions that are not specified out in the state
      machine description. 
      
      @until };

      Finally, we can make an object of our class, do some stuff on
      it, and plot the @c Dot graph to standard out. 
      
      @until }
      
      If we run the program like 
      @verbatim 
      test_fsm | dot -Tpng > test_fsm.png 
      @endverbatim 

      we get the following nice graphics that shows us the state
      machine 
      
      @image html test_fsm.png "Test FSM"

      @ingroup rcuce_core */

  //__________________________________________________________________
  /** @brief Base class for state machines. 
      @ingroup rcuce_fsm
  */
  class StateMachine 
  {
  public:
    /** State type */
    typedef unsigned short State;
    /** Action type */
    typedef unsigned short Action;
    /** List of states */ 
    typedef std::vector<State> StateVector;
    /** A map of transitions */
    typedef std::map<size_t,size_t> TransitionMap;
    /** Map of action to transitions */
    typedef std::map<Action,TransitionMap> ActionMap;

    /** Constructor */ 
    StateMachine() 
      : _states(), 
	_map(),
	_current(),
	_mutex()
    {}
    /** Destructor */ 
    virtual ~StateMachine() {}
	
    /** A a state to the state machine. 
	@param state The state identifier to add. 
	@return @c true if the state was added correctly */ 
    bool AddState(const State& state);
    /** Add a transition to the state machine. 
	@param action The action 
	@param src    The source state 
	@param dest   The destination state 
	@return @c true if the transition was added correctly */
    bool AddTransition(const State& src, 
		       const Action& action, 

		       const State& dest);
    /** Trigger a transition by sending an action.  If the transition
	was successful, then the member function Transition is called
	with the source state, the action, and the destination state
	as arguments.  If the action was illegal in the current state,
	then the member function IllegalAction is called with the
	action and source state as the parameters. 
	@param action Action to send. 
	@return The new state (if any) */
    const State& Trigger(const Action& action);
    
    /** Get the current state 
	@return The current state */
    const State& GetState() const;
    /** Set the current state 
	@param state The new state. 
	@return @c true if the state is legal, otherwise false */ 
    bool SetState(const State& state);
    /** Get the list of states 
	@return The lists of states defined */ 
    const StateVector& States() const { return _states; }
    /** Find all actions available in state @a s 
	@param s State to find transitions for 
	@param a On return, a list of actions  */
    void Actions(const State& s, std::vector<Action>& a) const;
    /** Find all actions available in the current state
	@param a On return, a list of actions  */
    void Actions(std::vector<Action>& a) const;
    /** Find all transitions available in state @a s 
	@param s State to find transitions for 
	@param t On return, a map of actions to states */
    void Transitions(const State& s, std::map<Action,State>& t) const;
    /** Find all transitions available in the current state
	@param t On return, a map of actions to states */
    void Transitions(std::map<Action,State>& t) const;
    
    /** Virtual member function to call on an illegal action.  The
	user should override this member function to take care of bad
	actions - for example go to an error state.  
	@param action The action that was tried. 
	@param src    The source state */
    virtual void IllegalAction(const Action& action, const State& src);
    /** Virtual member function to call when a transition is made. 
	@param src    The source state 
	@param action The action that prompted the transition 
	@param dest   The destination state  */
    virtual void Transition(const State&  src, 
			    const Action& action, 
			    const State&  dest);
  protected:
    /** Utility function to find the state index 
	@param state State to search for 
	@return The state index or -1 if the state wasn't found */ 
    int FindState(const State& state) const;
    /** Member function that derived class can overload to do
	something on entry into a state 
	@param s state entered */ 
    virtual void DoSetState(const State& s);
    /** The available states */ 
    StateVector _states;
    /** The available transitions */ 
    ActionMap  _map;
    /** The current states index */ 
    size_t _current;
    /** Mutex to ensure consistency */
    FeeServer::Mutex _mutex;
  };
  //
  inline void 
  StateMachine::IllegalAction(const Action&, const State&) 
  {}
  inline void 
  StateMachine::DoSetState(const State&) 
  {}
  inline void 
  StateMachine::Transition(const State&, 
			   const Action&, 
			   const State&) 
  {}

  //__________________________________________________________________
  /** @class StateMachinePlotter 
      @brief Plot a StateMachine as a @b Dot diagram 
      @ingroup rcuce_fsm
  */
  class StateMachinePlotter 
  {
  public:
    /** State type */
    typedef StateMachine::State State;
    /** Action type */
    typedef StateMachine::Action Action;
    /** List of states */ 
    typedef StateMachine::StateVector StateVector;
    /** A map of transitions */
    typedef StateMachine::TransitionMap TransitionMap;
    /** Map of action to transitions */
    typedef StateMachine::ActionMap ActionMap;

    /** Constructor 
	@param sm State machine to make dot graph for */ 
    StateMachinePlotter(const StateMachine& sm) 
      : _state_machine(sm)
    {}
    /** Destructor */ 
    virtual ~StateMachinePlotter() {}
    /** Get the name of a state 
	@param state State to get the name of */ 
    virtual const char* StateName(const State& state) const;
    /** Get the name of an action 
	@param action Action to get the name for */ 
    virtual const char* ActionName(const Action& action) const;
    /** Get the comment of a state 
	@param state State to get comment for */ 
    virtual const char* StateComment(const State& state) const;
    /** Extra stuff to put in to the graph at the end */
    virtual void AtStart(std::ostream&) const {}
    /** Extra stuff to put in to the graph at the end */
    virtual void AtEnd(std::ostream&) const {}
  protected:
    /** Plot the dot graph 
	@param o output stream 
	@return @a o */
    virtual std::ostream& Print(std::ostream& o) const;
    /** Output operator is a friend */ 
    friend std::ostream& operator<<(std::ostream&,const StateMachinePlotter&);
    /** The state machine we plot. */ 
    const StateMachine& _state_machine;
  };

  /** Output operator for a statemachine plotter 
      @ingroup rcuce_fsm */
  std::ostream& operator<<(std::ostream& o, const StateMachinePlotter& p);
}

#endif
//
// EOF
//

    
