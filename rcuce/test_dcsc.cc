#include <rcuce/rcuce_dcsc.hh>
#include <feeserverxx/fee_dbg.hh>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>

unsigned int 
read_val(const char* prompt)
{
  std::cout << prompt << std::flush;
  std::string str;
  std::cin >> str;
  std::stringstream s(str);
  unsigned int v;
  if (str[0] == '0') {
    if (str[1] == 'x' || str[1] == 'X') s >> std::hex >> v;
    else                                s >> std::oct >> v;
  }
  else 
    s >> v;
  return v;
}

int
main(int argc, char** argv)
{
  bool emul     = false;
  // bool debug    = false;
  bool verbose  = false;
  bool trace    = false;
  // bool info     = false;
  bool multi    = false;
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': 
	std::cout << "Usage: " << argv[0] << " OPTIONS\n\n" 
		  << "Options:\n" 
		  << "\t-h\t\tThis help\n" 
		  << "\t-e\t\tEmulation of DCSC\n"
		  << "\t-d\t\tEnable debug output\n"
		  << std::endl;
	return 0;
      case 'e': emul     = true; break;
	// case 'd': debug    = true; break;
      case 'v': verbose  = true; break;
      case 't': trace    = true; break;
	// case 'i': info     = true; break;
      case 'm': multi    = true; break;
      default: 
	std::cerr << argv[0] << ": unknown option " << argv[i] << std::endl;
	return 1;
      }
    }
  }
  DTRACE(trace);
  

  RcuCE::Dcsc* dcsc = 0;
  if (emul) dcsc = new RcuCE::DcscDummy;
  else      dcsc = new RcuCE::Dcsc("",verbose);

  enum { 
    kSingleRead = 1, 
    kSingleWrite, 
    kMultiRead, 
    kMultiWrite, 
    kQuit
  };
  typedef std::vector<unsigned int> int_array;
  while (true) { 
    unsigned int answer  = kQuit;
    unsigned int address = 0;
    unsigned int size    = 0;
    unsigned int value   = 0; 
    int_array    array(1);

    std::cout << "Please choose your operation:\n" 
              << "\tSingle read:  " << kSingleRead  << "\n" 
              << "\tSingle write: " << kSingleWrite << "\n" 
              << "\tMulti read:   " << kMultiRead   << "\n" 
              << "\tMulti write:  " << kMultiWrite  << "\n" 
              << "\tQuit:         " << kQuit        << "\n";
    answer  = read_val("Your choice:             ");
    if (answer == kQuit) break;
    if (answer > kQuit) { 
      std::cerr << "Invalid choice, try again\n\n" << std::endl;
      continue;
    }

    address = read_val("Please input an address: ");

    if (answer == kMultiRead || answer == kMultiWrite) 
      size = read_val("Please input a size:     ");
    else 
      size = 1;

    if (answer == kSingleWrite || answer == kMultiWrite) { 
      if (array.size() < size) array.resize(size);
      for (size_t i = 0; i < size; i++) { 
	value = read_val("Input value: ");
	array[i] = value;
      }
    }
    int ret = 0;
    switch (answer) { 
    case kSingleRead: 
      if (!multi) {
	ret = dcsc->Read(address, array[0]);
	value = array[0];
	break;
      }
      // Otherwise, fall through
    case kMultiRead: 
      ret = dcsc->Read(address, &(array[0]), size);
      break;
    case kSingleWrite: 
      if (!multi) { 
	ret = dcsc->Write(address, array[0]);
	break;
      }
      break;
    case kMultiWrite:
      ret = dcsc->Write(address, &(array[0]), size, RcuCE::Dcsc::Word32);
      break;
    }
    std::cout << "Return value is " << ret << std::endl;
    if (answer == kSingleRead || answer == kMultiRead) { 
      std::cout << "Result is" << std::endl;
      for (size_t i = 0; i < size; i++) { 
	std::cout << "\t" << i+1 << "/" << size << ":\t"
		  << std::hex << "0x" << array[i] << std::dec << std::endl;
      }
    }
    std::cout << "\n" << std::endl;
  }
    



  return 0;
}
//
// EOF
//
