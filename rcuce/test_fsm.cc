#include <rcuce/rcuce_fsm.hh>
#include <iostream>
#include <cassert>
#include <feeserverxx/fee_dbg.hh>
#include <unistd.h>

struct Fsm : public RcuCE::StateMachine 
{
  enum States { off, standby, downloading, configured, running, error };
  enum Actions { turn_on, configure, run, stop, turn_off };//EndActions
  Fsm() {
    AddState(off);
    AddState(standby);
    AddState(downloading);
    AddState(configured);
    AddState(running);
    AddState(error);
    AddTransition(turn_on,   off,        standby);
    AddTransition(configure, standby,    downloading);
    AddTransition(configure, configured, downloading);
    AddTransition(configure, running,    downloading);
    AddTransition(run,       configured, running);
    AddTransition(stop,      running,    configured);
    AddTransition(turn_off,  configured, off);
    SetState(off);
  }//EndCTor
  void Transition(const State&, const Action&, const State& dest)
  {
    if (dest == downloading) {
      usleep(1000);
      SetState(configured);
    }
  }
  void IllegalAction(const Action&, const State&) 
  {
    SetState(error);
  }
};//EndFsm
struct FsmPlotter : public RcuCE::StateMachinePlotter
{
  FsmPlotter(const Fsm& fsm) : RcuCE::StateMachinePlotter(fsm) {}
  const char* StateName(const State& state) const {
    switch (state) {
    case Fsm::off:		return "off";		break;
    case Fsm::standby:		return "standby";	break;
    case Fsm::downloading:	return "downloading";	break;
    case Fsm::configured:	return "configured";	break;
    case Fsm::running:		return "running";	break;
    case Fsm::error:		return "error";		break;
    }
    return RcuCE::StateMachinePlotter::StateName(state);
  }
  const char* ActionName(const Action& action) const {
    switch (action) {
    case Fsm::turn_on:		return "turn_on";	break;
    case Fsm::configure:	return "configure";	break;
    case Fsm::run:		return "run";		break;
    case Fsm::stop:		return "stop";		break;
    case Fsm::turn_off:		return "turn_off";	break;
    }
    return RcuCE::StateMachinePlotter::ActionName(action);
  }//EndActionName
  const char* StateComment(const State& state) const {
    switch (state) {
    case Fsm::off:		return "Starting\\nstate";		break;
    case Fsm::error:		return "Reachable from\\nall states";	break;
    }
    return 0;
  }//EndStateComment
  void AtEnd(std::ostream& o) const
  {
    o << "  s" << Fsm::downloading << " -> s" << Fsm::configured 
      << " [label=\"automatic\",style=dotted]\n";
  }
};

int main()
{
  Fsm fsm;
  fsm.Trigger(Fsm::turn_on);    assert(fsm.GetState() == Fsm::standby);
  fsm.Trigger(Fsm::configure);  assert(fsm.GetState() == Fsm::configured);
  fsm.Trigger(Fsm::run);        assert(fsm.GetState() == Fsm::running);
  fsm.Trigger(Fsm::configure);  assert(fsm.GetState() == Fsm::configured);
  fsm.Trigger(Fsm::run);        assert(fsm.GetState() == Fsm::running);
  fsm.Trigger(Fsm::stop);       assert(fsm.GetState() == Fsm::configured);
  fsm.Trigger(Fsm::turn_off);   assert(fsm.GetState() == Fsm::off);
  fsm.Trigger(Fsm::configure);  assert(fsm.GetState() == Fsm::error);
  std::cout << FsmPlotter(fsm) << std::endl;
  return 0;
}  
