#include <rcuce/rcuce_bfsm.hh>
#include <feeserverxx/fee_dbg.hh>
#include <iostream>
#include <iomanip>
#include <cassert>

using RcuCE::BasicFsm;
using RcuCE::BasicFsmParent;
typedef BasicFsmParent::FsmList FsmList;
typedef std::map<BasicFsm::Action,BasicFsm::State> TransMap;

//____________________________________________________________________
void ShowState(BasicFsm& b)
{
  std::cout << "Current state of " << b.Id() << " is " 
	    << std::setw(3) << b.GetState() << ": " 
	    << BasicFsm::State2String(b.GetState()) << std::endl;
}
//____________________________________________________________________
void ShowStates(BasicFsmParent& p)
{
  ShowState(p);
  const FsmList& l = p.Fsms();
  for (FsmList::const_iterator i = l.begin(); i != l.end(); ++i) {
    std::cout << "  ";
    ShowState(*(*i));
  }
}
//____________________________________________________________________
BasicFsm& ChooseFsm(BasicFsmParent& p)
{
  std::cout << "Available FSMs:\n" 
	    << "\t" << std::setw(3) << p.Id() << " [Parent]" << std::endl;
  FsmList& l = p.Fsms();
  for (FsmList::iterator i = l.begin(); i != l.end(); ++i) 
    std::cout << "\t  " << std::setw(3) << (*i)->Id() 
	      << " [Child]" << std::endl;
  std::cout << "Please choose the FSM: " << std::flush;
  unsigned int choice = 0;
  std::cin >> choice;
  if (choice == p.Id()) return p;
  for (FsmList::iterator i = l.begin(); i != l.end(); ++i) 
    if (choice == (*i)->Id()) return *(*i);
  return p;
}
//____________________________________________________________________
bool ChooseAction(BasicFsm& b)
{
  TransMap ts;
  b.Transitions(ts);
  int j = 1;
  std::cout << std::setw(3) << 0 << ": Quit" << std::endl;
  for (TransMap::const_iterator i = ts.begin(); i != ts.end(); ++i, ++j)
    std::cout << std::right << std::setw(3)  << i->first  << ": "
	      << std::left  << std::setw(30) 
	      << BasicFsm::Action2String(i->first) << " ["
	      << std::right << std::setw(3)  << i->second << ": " 
	      << BasicFsm::State2String(i->second) << "]" 
	      << std::endl;
  std::cout << "Please choose an action number: " << std::flush;
  int choice;
  std::cin >> choice;
  if (choice == 0) return false;
  TransMap::const_iterator t = ts.find(choice);
  if (t == ts.end()) { 
    std::cerr << "WARNING: Invalid transition # " << choice << "\n" 
	      << std::endl;
    // continue;
  }
  b.Trigger(choice);
  std::cout << std::endl;
  // BasicFsm::State target = t->second;
  // assert(target == fsm.GetState());
  return true;
}

  
//____________________________________________________________________
int
main()
{
  using RcuCE::BasicFsm;
  // using RcuCE::CommonActions;
  BasicFsm       c1(1);
  BasicFsm       c2(2);
  BasicFsmParent p1(0);
  p1.AddFsm(c1);
  p1.AddFsm(c2);
  
  // typedef  std::map<BasicFsm::Action,BasicFsm::State> TransMap;
  do { 
    ShowStates(p1);
    BasicFsm& b = ChooseFsm(p1);
    if (!ChooseAction(b)) break;
    // p1.UpdateState();
  } while(true);
  return 0;
}

  
//____________________________________________________________________
//
// EOF
//



  
