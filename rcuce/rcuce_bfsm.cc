#include <rcuce/rcuce_bfsm.hh>
#include <algorithm>
namespace 
{
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}

//====================================================================
RcuCE::BasicFsm::BasicFsm(unsigned char id, 
			  const State& initial) 
  : _id(id), 
    _parent(0)
{
  using namespace CommonStates;
  using namespace CommonActions;
  
  AddState(Idle);
  AddState(Standby);
  AddState(Downloading);
  AddState(Ready);
  AddState(Error);
  if (initial != Idle        || 
      initial != Standby     || 
      initial != Downloading || 
      initial != Ready       || 
      initial != Error) AddState(initial);
  SetState(initial);
  
  AddTransition(Error,       GoStandby,     Standby);
  AddTransition(Idle, 	     GoStandby,     Standby);
  AddTransition(Idle, 	     GoIdle,        Idle);
  AddTransition(Standby,     GoIdle,        Idle);
  AddTransition(Standby,     GoStandby,     Standby);
  AddTransition(Standby,     Configure,     Downloading);
  AddTransition(Downloading, GoStandby,     Standby);
  AddTransition(Downloading, ConfigureDone, Ready);
  AddTransition(Ready, 	     GoStandby,     Standby); 
  AddTransition(Ready,       Configure,     Downloading); 
}
//____________________________________________________________________
RcuCE::BasicFsm::~BasicFsm()
{
  if (_parent) _parent->RemoveFsm(*this);
}

//____________________________________________________________________
bool
RcuCE::BasicFsm::TryHandle(const Action&, 
			   unsigned char id, 
			   unsigned short, 
			   unsigned short) const
{ 
  return _id == id;
}
//____________________________________________________________________
void
RcuCE::BasicFsm::DoSetState(const State&)
{
  if (_parent) _parent->UpdateState();
}

//____________________________________________________________________
const RcuCE::BasicFsm::Action& 
RcuCE::BasicFsm::String2Action(const std::string& action)
{
  static Action fa = 0;
  using namespace CommonActions;
  std::string a(action);
  std::transform(a.begin(),a.end(),a.begin(),to_lower());
  if      (a == "go_idle")         fa = GoIdle;
  else if (a == "go_standby")      fa = GoStandby;
  else if (a == "configure")       fa = Configure;
  else if (a == "configure_done")  fa = ConfigureDone;
  return fa;
}
//____________________________________________________________________
std::string 
RcuCE::BasicFsm::Action2String(const Action& action)
{
  using namespace CommonActions;
  std::string a;      
  switch (action) {
  case GoIdle:		a = "GO_IDLE";		break;
  case GoStandby:	a = "GO_STANDBY";	break;
  case Configure:	a = "CONFIGURE";	break;
  case ConfigureDone:	a = "CONFIGURE_DONE";	break;
  }
  return a;
}
//____________________________________________________________________
const RcuCE::BasicFsm::State& 
RcuCE::BasicFsm::String2State(const std::string& state)
{
  using namespace CommonStates;
  static State fs = 0;
  std::string s(state);
  std::transform(s.begin(),s.end(),s.begin(),to_lower());
  if      (s == "idle")			fs = Idle;
  else if (s == "standby")		fs = Standby;
  else if (s == "downloading")		fs = Downloading;
  else if (s == "ready")		fs = Ready;
  else if (s == "error")		fs = Error;
  else if (s == "mixed")		fs = Mixed;
  else if (s == "running")		fs = Running;
  return fs;
}
//____________________________________________________________________
std::string 
RcuCE::BasicFsm::State2String(const State& state)
{
  using namespace CommonStates;
  std::string s;
  switch (state) {
  case Idle:		s = "IDLE";			break;
  case Standby:		s = "STANDBY";			break;
  case Downloading:	s = "DOWNLOADING";		break;
  case Ready:		s = "READY";			break;
  case Error:		s = "ERROR";			break;
  case Mixed:		s = "MIXED";			break;
  case Running:		s = "RUNNING";			break;
  }
  return s;
}

//____________________________________________________________________
void
RcuCE::BasicFsm::IllegalAction(const Action& a, const State& s) 
{ 
    std::cout << "Illegal action " << Action2String(a) 
	      << " in  state " << State2String(s) 
	      << std::endl;
  SetState(CommonStates::Error); 
}

//____________________________________________________________________
void
RcuCE::BasicFsm::SetParent(BasicFsmParent* p) 
{
  if (_parent) _parent->RemoveFsm(*this);
  _parent = p;
}

//====================================================================
RcuCE::StateService::StateService(const std::string&   name, 
				  RcuCE::StateMachine& fsm) 
  : _fsm(fsm), 
    _serv(name, fsm.GetState()) 
{ 
  _service = &_serv; 
}
//____________________________________________________________________
int
RcuCE::StateService:: Update() 
{
  _serv = _fsm.GetState();
  return 0;
}

//====================================================================
RcuCE::BasicFsmParent::BasicFsmParent(int id)
  : BasicFsm(id), 
    _fsms(0)
{
  using namespace CommonStates;
  using namespace CommonActions;
  AddState(Mixed);
  AddTransition(Mixed, GoStandby, Standby); 
  AddTransition(Mixed, GoIdle,    Idle); 
}
//____________________________________________________________________
RcuCE::BasicFsmParent::~BasicFsmParent()
{
  for (FsmList::iterator i = _fsms.begin(); i != _fsms.end(); ++i) 
    (*i)->SetParent(0);
}
//____________________________________________________________________
bool 
RcuCE::BasicFsmParent::AddFsm(BasicFsm& fsm)
{
  FsmList::iterator i = std::find(_fsms.begin(),_fsms.end(),&fsm);
  if (i != _fsms.end()) return false;
  _fsms.push_back(&fsm);
  fsm.SetParent(this);
  UpdateState();
  return true;
}
//____________________________________________________________________
bool 
RcuCE::BasicFsmParent::RemoveFsm(BasicFsm& fsm)
{
  FsmList::iterator i = std::find(_fsms.begin(),_fsms.end(),&fsm);
  if (i == _fsms.end()) return false;
  _fsms.remove(*i);
  fsm.SetParent(0);
  UpdateState();
  return true;
}
//____________________________________________________________________
RcuCE::BasicFsm* 
RcuCE::BasicFsmParent::FindFsm(unsigned int   action, 
	unsigned char  id, 
	unsigned short addr, 
	unsigned short param) const
{
  for (FsmList::const_iterator i = _fsms.begin(); i != _fsms.end(); ++i) 
    if ((*i)->TryHandle(action, id, addr, param)) return *i;
  return 0;
}
//____________________________________________________________________
RcuCE::BasicFsm* 
RcuCE::BasicFsmParent::FindFsm(const std::string& name) const
{
  for (FsmList::const_iterator i = _fsms.begin(); i != _fsms.end(); ++i) {
    const StateService* s = (*i)->GetStateService();
    if (!s) continue;
    if (name == s->Name()) return (*i);
  }
  return 0;
}
//____________________________________________________________________
void 
RcuCE::BasicFsmParent::Transition(const State&  src, 
				  const Action& action, 
				  const State&  dest)
{
  BasicFsm::Transition(src, action, dest);
  for (FsmList::const_iterator i = _fsms.begin(); i != _fsms.end(); ++i) {
    (*i)->Trigger(action);
  }
  UpdateState();
  if (GetState() != dest) {
    std::cout << "Destination state: " << State2String(dest) 
	      << " Current state: " << State2String(GetState()) 
	      << std::endl;
    SetState(CommonStates::Error);
  }
}
//____________________________________________________________________
const RcuCE::BasicFsm::State& 
RcuCE::BasicFsmParent::UpdateState() 
{
  bool first = true;
  State found = CommonStates::Mixed;
  for (FsmList::const_iterator i = _fsms.begin(); i != _fsms.end(); ++i) {
    const State& s = (*i)->GetState();
    if (s == CommonStates::Error) { 
      SetState(CommonStates::Error);
      return GetState();
    }
    if (!first && s != found) { 
      SetState(CommonStates::Mixed);
      return GetState();
    }
    found = s;
    first = false;
  }
  SetState(found);
  return GetState();
}

//____________________________________________________________________
//
// EOF
//
