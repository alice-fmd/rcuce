#include <rcuce/rcuce_bfsm.hh>
#include <feeserverxx/fee_dbg.hh>
#include <iostream>
#include <iomanip>
#include <cassert>

//____________________________________________________________________
int
main()
{
  using RcuCE::BasicFsm;
  // using RcuCE::CommonActions;
  BasicFsm fsm(0);
  
  typedef  std::map<BasicFsm::Action,BasicFsm::State> TransMap;
  do { 
    std::cout << "Current state is " << std::setw(3) << fsm.GetState() << ": " 
	      << BasicFsm::State2String(fsm.GetState()) 
	      << "\nPossible actions are: " << std::endl;
    TransMap ts;
    fsm.Transitions(ts);
    int j = 1;
    std::cout << std::setw(3) << 0 << ": Quit" << std::endl;
    for (TransMap::const_iterator i = ts.begin(); i != ts.end(); ++i, ++j)
      std::cout << std::right << std::setw(3)  << i->first  << ": "
		<< std::left  << std::setw(30) 
		<< BasicFsm::Action2String(i->first) << " ["
		<< std::right << std::setw(3)  << i->second << ": " 
		<< BasicFsm::State2String(i->second) << "]" 
		<< std::endl;
    std::cout << "Please choose an action number: " << std::flush;
    int choice;
    std::cin >> choice;
    if (choice == 0) break;
    TransMap::const_iterator t = ts.find(choice);
    if (t == ts.end()) { 
      std::cerr << "WARNING: Invalid transition # " << choice << "\n" 
		<< std::endl;
      // continue;
    }
    fsm.Trigger(choice);
    std::cout << std::endl;
    // BasicFsm::State target = t->second;
    // assert(target == fsm.GetState());
  } while(true);
  return 0;
}

  
//____________________________________________________________________
//
// EOF
//



  
