dnl ------------------------------------------------------------------
dnl -*- mode: Autoconf -*- 
dnl
dnl  Generic feeserverxx framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_FEESERVER([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_FEESERVERXX],
[
    AC_REQUIRE([AC_THREAD_FLAGS])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([feeserverxx],
        [AC_HELP_STRING([--with-feeserverxx],	
                        [Prefix where FeeServer++ is installed])],
	[],[with_feeserverxx="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([feeserverxx-url],
        [AC_HELP_STRING([--with-feeserverxx-url],
		[Base URL where the FeeServer++ dodumentation is installed])],
        feeserverxx_url=$withval, feeserverxx_url="")
    if test "x${FEESERVERXX_CONFIG+set}" != xset ; then 
        if test "x$with_feeserverxx" != "xno" ; then 
	    FEESERVERXX_CONFIG=$with_feeserverxx/bin/feeserverxx-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_feeserverxx" != "xno" ; then 
        AC_PATH_PROG(FEESERVERXX_CONFIG, feeserverxx-config, no)
        feeserverxx_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for FeeServer++ version >= $feeserverxx_min_version)

        # Check if we got the script
        with_feeserverxx=no    
        if test "x$FEESERVERXX_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           FEESERVERXX_CPPFLAGS=`$FEESERVERXX_CONFIG --cppflags`
           FEESERVERXX_INCLUDEDIR=`$FEESERVERXX_CONFIG --includedir`
           FEESERVERXX_LIBS=`$FEESERVERXX_CONFIG --libs`
           FEESERVERXX_LTLIBS=`$FEESERVERXX_CONFIG --ltlibs`
           FEESERVERXX_LIBDIR=`$FEESERVERXX_CONFIG --libdir`
           FEESERVERXX_LDFLAGS=`$FEESERVERXX_CONFIG --ldflags`
           FEESERVERXX_LTLDFLAGS=`$FEESERVERXX_CONFIG --ltldflags`
           FEESERVERXX_PREFIX=`$FEESERVERXX_CONFIG --prefix`
           FEESERVERXX_DOCDIR=`$FEESERVERXX_CONFIG --docdir`
           
           # Check the version number is OK.
           feeserverxx_version=`$FEESERVERXX_CONFIG -V` 
           feeserverxx_vers=`echo $feeserverxx_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           feeserverxx_regu=`echo $feeserverxx_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $feeserverxx_vers -ge $feeserverxx_regu ; then 
                with_feeserverxx=yes
           fi
        fi
        AC_MSG_RESULT($with_feeserverxx - is $feeserverxx_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_FEESERVERXX, [Whether we have feeserverxx])
    
    
        if test "x$with_feeserverxx" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
            LDFLAGS="$LDFLAGS $FEESERVERXX_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $FEESERVERXX_CPPFLAGS"
	    LIBS="$LIBS $FEESERVERXX_LIBS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_feeserverxx_fee_main_hh=0
            AC_CHECK_HEADER([feeserverxx/fee_main.hh], 
                            [have_feeserverxx_fee_main_hh=1])
    
            # Check the library. 
            have_libfeeserverxx=no
            AC_MSG_CHECKING(for -lfeeserverxx)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <feeserverxx/fee_main.hh>],
                                           [FeeServer::Main m("a","b","c");])], 
                                            [have_libfeeserverxx=yes])
            AC_MSG_RESULT($have_libfeeserverxx)
    
            if test $have_feeserverxx_fee_main_hh -gt 0    && \
                test "x$have_libfeeserverxx"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_FEESERVERXX)
            else 
                with_feeserverxx=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
	    CPPFLAGS=$save_CPPFLAGS
	    LDFLAGS=$save_LDFLAGS
	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the FeeServer++ documentation is installed)
        if test "x$feeserverxx_url" = "x" ; then
           FEESERVERXX_URL=`$FEESERVERXX_CONFIG --htmldir`
        else 
    	   FEESERVERXX_URL=$feeserverxx_url
        fi	
        AC_MSG_RESULT($FEESERVERXX_URL)
    fi
   
    if test "x$with_feeserverxx" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FEESERVERXX_URL)
    AC_SUBST(FEESERVERXX_DOCDIR)
    AC_SUBST(FEESERVERXX_PREFIX)
    AC_SUBST(FEESERVERXX_CPPFLAGS)
    AC_SUBST(FEESERVERXX_INCLUDEDIR)
    AC_SUBST(FEESERVERXX_LDFLAGS)
    AC_SUBST(FEESERVERXX_LIBDIR)
    AC_SUBST(FEESERVERXX_LIBS)
    AC_SUBST(FEESERVERXX_LTLIBS)
    AC_SUBST(FEESERVERXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
