dnl -*- mode: autoconf -*- 
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DIM],
[
  AC_REQUIRE([AC_DIM_ARCH])
  dnl ------------------------------------------------------------------
  dnl AC_ARG_ENABLE([builtin-dim],
  dnl 	        [AC_HELP_STRING([--enable-builtin-dim],
  dnl 	                        [Use shipped DIM, not systems])])
  have_dim=no
  AC_LANG_PUSH(C++)
  AC_CHECK_LIB(dim, [dic_get_server],
	       [AC_CHECK_HEADERS([dim/dim.hxx],[have_dim=yes])])
  AC_LANG_POP(C++)
  dnl if test "x$enable_builtin_dim" = "xyes" ; then 
  dnl    have_dim=no
  dnl fi
  dnl AC_MSG_CHECKING(whether to use possible system DIM installed)
  dnl if test "x$have_dim" = "xno" ; then 
  dnl    AC_CONFIG_SUBDIRS(dim)
  dnl fi
  dnl AC_MSG_RESULT($have_dim)
  dnl AM_CONDITIONAL(NEED_DIM, test "x$have_dim" = "xno")

  if test "x$have_dim" = "xyes" ; then 
    ifelse([$1], , :, [$1])     
  else 
    ifelse([$2], , :, [$2])     
  fi
])
dnl
dnl EOF
dnl
