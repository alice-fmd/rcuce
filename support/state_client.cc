#include <dim/dic.hxx>
// #include <rcuce/rcuce_states.hh>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ctime>

struct StateInfo : public DimInfoHandler 
{
  StateInfo(const std::string& name, int rate) 
    : _info(0), _no_link(-1), _last(-2)
  {
    std::stringstream s;
    s << name << "_RCU_STATE";
    _info = new DimStampedInfo(s.str().c_str(), rate, _no_link, this);
  }
  StateInfo(const StateInfo& o); 
  /*    : // DimInfoHandler(o), 
      _info(o._info), 
      _no_link(o._no_link), 
      _last(o._last)
      {} */
  StateInfo& operator=(const StateInfo& o) 
  {
    _info = o._info;
    // _no_link = o._no_link;
    _last = o._last;
    return *this;
  }

  std::string State2String(int state)
  {
    // using namespace RcuCE::CommonStates;
    std::string s;    
    switch (state) {
    case 1:  	   s = "IDLE       "; break;
    case 2:	   s = "STANDBY    "; break;
    case 3:	   s = "DOWNLOADING"; break;
    case 4:	   s = "READY      "; break;
    case 5:	   s = "MIXED      "; break;
    case 6:	   s = "ERROR      "; break;
    case 7:	   s = "RUNNING    "; break;
    default:       
      if (state == _no_link) 
	s = "NO_LINK    "; 
      else 
	s = "UNKNOWN    "; 
      break;
    }
    return s;
  }

  void infoHandler() 
  {
    if (getInfo() != _info) return;

    int         state = _info->getInt();
    int         qual  = _info->getQuality();
    time_t      time  = _info->getTimestamp();
    int         utime = _info->getTimestampMillisecs();
    if (_last != state) {
      char buf[128];
      strftime(buf, 128, "%x %X", localtime(&time));
      std::cout << '[' << buf << '.' << std::setfill('0') 
  	        << std::setw(3) << utime << std::setfill(' ') 
	        << "] State is now: " << State2String(state) 
	        << " (quality: " << qual << ")" << std::endl;
   }
   _last = state;
  }
  DimStampedInfo* _info;
  const int       _no_link;
  int             _last;
};

void
usage(std::ostream& o, const char* name)
{
  o << "Usage: " << name << " [OPTIONS]\n\n"
    << "Options:\n"
    << "\t-d HOST	DNS nost name\n"
    << "\t-n NAME	Server name\n" 
    << "\t-u SECS	Minimal update rate in seconds\n"
    << std::endl;
}

template <typename T>
T
str2val(const char* str, T& v) 
{
  std::stringstream s(str);
  s >> v;
  return v;
}

int
main(int argc, char** argv)
{
  std::string dns  = "localhost";
  std::string name = "FMD-FEE_0_0_0";
  int         rate = 0;

  for (int i = 1; i < argc; i++) { 
    if (argv[i][0] != '-') continue;
    switch (argv[i][1]) { 
    case 'h':   usage(std::cout, argv[0]); return 0; 
    case 'd':   dns  = argv[++i]; break;
    case 'n':   name = argv[++i]; break;
    case 'u':   rate = str2val(argv[++i], rate); break;
    default: 
      std::cerr << argv[0] << ": unknown option '" 
		<< argv[i] << "'" << std::endl;
      return 1;
    }
  }
  
  std::cout << "DNS:         " << dns << "\n"
	    << "Server:      " << name << "\n"
	    << "Update rate: " << rate << std::endl;

  DimClient::setDnsNode(dns.c_str());
  
  StateInfo s(name, rate);
  
  while (true) pause();

  return 0;
}

  
//
// EOF
//
