dnl -*- mode: Autoconf -*- 
dnl
dnl  
dnl  ROOT generic rcuce framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_RCUCE([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUCE],
[
    AC_REQUIRE([AC_FEESERVER])
    AC_REQUIRE([AC_DCSC])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuce],
        [AC_HELP_STRING([--with-rcuce],	
                        [Prefix where RcuCE is installed])],
	[],[with_rcuce="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuce-url],
        [AC_HELP_STRING([--with-rcuce-url],
		[Base URL where the RcuCE dodumentation is installed])],
        rcuce_url=$withval, rcuce_url="")
    if test "x${RCUCE_CONFIG+set}" != xset ; then 
        if test "x$with_rcuce" != "xno" ; then 
	    RCUCE_CONFIG=$with_rcuce/bin/rcuce-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_rcuce" != "xno" ; then 
        AC_PATH_PROG(RCUCE_CONFIG, rcuce-config, no)
        rcuce_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for RcuCE version >= $rcuce_min_version)

        # Check if we got the script
        with_rcuce=no    
        if test "x$RCUCE_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           RCUCE_CPPFLAGS=`$RCUCE_CONFIG --cppflags`
           RCUCE_INCLUDEDIR=`$RCUCE_CONFIG --includedir`
           RCUCE_LIBS=`$RCUCE_CONFIG --libs`
           RCUCE_LTLIBS=`$RCUCE_CONFIG --ltlibs`
           RCUCE_LIBDIR=`$RCUCE_CONFIG --libdir`
           RCUCE_LDFLAGS=`$RCUCE_CONFIG --ldflags`
           RCUCE_LTLDFLAGS=`$RCUCE_CONFIG --ltldflags`
           RCUCE_PREFIX=`$RCUCE_CONFIG --prefix`
           
           # Check the version number is OK.
           rcuce_version=`$RCUCE_CONFIG -V` 
           rcuce_vers=`echo $rcuce_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           rcuce_regu=`echo $rcuce_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $rcuce_vers -ge $rcuce_regu ; then 
                with_rcuce=yes
           fi
        fi
        AC_MSG_RESULT($with_rcuce - is $rcuce_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_RCUCE, [Whether we have rcuce])
    
    
        if test "x$with_rcuce" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
            LDFLAGS="$LDFLAGS $FEESERVER_LDFLAGS $DCSC_LDFLAGS $RCUCE_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $FEESERVER_CPPFLAGS $DCSC_CPPFLAGS $RCUCE_CPPFLAGS"
     	    LIBS="$LIBS $FEESERVER_LIBS $DCSC_LIBS"
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_rcuce_rcuce_ce_hh=0
            AC_CHECK_HEADER([rcuce/rcuce_ce.hh], 
                            [have_rcuce_rcuce_ce_hh=1])
    
            # Check the library. 
            have_librcuce=no
            AC_MSG_CHECKING(for -lrcuce)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <rcuce/rcuce_dcsc.hh>
#include <rcuce/rcuce_msg.hh>
DINIT(false);],
                                            [RcuCE::Dcsc d;])], 
                                            [have_librcuce=yes])
            AC_MSG_RESULT($have_librcuce)
    
            if test $have_rcuce_rcuce_ce_hh -gt 0    && \
                test "x$have_librcuce"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_RCUCE)
            else 
                with_rcuce=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	    CPPFLAGS=$save_CPPFLAGS
      	    LDFLAGS=$save_LDFLAGS
      	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the RcuCE documentation is installed)
        if test "x$rcuce_url" = "x" && \
    	test ! "x$RCUCE_PREFIX" = "x" ; then 
           RCUCE_URL=${RCUCE_PREFIX}/share/doc/rcuce
        else 
    	   RCUCE_URL=$rcuce_url
        fi	
        AC_MSG_RESULT($RCUCE_URL)
    fi
   
    if test "x$with_rcuce" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUCE_URL)
    AC_SUBST(RCUCE_PREFIX)
    AC_SUBST(RCUCE_CPPFLAGS)
    AC_SUBST(RCUCE_INCLUDEDIR)
    AC_SUBST(RCUCE_LDFLAGS)
    AC_SUBST(RCUCE_LIBDIR)
    AC_SUBST(RCUCE_LIBS)
    AC_SUBST(RCUCE_LTLIBS)
    AC_SUBST(RCUCE_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
