/** @mainpage RCU Control Engine Implemented in C++ 

    This is a C++ implementation of the DCS RCU Control Engine


    @section content Contents 
    
    - @ref overview
    - @ref rcuce_bits
    - @ref rcuce_core
      - @ref rcuce_dcsc 
      - @ref rcuce_rcu 
      - @ref rcuce_fsm
      - @ref rcuce_serv
        - @ref rcuce_rcu_serv
    - @ref rcuce_fecs
    - @ref rcuce_msg

    @section overview Overview

    This is a re-implementation of the RCU control engine, based on
    the C++ reimplementation of the FeeServer. 

    The basics of the library is the RcuCE::ControlEngine class.  This
    defines a the interface to the FeeServer.  It handles incoming
    events, and updates the services defined in the server.  The
    control engine defines a few commands it self (see @ref
    rcuce_bits), but delegates most other commands to the registered
    RcuCE::CommandHandler objects.  The control engine also has a few
    services, but most other services are managed by registered
    ServiceProvider objects.

    The library also defines a top-level interface to the RCU (@ref
    RcuCE::Rcu).  This provides all the commands to read/write
    registers of the RCU and associated Front-end cards both via the
    ALTRO bus and the @f$ I^2C@f$.

    The RcuCE::Rcu class uses the RcuCE::Dcsc class which is an
    abstraction over the Dcsc message buffer interface.

    Front-end card services are not defined directly in the library.
    Instead, the user should provide a sub-class of the
    RcuCE::FecFactory class (see @ref rcuce_fecs) to create custom
    RcuCE::Fec objects whenever a front-end card becomes active.  When
    the RcuCE::Rcu object sees that a FEC has been turned on, it
    suspends the monitor thread, and asks the RcuCE::FecFactory to
    give it a new RcuCE::Fec object.  The RcuCE::Fec object should
    register new services via the RcuCE::ServiceProvider interface.
    After having made all needed Fec objects, the RcuCE::Rcu object
    resumes the monitor thread.  A similar approach is used if a FEC
    disappeares: The monitor thread is suspended, the RcuCE::Fec
    object is removed, along with its services, and the monitor thread
    is resumed.

    The library provides a Finite State Machine (FSM) interface that a
    user can derive from to define their own state machines.  See also
    @ref rcuce_fsm. 
*/
#error not for compilation. 

//
// EOF
//
